﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class PoBAL
    {
        #region Global declaration
        PoDAL objDAL = PoDAL.Instance;
        #endregion

        #region Constructer

        private static readonly PoBAL instance = new PoBAL();   //singleton

        static PoBAL()
        {
        }

        private PoBAL()
        {

        }

        public static PoBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get PoDetails
        public PurchaseOrderDetails GetPurchaseorderDetails(Int64 poNumber)
        {
            try
            {
                return objDAL.GetPurchaseOrderDetails(poNumber);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
