﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
    public class FieldsBAL
    {
        #region Global declaration
        FieldsDAL objDAL = FieldsDAL.Instance;
        #endregion

        #region Constructer

        private static readonly FieldsBAL instance = new FieldsBAL();   //singleton

        static FieldsBAL()
        {
        }

        private FieldsBAL()
        {
             
        }

        public static FieldsBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Fields
        public List<Fields> GetFields(int folderid)
        {
            try
            {
                return objDAL.GetFields(folderid);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Get Fields
        public FolderDetails GetFolderFields(string PageName, int folderid)
        {
            try
            {
                return objDAL.GetFolderFields(PageName, folderid);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
       
    }
}
