﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class PackagesBAL
    {
        #region Global declaration
        PackagesDAL objDAL = PackagesDAL.Instance;
        #endregion

        #region Constructer

        private static readonly PackagesBAL instance = new PackagesBAL();   //singleton

        static PackagesBAL()
        {
        }

        private PackagesBAL()
        {

        }

        public static PackagesBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Packages
        //public List<PackagesData> GetPackages(string pkgNumber)
        //{
        //    try
        //    {
        //        return objDAL.GetPackages(pkgNumber);
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        public List<PackagesData> GetPackages(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetPackages(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
