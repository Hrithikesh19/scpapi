﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class DemandBAL
    {
        #region Global declaration
        DemandDAL objDAL = DemandDAL.Instance;
        #endregion

        #region Constructer

        private static readonly DemandBAL instance = new DemandBAL();   //singleton

        static DemandBAL()
        {
        }

        private DemandBAL()
        {

        }

        public static DemandBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Demand WorkBench Data
        public List<DemandPurchaseWorkBenchData> GetDemandPurchaseWorkBenchData(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetDemandPurchaseWorkBenchData(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Get Demand Item Attributes
        public List<DemandItemAttributes> GetDemandItemAttributes(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetDemandAttributes(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        public DemandDetails GetDemandSearchForecast(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetDemandSearchForecast(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public DemandDetails GetDemandDetails(string itemNumber)
        {
            try
            {
                return objDAL.GetDemandDetails(itemNumber);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
