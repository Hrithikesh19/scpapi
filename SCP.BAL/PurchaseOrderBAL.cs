﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class PurchaseOrderBAL
    {
        #region Global declaration
        PurchaseOrderDAL objDAL = PurchaseOrderDAL.Instance;
        #endregion

        #region Constructer

        private static readonly PurchaseOrderBAL instance = new PurchaseOrderBAL();   //singleton

        static PurchaseOrderBAL()
        {
        }

        private PurchaseOrderBAL()
        {

        }

        public static PurchaseOrderBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Purchase Order Data
        public List<PurchaseOrderData> GetPurchaseOrderData(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetPurchaseOrderData(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
