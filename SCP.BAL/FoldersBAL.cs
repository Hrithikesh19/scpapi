﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class FoldersBAL
    {
        #region Global declaration
        FoldersDAL objDAL = FoldersDAL.Instance;
        #endregion

        #region Constructer

        private static readonly FoldersBAL instance = new FoldersBAL();   //singleton

        static FoldersBAL()
        {
        }

        private FoldersBAL()
        {

        }

        public static FoldersBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Fields
        public List<Folders> GetFolders(string pageName)
        {
            try
            {
                return objDAL.GetFolders(pageName);
            }
            catch (Exception ex)
            {
                return null;
            }
        }     
        #endregion
    }
}
