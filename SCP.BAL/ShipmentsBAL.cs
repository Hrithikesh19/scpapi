﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class ShipmentsBAL
    {
        #region Global declaration
        ShipmentsDAL objDAL = ShipmentsDAL.Instance;
        #endregion

        #region Constructer

        private static readonly ShipmentsBAL instance = new ShipmentsBAL();   //singleton

        static ShipmentsBAL()
        {
        }

        private ShipmentsBAL()
        {

        }

        public static ShipmentsBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Shipments
        public List<ShipmentsData> GetShipmentByShipmentNumber(Int64 shipmentnumber)
        {
            try
            {
                return objDAL.GetShipmentByShipmentNumber(shipmentnumber);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<ShipmentsData> GetShipments(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetShipments(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
