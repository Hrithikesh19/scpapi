﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SCP.DAL;
using SCP.BO;

namespace SCP.BAL
{
   public class ScheduleBAL
    {
        #region Global declaration
        ScheduleDAL objDAL = ScheduleDAL.Instance;
        #endregion

        #region Constructer

        private static readonly ScheduleBAL instance = new ScheduleBAL();   //singleton

        static ScheduleBAL()
        {
        }

        private ScheduleBAL()
        {

        }

        public static ScheduleBAL Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        #region Get Schedule Data
        public List<ScheduleData> GetScheduleData(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetScheduleData(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
        #region Get Schedule Summary
        public List<SchedulesSummary> GetScheduleSummary(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetScheduleSummary(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Get Schedule DeliveryDates
        public List<ScheduleDeliverDates> GetScheduleDeliveryDates(PoOrderRequest request)
        {
            try
            {
                return objDAL.GetScheduleDeliveryDates(request);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
