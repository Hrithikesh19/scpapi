﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;

namespace SCP.DAL
{
    public class PackagesDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;
        DataTable dt;
        #endregion

        #region constructor

        private static readonly PackagesDAL instance = new PackagesDAL();   //singleton

        static PackagesDAL()
        {
        }
        private PackagesDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static PackagesDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Packages 
        //public List<PackagesData> GetPackages(string pkgNumber)
        //{
        //    try
        //    {

        //        con = new SqlConnection(ConnString);
        //        SqlCommand objSqlCommand = new SqlCommand("GetPackagesData", con);
        //        objSqlCommand.CommandType = CommandType.StoredProcedure;
        //        objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@pkgNumber", pkgNumber, SqlDbType.VarChar));

        //        List<PackagesData> packages = new List<PackagesData>();
        //        using (con)
        //        {
        //            if (con.State != ConnectionState.Open)
        //            {
        //                con.Open();
        //            }

        //            using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
        //            {
        //                // Fill the DataSet using default values for DataTable names, etc
        //                DataSet dataset = new DataSet();
        //                da.Fill(dataset);

        //                packages = (from DataRow reader in dataset.Tables[0].Rows
        //                            select new PackagesData()
        //                            {

        //                                PACKAGE_ID = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
        //                                PACKAGE_NUMBER = reader["PACKAGE_NUMBER"] != null ? reader["PACKAGE_NUMBER"].ToString() : "",
        //                                SHIPMENT_ID = reader["SHIPMENT_ID"] != null ? reader["SHIPMENT_ID"].ToString() : "",
        //                                STATUS = reader["STATUS"] != null ? reader["STATUS"].ToString() : "",
        //                                STAGING_LANE = reader["STAGING_LANE"] != null ? reader["STAGING_LANE"].ToString() : "",
        //                                QUANTITY = reader["QUANTITY"] != null ? reader["QUANTITY"].ToString() : "",
        //                                SECONDARY_QUANTITY = reader["SECONDARY_QUANTITY"] != null ? reader["SECONDARY_QUANTITY"].ToString() : "",
        //                                QUANTITY_RECEIVED = reader["QUANTITY_RECEIVED"] != null ? reader["QUANTITY_RECEIVED"].ToString() : "",
        //                                REFERENCE = reader["REFERENCE"] != null ? reader["REFERENCE"].ToString() : "",
        //                                REVISION = reader["REVISION"] != null ? reader["REVISION"].ToString() : "",
        //                                INVOICE_PRICE = reader["INVOICE_PRICE"] != null ? reader["INVOICE_PRICE"].ToString() : "",
        //                                HOST_KEY = reader["HOST_KEY"] != null ? reader["HOST_KEY"].ToString() : "",
        //                                MIM_TRACKING_NUMBER = reader["MIM_TRACKING_NUMBER"] != null ? reader["MIM_TRACKING_NUMBER"].ToString() : "",
        //                                MIM_FREIGHT_CHARGE = reader["MIM_FREIGHT_CHARGE"] != null ? reader["MIM_FREIGHT_CHARGE"].ToString() : "",
        //                                MIM_CURRENCY = reader["MIM_CURRENCY"] != null ? reader["MIM_CURRENCY"].ToString() : "",
        //                                MIM_WEIGHT = reader["MIM_WEIGHT"] != null ? reader["MIM_WEIGHT"].ToString() : "",
        //                                ERP_CODE = reader["ERP_CODE"] != null ? reader["ERP_CODE"].ToString() : "",
        //                                ERP_KEY_ID = reader["ERP_KEY_ID"] != null ? reader["ERP_KEY_ID"].ToString() : "",
        //                                ERP_KEY_FIELD = reader["ERP_KEY_FIELD"] != null ? reader["ERP_KEY_FIELD"].ToString() : "",
        //                                ERP_SHIP_TO_KEY = reader["ERP_SHIP_TO_KEY"] != null ? reader["ERP_SHIP_TO_KEY"].ToString() : "",
        //                                ERP_SHIP_FROM_KEY = reader["ERP_SHIP_FROM_KEY"] != null ? reader["ERP_SHIP_FROM_KEY"].ToString() : "",
        //                                ERP_ITEM_NUMBER = reader["ERP_ITEM_NUMBER"] != null ? reader["ERP_ITEM_NUMBER"].ToString() : "",
        //                                ERP_ITEM_DESCRIPTION = reader["ERP_ITEM_DESCRIPTION"] != null ? reader["ERP_ITEM_DESCRIPTION"].ToString() : "",
        //                                ERP_INVENTORY_ITEM_ID = reader["ERP_INVENTORY_ITEM_ID"] != null ? reader["ERP_INVENTORY_ITEM_ID"].ToString() : "",
        //                                ERP_OPM_ITEM_ID = reader["ERP_OPM_ITEM_ID"] != null ? reader["ERP_OPM_ITEM_ID"].ToString() : "",
        //                                ERP_ITEM_REV = reader["ERP_ITEM_REV"] != null ? reader["ERP_ITEM_REV"].ToString() : "",
        //                                ERP_ITEM_LOT_CONTROLLED = reader["ERP_ITEM_LOT_CONTROLLED"] != null ? reader["ERP_ITEM_LOT_CONTROLLED"].ToString() : "",
        //                                ERP_ITEM_SUBLOT_CONTROLLED = reader["ERP_ITEM_SUBLOT_CONTROLLED"] != null ? reader["ERP_ITEM_SUBLOT_CONTROLLED"].ToString() : "",
        //                                ERP_ITEM_SERIAL_CONTROLLED = reader["ERP_ITEM_SERIAL_CONTROLLED"] != null ? reader["ERP_ITEM_SERIAL_CONTROLLED"].ToString() : "",
        //                                ERP_ITEM_REV_CONTROLLED = reader["ERP_ITEM_REV_CONTROLLED"] != null ? reader["ERP_ITEM_REV_CONTROLLED"].ToString() : "",
        //                                ERP_ITEM_UOM = reader["ERP_ITEM_UOM"] != null ? reader["ERP_ITEM_UOM"].ToString() : "",
        //                                ERP_ITEM_SECONDARY_UOM = reader["ERP_ITEM_SECONDARY_UOM"] != null ? reader["ERP_ITEM_SECONDARY_UOM"].ToString() : "",
        //                                ERP_ITEM_DUALUOM_CONTROL = reader["ERP_ITEM_DUALUOM_CONTROL"] != null ? reader["ERP_ITEM_DUALUOM_CONTROL"].ToString() : "",
        //                                ERP_ITEM_WEIGHT = reader["ERP_ITEM_WEIGHT"] != null ? reader["ERP_ITEM_WEIGHT"].ToString() : "",
        //                                ERP_ITEM_WEIGHT_UOM = reader["ERP_ITEM_WEIGHT_UOM"] != null ? reader["ERP_ITEM_WEIGHT_UOM"].ToString() : "",
        //                                ERP_PO_NUMBER = reader["ERP_PO_NUMBER"] != null ? reader["ERP_PO_NUMBER"].ToString() : "",
        //                                ERP_PO_NOTE = reader["ERP_PO_NOTE"] != null ? reader["ERP_PO_NOTE"].ToString() : "",
        //                                ERP_PO_TYPE = reader["ERP_PO_TYPE"] != null ? reader["ERP_PO_TYPE"].ToString() : "",
        //                                ERP_LINE_NUMBER = reader["ERP_LINE_NUMBER"] != null ? reader["ERP_LINE_NUMBER"].ToString() : "",
        //                                ERP_VENDOR_NAME = reader["ERP_VENDOR_NAME"] != null ? reader["ERP_VENDOR_NAME"].ToString() : "",
        //                                ERP_BUYER_NAME = reader["ERP_BUYER_NAME"] != null ? reader["ERP_BUYER_NAME"].ToString() : "",
        //                                ERP_KANBAN_CARD = reader["ERP_KANBAN_CARD"] != null ? reader["ERP_KANBAN_CARD"].ToString() : "",
        //                                ERP_SHIP_TO_NAME = reader["ERP_SHIP_TO_NAME"] != null ? reader["ERP_SHIP_TO_NAME"].ToString() : "",
        //                                ERP_SHIP_TO_NAME_ID = reader["ERP_SHIP_TO_NAME_ID"] != null ? reader["ERP_SHIP_TO_NAME_ID"].ToString() : "",
        //                                ERP_SHIP_TO_LOCATION = reader["ERP_SHIP_TO_LOCATION"] != null ? reader["ERP_SHIP_TO_LOCATION"].ToString() : "",
        //                                ERP_SHIP_TO_LOCATION_ID = reader["ERP_SHIP_TO_LOCATION_ID"] != null ? reader["ERP_SHIP_TO_LOCATION_ID"].ToString() : "",
        //                                ERP_SCHEDULE_NUMBER = reader["ERP_SCHEDULE_NUMBER"] != null ? reader["ERP_SCHEDULE_NUMBER"].ToString() : "",
        //                                PO_LINE_LOCATION_ID = reader["PO_LINE_LOCATION_ID"] != null ? reader["PO_LINE_LOCATION_ID"].ToString() : "",
        //                                INTERFACE_LOG = reader["INTERFACE_LOG"] != null ? reader["INTERFACE_LOG"].ToString() : "",
        //                                INTERFACE_ERROR = reader["INTERFACE_ERROR"] != null ? reader["INTERFACE_ERROR"].ToString() : "",
        //                                SO_PO_TRANSACTION_STATUS = reader["SO_PO_TRANSACTION_STATUS"] != null ? reader["SO_PO_TRANSACTION_STATUS"].ToString() : "",
        //                                SCH_PROMISE_NUMBER = reader["SCH_PROMISE_NUMBER"] != null ? reader["SCH_PROMISE_NUMBER"].ToString() : "",
        //                                VMI_AGREEMENT_NUMBER = reader["VMI_AGREEMENT_NUMBER"] != null ? reader["VMI_AGREEMENT_NUMBER"].ToString() : "",
        //                                APPLICATION_CODE = reader["APPLICATION_CODE"] != null ? reader["APPLICATION_CODE"].ToString() : "",
        //                                APPLICATION_KEY_ID = reader["APPLICATION_KEY_ID"] != null ? reader["APPLICATION_KEY_ID"].ToString() : "",
        //                                //ATTRIBUTE1 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE2 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE3 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE4 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE5 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE6 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE7 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE8 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE9 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE10 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE11 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE12 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE13 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE14 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE15 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE16 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE17 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE18 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE19 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                //ATTRIBUTE20 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
        //                                CREATED_BY = reader["CREATED_BY"] != null ? reader["CREATED_BY"].ToString() : "",
        //                                CREATION_DATE = reader["CREATION_DATE"] != null ? reader["CREATION_DATE"].ToString() : "",
        //                                LAST_UPDATE_DATE = reader["LAST_UPDATE_DATE"] != null ? reader["LAST_UPDATE_DATE"].ToString() : "",
        //                                LAST_UPDATE_BY = reader["LAST_UPDATE_BY"] != null ? reader["LAST_UPDATE_BY"].ToString() : "",
        //                                VENDOR_MATERIAL = reader["VENDOR_MATERIAL"] != null ? reader["VENDOR_MATERIAL"].ToString() : "",
        //                                COUNTRY_OF_ORIGIN_CODE = reader["COUNTRY_OF_ORIGIN_CODE"] != null ? reader["COUNTRY_OF_ORIGIN_CODE"].ToString() : "",
        //                                COUNTRY_OF_ORIGIN_MARK = reader["COUNTRY_OF_ORIGIN_MARK"] != null ? reader["COUNTRY_OF_ORIGIN_MARK"].ToString() : "",
        //                                REJECT_REASON = reader["REJECT_REASON"] != null ? reader["REJECT_REASON"].ToString() : "",
        //                                FSL_USED = reader["FSL_USED"] != null ? reader["FSL_USED"].ToString() : "",
        //                                BOX_PER_PACKAGE = reader["BOX_PER_PACKAGE"] != null ? reader["BOX_PER_PACKAGE"].ToString() : "",
        //                                LAST_UPDATE_BY_NAME = reader["LAST_UPDATE_BY_NAME"] != null ? reader["LAST_UPDATE_BY_NAME"].ToString() : "",
        //                                FAI_BLOCK = reader["FAI_BLOCK"] != null ? reader["FAI_BLOCK"].ToString() : "",
        //                                RELEASE_UNTIL_DATE = reader["RELEASE_UNTIL_DATE"] != null ? reader["RELEASE_UNTIL_DATE"].ToString() : "",
        //                                TYPE_CODE = reader["TYPE_CODE"] != null ? reader["TYPE_CODE"].ToString() : "",
        //                                SPECIALHANDLING = reader["SPECIALHANDLING"] != null ? reader["SPECIALHANDLING"].ToString() : "",
        //                                MP_CALL_ID = reader["MP_CALL_ID"] != null ? reader["MP_CALL_ID"].ToString() : ""
        //                            }).ToList();
        //            }


        //        }
        //        return packages;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (con.State == ConnectionState.Open)
        //        {
        //            con.Close();
        //        }
        //        return null;
        //    }
        //    finally
        //    {
        //        if (con.State == ConnectionState.Open)
        //        {
        //            con.Close();
        //        }
        //    }

        //}

        public List<PackagesData> GetPackages(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetPackageData", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<PackagesData> packages = new List<PackagesData>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        packages = (from DataRow reader in dataset.Tables[0].Rows
                                    select new PackagesData()
                                    {

                                        PACKAGE_ID = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        PACKAGE_NUMBER = reader["PACKAGE_NUMBER"] != null ? reader["PACKAGE_NUMBER"].ToString() : "",
                                        SHIPMENT_ID = reader["SHIPMENT_ID"] != null ? reader["SHIPMENT_ID"].ToString() : "",
                                        STATUS = reader["STATUS"] != null ? reader["STATUS"].ToString() : "",
                                        STAGING_LANE = reader["STAGING_LANE"] != null ? reader["STAGING_LANE"].ToString() : "",
                                        QUANTITY = reader["QUANTITY"] != null ? reader["QUANTITY"].ToString() : "",
                                        SECONDARY_QUANTITY = reader["SECONDARY_QUANTITY"] != null ? reader["SECONDARY_QUANTITY"].ToString() : "",
                                        QUANTITY_RECEIVED = reader["QUANTITY_RECEIVED"] != null ? reader["QUANTITY_RECEIVED"].ToString() : "",
                                        REFERENCE = reader["REFERENCE"] != null ? reader["REFERENCE"].ToString() : "",
                                        REVISION = reader["REVISION"] != null ? reader["REVISION"].ToString() : "",
                                        INVOICE_PRICE = reader["INVOICE_PRICE"] != null ? reader["INVOICE_PRICE"].ToString() : "",
                                        HOST_KEY = reader["HOST_KEY"] != null ? reader["HOST_KEY"].ToString() : "",
                                        MIM_TRACKING_NUMBER = reader["MIM_TRACKING_NUMBER"] != null ? reader["MIM_TRACKING_NUMBER"].ToString() : "",
                                        MIM_FREIGHT_CHARGE = reader["MIM_FREIGHT_CHARGE"] != null ? reader["MIM_FREIGHT_CHARGE"].ToString() : "",
                                        MIM_CURRENCY = reader["MIM_CURRENCY"] != null ? reader["MIM_CURRENCY"].ToString() : "",
                                        MIM_WEIGHT = reader["MIM_WEIGHT"] != null ? reader["MIM_WEIGHT"].ToString() : "",
                                        ERP_CODE = reader["ERP_CODE"] != null ? reader["ERP_CODE"].ToString() : "",
                                        ERP_KEY_ID = reader["ERP_KEY_ID"] != null ? reader["ERP_KEY_ID"].ToString() : "",
                                        ERP_KEY_FIELD = reader["ERP_KEY_FIELD"] != null ? reader["ERP_KEY_FIELD"].ToString() : "",
                                        ERP_SHIP_TO_KEY = reader["ERP_SHIP_TO_KEY"] != null ? reader["ERP_SHIP_TO_KEY"].ToString() : "",
                                        ERP_SHIP_FROM_KEY = reader["ERP_SHIP_FROM_KEY"] != null ? reader["ERP_SHIP_FROM_KEY"].ToString() : "",
                                        ERP_ITEM_NUMBER = reader["ERP_ITEM_NUMBER"] != null ? reader["ERP_ITEM_NUMBER"].ToString() : "",
                                        ERP_ITEM_DESCRIPTION = reader["ERP_ITEM_DESCRIPTION"] != null ? reader["ERP_ITEM_DESCRIPTION"].ToString() : "",
                                        ERP_INVENTORY_ITEM_ID = reader["ERP_INVENTORY_ITEM_ID"] != null ? reader["ERP_INVENTORY_ITEM_ID"].ToString() : "",
                                        ERP_OPM_ITEM_ID = reader["ERP_OPM_ITEM_ID"] != null ? reader["ERP_OPM_ITEM_ID"].ToString() : "",
                                        ERP_ITEM_REV = reader["ERP_ITEM_REV"] != null ? reader["ERP_ITEM_REV"].ToString() : "",
                                        ERP_ITEM_LOT_CONTROLLED = reader["ERP_ITEM_LOT_CONTROLLED"] != null ? reader["ERP_ITEM_LOT_CONTROLLED"].ToString() : "",
                                        ERP_ITEM_SUBLOT_CONTROLLED = reader["ERP_ITEM_SUBLOT_CONTROLLED"] != null ? reader["ERP_ITEM_SUBLOT_CONTROLLED"].ToString() : "",
                                        ERP_ITEM_SERIAL_CONTROLLED = reader["ERP_ITEM_SERIAL_CONTROLLED"] != null ? reader["ERP_ITEM_SERIAL_CONTROLLED"].ToString() : "",
                                        ERP_ITEM_REV_CONTROLLED = reader["ERP_ITEM_REV_CONTROLLED"] != null ? reader["ERP_ITEM_REV_CONTROLLED"].ToString() : "",
                                        ERP_ITEM_UOM = reader["ERP_ITEM_UOM"] != null ? reader["ERP_ITEM_UOM"].ToString() : "",
                                        ERP_ITEM_SECONDARY_UOM = reader["ERP_ITEM_SECONDARY_UOM"] != null ? reader["ERP_ITEM_SECONDARY_UOM"].ToString() : "",
                                        ERP_ITEM_DUALUOM_CONTROL = reader["ERP_ITEM_DUALUOM_CONTROL"] != null ? reader["ERP_ITEM_DUALUOM_CONTROL"].ToString() : "",
                                        ERP_ITEM_WEIGHT = reader["ERP_ITEM_WEIGHT"] != null ? reader["ERP_ITEM_WEIGHT"].ToString() : "",
                                        ERP_ITEM_WEIGHT_UOM = reader["ERP_ITEM_WEIGHT_UOM"] != null ? reader["ERP_ITEM_WEIGHT_UOM"].ToString() : "",
                                        ERP_PO_NUMBER = reader["ERP_PO_NUMBER"] != null ? reader["ERP_PO_NUMBER"].ToString() : "",
                                        ERP_PO_NOTE = reader["ERP_PO_NOTE"] != null ? reader["ERP_PO_NOTE"].ToString() : "",
                                        ERP_PO_TYPE = reader["ERP_PO_TYPE"] != null ? reader["ERP_PO_TYPE"].ToString() : "",
                                        ERP_LINE_NUMBER = reader["ERP_LINE_NUMBER"] != null ? reader["ERP_LINE_NUMBER"].ToString() : "",
                                        ERP_VENDOR_NAME = reader["ERP_VENDOR_NAME"] != null ? reader["ERP_VENDOR_NAME"].ToString() : "",
                                        ERP_BUYER_NAME = reader["ERP_BUYER_NAME"] != null ? reader["ERP_BUYER_NAME"].ToString() : "",
                                        ERP_KANBAN_CARD = reader["ERP_KANBAN_CARD"] != null ? reader["ERP_KANBAN_CARD"].ToString() : "",
                                        ERP_SHIP_TO_NAME = reader["ERP_SHIP_TO_NAME"] != null ? reader["ERP_SHIP_TO_NAME"].ToString() : "",
                                        ERP_SHIP_TO_NAME_ID = reader["ERP_SHIP_TO_NAME_ID"] != null ? reader["ERP_SHIP_TO_NAME_ID"].ToString() : "",
                                        ERP_SHIP_TO_LOCATION = reader["ERP_SHIP_TO_LOCATION"] != null ? reader["ERP_SHIP_TO_LOCATION"].ToString() : "",
                                        ERP_SHIP_TO_LOCATION_ID = reader["ERP_SHIP_TO_LOCATION_ID"] != null ? reader["ERP_SHIP_TO_LOCATION_ID"].ToString() : "",
                                        ERP_SCHEDULE_NUMBER = reader["ERP_SCHEDULE_NUMBER"] != null ? reader["ERP_SCHEDULE_NUMBER"].ToString() : "",
                                        PO_LINE_LOCATION_ID = reader["PO_LINE_LOCATION_ID"] != null ? reader["PO_LINE_LOCATION_ID"].ToString() : "",
                                        INTERFACE_LOG = reader["INTERFACE_LOG"] != null ? reader["INTERFACE_LOG"].ToString() : "",
                                        INTERFACE_ERROR = reader["INTERFACE_ERROR"] != null ? reader["INTERFACE_ERROR"].ToString() : "",
                                        SO_PO_TRANSACTION_STATUS = reader["SO_PO_TRANSACTION_STATUS"] != null ? reader["SO_PO_TRANSACTION_STATUS"].ToString() : "",
                                        SCH_PROMISE_NUMBER = reader["SCH_PROMISE_NUMBER"] != null ? reader["SCH_PROMISE_NUMBER"].ToString() : "",
                                        VMI_AGREEMENT_NUMBER = reader["VMI_AGREEMENT_NUMBER"] != null ? reader["VMI_AGREEMENT_NUMBER"].ToString() : "",
                                        APPLICATION_CODE = reader["APPLICATION_CODE"] != null ? reader["APPLICATION_CODE"].ToString() : "",
                                        APPLICATION_KEY_ID = reader["APPLICATION_KEY_ID"] != null ? reader["APPLICATION_KEY_ID"].ToString() : "",
                                        //ATTRIBUTE1 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE2 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE3 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE4 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE5 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE6 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE7 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE8 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE9 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE10 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE11 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE12 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE13 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE14 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE15 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE16 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE17 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE18 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE19 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        //ATTRIBUTE20 = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                        CREATED_BY = reader["CREATED_BY"] != null ? reader["CREATED_BY"].ToString() : "",
                                        CREATION_DATE = reader["CREATION_DATE"] != null ? reader["CREATION_DATE"].ToString() : "",
                                        LAST_UPDATE_DATE = reader["LAST_UPDATE_DATE"] != null ? reader["LAST_UPDATE_DATE"].ToString() : "",
                                        LAST_UPDATE_BY = reader["LAST_UPDATE_BY"] != null ? reader["LAST_UPDATE_BY"].ToString() : "",
                                        VENDOR_MATERIAL = reader["VENDOR_MATERIAL"] != null ? reader["VENDOR_MATERIAL"].ToString() : "",
                                        COUNTRY_OF_ORIGIN_CODE = reader["COUNTRY_OF_ORIGIN_CODE"] != null ? reader["COUNTRY_OF_ORIGIN_CODE"].ToString() : "",
                                        COUNTRY_OF_ORIGIN_MARK = reader["COUNTRY_OF_ORIGIN_MARK"] != null ? reader["COUNTRY_OF_ORIGIN_MARK"].ToString() : "",
                                        REJECT_REASON = reader["REJECT_REASON"] != null ? reader["REJECT_REASON"].ToString() : "",
                                        FSL_USED = reader["FSL_USED"] != null ? reader["FSL_USED"].ToString() : "",
                                        BOX_PER_PACKAGE = reader["BOX_PER_PACKAGE"] != null ? reader["BOX_PER_PACKAGE"].ToString() : "",
                                        LAST_UPDATE_BY_NAME = reader["LAST_UPDATE_BY_NAME"] != null ? reader["LAST_UPDATE_BY_NAME"].ToString() : "",
                                        FAI_BLOCK = reader["FAI_BLOCK"] != null ? reader["FAI_BLOCK"].ToString() : "",
                                        RELEASE_UNTIL_DATE = reader["RELEASE_UNTIL_DATE"] != null ? reader["RELEASE_UNTIL_DATE"].ToString() : "",
                                        TYPE_CODE = reader["TYPE_CODE"] != null ? reader["TYPE_CODE"].ToString() : "",
                                        SPECIALHANDLING = reader["SPECIALHANDLING"] != null ? reader["SPECIALHANDLING"].ToString() : "",
                                        MP_CALL_ID = reader["MP_CALL_ID"] != null ? reader["MP_CALL_ID"].ToString() : ""
                                    }).ToList();
                    }


                }
                return packages;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }
        #endregion
    }
}
