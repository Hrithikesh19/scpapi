﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;
using UserDataAccess;

namespace SCP.DAL
{
    public class DemandDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;

        #endregion

        #region constructor

        private static readonly DemandDAL instance = new DemandDAL();   //singleton
        DataTable dt;
        static DemandDAL()
        {
        }
        private DemandDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static DemandDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Demand Purchase WorkBench Search 
        public List<DemandPurchaseWorkBenchData> GetDemandPurchaseWorkBenchData(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetDemandPurchaseWorkBenchData", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<DemandPurchaseWorkBenchData> schedules = new List<DemandPurchaseWorkBenchData>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        schedules = (from DataRow reader in dataset.Tables[0].Rows
                                     select new DemandPurchaseWorkBenchData()
                                     {

                                         // supplierName = Convert.ToInt64(reader["scheduleNumber"]),
                                         supplierName = reader["supplierName"] != null ? reader["supplierName"].ToString() : "",
                                         exloc8 = reader["exloc8"] != null ? reader["exloc8"].ToString() : "",
                                         org = reader["org"] != null ? reader["org"].ToString() : "",
                                         pc = reader["pc"] != null ? reader["pc"].ToString() : "",
                                         partNumber = reader["partNumber"] != null ? reader["partNumber"].ToString() : "",
                                         partDescription = reader["partDescription"] != null ? reader["partDescription"].ToString() : "",
                                         status = reader["status"] != null ? reader["status"].ToString() : "",
                                         numberOfDaysAging = reader["numberOfDaysAging"] != null ? reader["numberOfDaysAging"].ToString() : "",
                                         supplierShortToOpen = reader["supplierShortToOpen"] != null ? reader["supplierShortToOpen"].ToString() : "",
                                         stockoutShortTerm = reader["stockoutShortTerm"] != null ? reader["stockoutShortTerm"].ToString() : "",
                                         supplierShortToMrpMidTerm = reader["supplierShortToMrpMidTerm"] != null ? reader["supplierShortToMrpMidTerm"].ToString() : "",
                                         wipQtyShortLongTerm = reader["wipQtyShortLongTerm"] != null ? reader["wipQtyShortLongTerm"].ToString() : "",
                                         demandSpike = reader["demandSpike"] != null ? reader["demandSpike"].ToString() : "",
                                         belowMinWithNoOpenPo = reader["belowMinWithNoOpenPo"] != null ? reader["belowMinWithNoOpenPo"].ToString() : "",
                                         paceLtIssue = reader["paceLtIssue"] != null ? reader["paceLtIssue"].ToString() : "",
                                         qtyOverMax = reader["qtyOverMax"] != null ? reader["qtyOverMax"].ToString() : "",
                                         amat = reader["amat"] != null ? reader["amat"].ToString() : "",
                                         propsData = reader["propsData"] != null ? reader["propsData"].ToString() : "",
                                         openPoReleases = reader["openPoReleases"] != null ? reader["openPoReleases"].ToString() : "",
                                         exloc1 = reader["exloc1"] != null ? reader["exloc1"].ToString() : "",
                                         sci = reader["sci"] != null ? reader["sci"].ToString() : "",
                                         min = reader["min"] != null ? reader["min"].ToString() : "",
                                         max = reader["max"] != null ? reader["max"].ToString() : "",
                                         awu = reader["awu"] != null ? reader["awu"].ToString() : "",
                                         supplierFgi = reader["supplierFgi"] != null ? reader["supplierFgi"].ToString() : "",
                                         wipNext4Weeks = reader["wipNext4Weeks"] != null ? reader["wipNext4Weeks"].ToString() : "",
                                         wipWeeks5To8 = reader["wipWeeks5To8"] != null ? reader["wipWeeks5To8"].ToString() : "",
                                         exloc7 = reader["exloc7"] != null ? reader["exloc7"].ToString() : "",
                                         maxBu = reader["maxBu"] != null ? reader["maxBu"].ToString() : "",
                                         safetyStockLevel = reader["safetyStockLevel"] != null ? reader["safetyStockLevel"].ToString() : "",
                                         leadTime = reader["leadTime"] != null ? reader["leadTime"].ToString() : "",
                                         userItemType = reader["userItemType"] != null ? reader["userItemType"].ToString() : "",
                                         MOQ = reader["MOQ"] != null ? reader["MOQ"].ToString() : "",
                                         FOQ = reader["FOQ"] != null ? reader["FOQ"].ToString() : "",

                                     }).ToList();
                    }


                }
                return schedules;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion
        public DemandDetails GetDemandSearchForecast(PoOrderRequest request)
        {
            try
            {
                string itemNumbers = string.Empty;
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {

                        if (item.Condition.ToLower() == "contains")
                            strQuery += item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);
                if (strQuery == "")
                {
                    dt = SQLConnections.MyTable("select itemNumber from DemandItem");
                }
                else
                {
                    dt = SQLConnections.MyTable("select itemNumber from DemandItem where " + strQuery);
                }

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        itemNumbers = itemNumbers + "," + item["itemNumber"].ToString();
                    }

                }
                // itemNumbers.Remove(1,1);

                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetDemandDetails", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@itemNumber", itemNumbers, SqlDbType.NVarChar));

                DemandDetails demanddet = new DemandDetails();

                List<DemandFields> DemandItems = new List<DemandFields>();
                List<DemandFields> DemandCompany = new List<DemandFields>();
                List<DemandFields> DemandSupplier = new List<DemandFields>();
                List<DemandFields> DemandPlant = new List<DemandFields>();

                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        DemandItems = (from DataRow reader in dataset.Tables[0].Rows
                                       select new DemandFields()
                                       {
                                           itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                           itemType = reader["itemType"] != null ? reader["itemType"].ToString() : "",
                                           mrpController = reader["mrpController"] != null ? reader["mrpController"].ToString() : "",
                                           pace = reader["pace"] != null ? reader["pace"].ToString() : "",
                                           parentSupplierCompany = reader["parentSupplierCompany"] != null ? reader["parentSupplierCompany"].ToString() : "",
                                           plantName = reader["plantName"] != null ? reader["plantName"].ToString() : "",
                                           purchasingGroup = reader["purchasingGroup"] != null ? reader["purchasingGroup"].ToString() : "",
                                           supplierNumber = reader["supplierNumber"] != null ? reader["supplierNumber"].ToString() : ""
                                       }).ToList();

                        DemandCompany = (from DataRow reader in dataset.Tables[1].Rows
                                         select new DemandFields()
                                         {
                                             itemNumber = reader["ItemNumber"] != null ? reader["ItemNumber"].ToString() : "",
                                             CompanyName = reader["CompanyName"] != null ? reader["CompanyName"].ToString() : "",
                                             LMD = reader["LMD"] != null ? reader["LMD"].ToString() : "",
                                             TotalRequirements = reader["TotalRequirements"] != null ? reader["TotalRequirements"].ToString() : "",
                                             PastDueRequirements = reader["PastDueRequirements"] != null ? reader["PastDueRequirements"].ToString() : "",
                                             Measure = reader["Measure"] != null ? reader["Measure"].ToString() : "",
                                             W1 = reader["W1"] != null ? reader["W1"].ToString() : "",
                                             W2 = reader["W2"] != null ? reader["W2"].ToString() : "",
                                             W3 = reader["W3"] != null ? reader["W3"].ToString() : "",
                                             W4 = reader["W4"] != null ? reader["W4"].ToString() : "",
                                             W5 = reader["W5"] != null ? reader["W5"].ToString() : "",
                                             W6 = reader["W6"] != null ? reader["W6"].ToString() : "",
                                             W7 = reader["W7"] != null ? reader["W7"].ToString() : "",
                                             W8 = reader["W8"] != null ? reader["W8"].ToString() : "",
                                             W9 = reader["W9"] != null ? reader["W9"].ToString() : "",
                                             W10 = reader["W10"] != null ? reader["W10"].ToString() : "",
                                             W11 = reader["W11"] != null ? reader["W11"].ToString() : "",
                                             W12 = reader["W12"] != null ? reader["W12"].ToString() : "",
                                             W13 = reader["W13"] != null ? reader["W13"].ToString() : "",
                                             W14 = reader["W14"] != null ? reader["W14"].ToString() : "",
                                             W15 = reader["W15"] != null ? reader["W15"].ToString() : "",
                                             W16 = reader["W16"] != null ? reader["W16"].ToString() : "",
                                             W17 = reader["W17"] != null ? reader["W17"].ToString() : "",
                                             W18 = reader["W18"] != null ? reader["W18"].ToString() : "",
                                             W19 = reader["W19"] != null ? reader["W19"].ToString() : "",
                                             W20 = reader["W20"] != null ? reader["W20"].ToString() : "",
                                             W21 = reader["W21"] != null ? reader["W21"].ToString() : "",
                                             W22 = reader["W22"] != null ? reader["W22"].ToString() : "",
                                             W23 = reader["W23"] != null ? reader["W23"].ToString() : "",
                                             W24 = reader["W24"] != null ? reader["W24"].ToString() : "",
                                             W25 = reader["W25"] != null ? reader["W25"].ToString() : "",
                                             W26 = reader["W26"] != null ? reader["W26"].ToString() : "",
                                             W27 = reader["W27"] != null ? reader["W27"].ToString() : "",
                                             W28 = reader["W28"] != null ? reader["W28"].ToString() : "",
                                             W29 = reader["W29"] != null ? reader["W29"].ToString() : "",
                                             W30 = reader["W30"] != null ? reader["W30"].ToString() : "",
                                             W31 = reader["W31"] != null ? reader["W31"].ToString() : ""
                                         }).ToList();

                        DemandSupplier = (from DataRow reader in dataset.Tables[2].Rows
                                          select new DemandFields()
                                          {
                                              itemNumber = reader["ItemNumber"] != null ? reader["ItemNumber"].ToString() : "",
                                              VendorNumber = reader["VendorNumber"] != null ? reader["VendorNumber"].ToString() : "",
                                              LMD = reader["LMD"] != null ? reader["LMD"].ToString() : "",
                                              TotalRequirements = reader["TotalRequirements"] != null ? reader["TotalRequirements"].ToString() : "",
                                              PastDueRequirements = reader["PastDueRequirements"] != null ? reader["PastDueRequirements"].ToString() : "",
                                              Measure = reader["Measure"] != null ? reader["Measure"].ToString() : "",
                                              W1 = reader["W1"] != null ? reader["W1"].ToString() : "",
                                              W2 = reader["W2"] != null ? reader["W2"].ToString() : "",
                                              W3 = reader["W3"] != null ? reader["W3"].ToString() : "",
                                              W4 = reader["W4"] != null ? reader["W4"].ToString() : "",
                                              W5 = reader["W5"] != null ? reader["W5"].ToString() : "",
                                              W6 = reader["W6"] != null ? reader["W6"].ToString() : "",
                                              W7 = reader["W7"] != null ? reader["W7"].ToString() : "",
                                              W8 = reader["W8"] != null ? reader["W8"].ToString() : "",
                                              W9 = reader["W9"] != null ? reader["W9"].ToString() : "",
                                              W10 = reader["W10"] != null ? reader["W10"].ToString() : "",
                                              W11 = reader["W11"] != null ? reader["W11"].ToString() : "",
                                              W12 = reader["W12"] != null ? reader["W12"].ToString() : "",
                                              W13 = reader["W13"] != null ? reader["W13"].ToString() : "",
                                              W14 = reader["W14"] != null ? reader["W14"].ToString() : "",
                                              W15 = reader["W15"] != null ? reader["W15"].ToString() : "",
                                              W16 = reader["W16"] != null ? reader["W16"].ToString() : "",
                                              W17 = reader["W17"] != null ? reader["W17"].ToString() : "",
                                              W18 = reader["W18"] != null ? reader["W18"].ToString() : "",
                                              W19 = reader["W19"] != null ? reader["W19"].ToString() : "",
                                              W20 = reader["W20"] != null ? reader["W20"].ToString() : "",
                                              W21 = reader["W21"] != null ? reader["W21"].ToString() : "",
                                              W22 = reader["W22"] != null ? reader["W22"].ToString() : "",
                                              W23 = reader["W23"] != null ? reader["W23"].ToString() : "",
                                              W24 = reader["W24"] != null ? reader["W24"].ToString() : "",
                                              W25 = reader["W25"] != null ? reader["W25"].ToString() : "",
                                              W26 = reader["W26"] != null ? reader["W26"].ToString() : "",
                                              W27 = reader["W27"] != null ? reader["W27"].ToString() : "",
                                              W28 = reader["W28"] != null ? reader["W28"].ToString() : "",
                                              W29 = reader["W29"] != null ? reader["W29"].ToString() : "",
                                              W30 = reader["W30"] != null ? reader["W30"].ToString() : "",
                                              W31 = reader["W31"] != null ? reader["W31"].ToString() : ""
                                          }).ToList();

                        DemandPlant = (from DataRow reader in dataset.Tables[3].Rows
                                       select new DemandFields()
                                       {
                                           itemNumber = reader["ItemNumber"] != null ? reader["ItemNumber"].ToString() : "",
                                           VendorNumber = reader["VendorNumber"] != null ? reader["VendorNumber"].ToString() : "",
                                           plantName = reader["PlantName"] != null ? reader["PlantName"].ToString() : "",
                                           LMD = reader["LMD"] != null ? reader["LMD"].ToString() : "",
                                           pace = reader["Pace"] != null ? reader["Pace"].ToString() : "",
                                           PaceDescription = reader["PaceDescription"] != null ? reader["PaceDescription"].ToString() : "",
                                           AWU = reader["AWU"] != null ? reader["AWU"].ToString() : "",
                                           Min = reader["Min"] != null ? reader["Min"].ToString() : "",
                                           Max = reader["Max"] != null ? reader["Max"].ToString() : "",

                                           Onhand = reader["Onhand"] != null ? reader["Onhand"].ToString() : "",
                                           itemType = reader["ItemType"] != null ? reader["ItemType"].ToString() : "",
                                           Measure = reader["Measure"] != null ? reader["Measure"].ToString() : "",
                                           W1 = reader["W1"] != null ? reader["W1"].ToString() : "",
                                           W2 = reader["W2"] != null ? reader["W2"].ToString() : "",
                                           W3 = reader["W3"] != null ? reader["W3"].ToString() : "",
                                           W4 = reader["W4"] != null ? reader["W4"].ToString() : "",
                                           W5 = reader["W5"] != null ? reader["W5"].ToString() : "",
                                           W6 = reader["W6"] != null ? reader["W6"].ToString() : "",
                                           W7 = reader["W7"] != null ? reader["W7"].ToString() : "",
                                           W8 = reader["W8"] != null ? reader["W8"].ToString() : "",
                                           W9 = reader["W9"] != null ? reader["W9"].ToString() : "",
                                           W10 = reader["W10"] != null ? reader["W10"].ToString() : "",
                                           W11 = reader["W11"] != null ? reader["W11"].ToString() : "",
                                           W12 = reader["W12"] != null ? reader["W12"].ToString() : "",
                                           W13 = reader["W13"] != null ? reader["W13"].ToString() : "",
                                           W14 = reader["W14"] != null ? reader["W14"].ToString() : "",
                                           W15 = reader["W15"] != null ? reader["W15"].ToString() : "",
                                           W16 = reader["W16"] != null ? reader["W16"].ToString() : "",
                                           W17 = reader["W17"] != null ? reader["W17"].ToString() : "",
                                           W18 = reader["W18"] != null ? reader["W18"].ToString() : "",
                                           W19 = reader["W19"] != null ? reader["W19"].ToString() : "",
                                           W20 = reader["W20"] != null ? reader["W20"].ToString() : "",
                                           W21 = reader["W21"] != null ? reader["W21"].ToString() : "",
                                           W22 = reader["W22"] != null ? reader["W22"].ToString() : "",
                                           W23 = reader["W23"] != null ? reader["W23"].ToString() : "",
                                           W24 = reader["W24"] != null ? reader["W24"].ToString() : "",
                                           W25 = reader["W25"] != null ? reader["W25"].ToString() : "",
                                           W26 = reader["W26"] != null ? reader["W26"].ToString() : "",
                                           W27 = reader["W27"] != null ? reader["W27"].ToString() : "",
                                           W28 = reader["W28"] != null ? reader["W28"].ToString() : "",
                                           W29 = reader["W29"] != null ? reader["W29"].ToString() : "",
                                           W30 = reader["W30"] != null ? reader["W30"].ToString() : "",
                                           W31 = reader["W31"] != null ? reader["W31"].ToString() : ""

                                       }).ToList();
                    }
                }
                demanddet.DemandItems = DemandItems;
                demanddet.DemandCompany = DemandCompany;
                demanddet.DemandPlant = DemandPlant;
                demanddet.DemandSupplier = DemandSupplier;

                return demanddet;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #region Get Demand Item Attributes 
        public List<DemandItemAttributes> GetDemandAttributes(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetDemandItemAttributes", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<DemandItemAttributes> schedules = new List<DemandItemAttributes>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        schedules = (from DataRow reader in dataset.Tables[0].Rows
                                     select new DemandItemAttributes()
                                     {

                                         plantName = reader["plantName"] != null ? reader["plantName"].ToString() : "",
                                         supplierName = reader["supplierName"] != null ? reader["supplierName"].ToString() : "",
                                         itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                         itemDescription = reader["itemDescription"] != null ? reader["itemDescription"].ToString() : "",
                                         Revision = reader["Revision"] != null ? reader["Revision"].ToString() : "",
                                         amatOnHand = reader["amatOnHand"] != null ? reader["amatOnHand"].ToString() : "",
                                         consignedOnHand = reader["consignedOnHand"] != null ? reader["consignedOnHand"].ToString() : "",
                                         purchasingGroup = reader["purchasingGroup"] != null ? reader["purchasingGroup"].ToString() : "",
                                         safetyStockReq = reader["safetyStockReq"] != null ? reader["safetyStockReq"].ToString() : "",
                                         serializedPart = reader["serializedPart"] != null ? reader["serializedPart"].ToString() : "",
                                         specProcKey = reader["specProcKey"] != null ? reader["specProcKey"].ToString() : "",
                                         unitOfMeasure = reader["unitOfMeasure"] != null ? reader["unitOfMeasure"].ToString() : ""

                                     }).ToList();
                    }


                }
                return schedules;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion


        public DemandDetails GetDemandDetails(string itemNumber)
        {
            try
            {
                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetDemandDetails", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@itemNumber", itemNumber, SqlDbType.NVarChar));

                DemandDetails demanddet = new DemandDetails();

                List<DemandFields> DemandItems = new List<DemandFields>();
                List<DemandFields> DemandCompany = new List<DemandFields>();
                List<DemandFields> DemandSupplier = new List<DemandFields>();
                List<DemandFields> DemandPlant = new List<DemandFields>();

                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        DemandItems = (from DataRow reader in dataset.Tables[0].Rows
                                       select new DemandFields()
                                       {
                                           itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                           itemType = reader["itemType"] != null ? reader["itemType"].ToString() : "",
                                           mrpController = reader["mrpController"] != null ? reader["mrpController"].ToString() : "",
                                           pace = reader["pace"] != null ? reader["pace"].ToString() : "",
                                           parentSupplierCompany = reader["parentSupplierCompany"] != null ? reader["parentSupplierCompany"].ToString() : "",
                                           plantName = reader["plantName"] != null ? reader["plantName"].ToString() : "",
                                           purchasingGroup = reader["purchasingGroup"] != null ? reader["purchasingGroup"].ToString() : "",
                                           supplierNumber = reader["supplierNumber"] != null ? reader["supplierNumber"].ToString() : ""
                                       }).ToList();

                        DemandCompany = (from DataRow reader in dataset.Tables[1].Rows
                                         select new DemandFields()
                                         {
                                             itemNumber = reader["ItemNumber"] != null ? reader["ItemNumber"].ToString() : "",
                                             CompanyName = reader["CompanyName"] != null ? reader["CompanyName"].ToString() : "",
                                             LMD = reader["LMD"] != null ? reader["LMD"].ToString() : "",
                                             TotalRequirements = reader["TotalRequirements"] != null ? reader["TotalRequirements"].ToString() : "",
                                             PastDueRequirements = reader["PastDueRequirements"] != null ? reader["PastDueRequirements"].ToString() : "",
                                             Measure = reader["Measure"] != null ? reader["Measure"].ToString() : "",
                                             W1 = reader["W1"] != null ? reader["W1"].ToString() : "",
                                             W2 = reader["W2"] != null ? reader["W2"].ToString() : "",
                                             W3 = reader["W3"] != null ? reader["W3"].ToString() : "",
                                             W4 = reader["W4"] != null ? reader["W4"].ToString() : "",
                                             W5 = reader["W5"] != null ? reader["W5"].ToString() : "",
                                             W6 = reader["W6"] != null ? reader["W6"].ToString() : "",
                                             W7 = reader["W7"] != null ? reader["W7"].ToString() : "",
                                             W8 = reader["W8"] != null ? reader["W8"].ToString() : "",
                                             W9 = reader["W9"] != null ? reader["W9"].ToString() : "",
                                             W10 = reader["W10"] != null ? reader["W10"].ToString() : "",
                                             W11 = reader["W11"] != null ? reader["W11"].ToString() : "",
                                             W12 = reader["W12"] != null ? reader["W12"].ToString() : "",
                                             W13 = reader["W13"] != null ? reader["W13"].ToString() : "",
                                             W14 = reader["W14"] != null ? reader["W14"].ToString() : "",
                                             W15 = reader["W15"] != null ? reader["W15"].ToString() : "",
                                             W16 = reader["W16"] != null ? reader["W16"].ToString() : "",
                                             W17 = reader["W17"] != null ? reader["W17"].ToString() : "",
                                             W18 = reader["W18"] != null ? reader["W18"].ToString() : "",
                                             W19 = reader["W19"] != null ? reader["W19"].ToString() : "",
                                             W20 = reader["W20"] != null ? reader["W20"].ToString() : "",
                                             W21 = reader["W21"] != null ? reader["W21"].ToString() : "",
                                             W22 = reader["W22"] != null ? reader["W22"].ToString() : "",
                                             W23 = reader["W23"] != null ? reader["W23"].ToString() : "",
                                             W24 = reader["W24"] != null ? reader["W24"].ToString() : "",
                                             W25 = reader["W25"] != null ? reader["W25"].ToString() : "",
                                             W26 = reader["W26"] != null ? reader["W26"].ToString() : "",
                                             W27 = reader["W27"] != null ? reader["W27"].ToString() : "",
                                             W28 = reader["W28"] != null ? reader["W28"].ToString() : "",
                                             W29 = reader["W29"] != null ? reader["W29"].ToString() : "",
                                             W30 = reader["W30"] != null ? reader["W30"].ToString() : "",
                                             W31 = reader["W31"] != null ? reader["W31"].ToString() : ""
                                         }).ToList();

                        DemandSupplier = (from DataRow reader in dataset.Tables[2].Rows
                                          select new DemandFields()
                                          {
                                              itemNumber = reader["ItemNumber"] != null ? reader["ItemNumber"].ToString() : "",
                                              VendorNumber = reader["VendorNumber"] != null ? reader["VendorNumber"].ToString() : "",
                                              LMD = reader["LMD"] != null ? reader["LMD"].ToString() : "",
                                              TotalRequirements = reader["TotalRequirements"] != null ? reader["TotalRequirements"].ToString() : "",
                                              PastDueRequirements = reader["PastDueRequirements"] != null ? reader["PastDueRequirements"].ToString() : "",
                                              Measure = reader["Measure"] != null ? reader["Measure"].ToString() : "",
                                              W1 = reader["W1"] != null ? reader["W1"].ToString() : "",
                                              W2 = reader["W2"] != null ? reader["W2"].ToString() : "",
                                              W3 = reader["W3"] != null ? reader["W3"].ToString() : "",
                                              W4 = reader["W4"] != null ? reader["W4"].ToString() : "",
                                              W5 = reader["W5"] != null ? reader["W5"].ToString() : "",
                                              W6 = reader["W6"] != null ? reader["W6"].ToString() : "",
                                              W7 = reader["W7"] != null ? reader["W7"].ToString() : "",
                                              W8 = reader["W8"] != null ? reader["W8"].ToString() : "",
                                              W9 = reader["W9"] != null ? reader["W9"].ToString() : "",
                                              W10 = reader["W10"] != null ? reader["W10"].ToString() : "",
                                              W11 = reader["W11"] != null ? reader["W11"].ToString() : "",
                                              W12 = reader["W12"] != null ? reader["W12"].ToString() : "",
                                              W13 = reader["W13"] != null ? reader["W13"].ToString() : "",
                                              W14 = reader["W14"] != null ? reader["W14"].ToString() : "",
                                              W15 = reader["W15"] != null ? reader["W15"].ToString() : "",
                                              W16 = reader["W16"] != null ? reader["W16"].ToString() : "",
                                              W17 = reader["W17"] != null ? reader["W17"].ToString() : "",
                                              W18 = reader["W18"] != null ? reader["W18"].ToString() : "",
                                              W19 = reader["W19"] != null ? reader["W19"].ToString() : "",
                                              W20 = reader["W20"] != null ? reader["W20"].ToString() : "",
                                              W21 = reader["W21"] != null ? reader["W21"].ToString() : "",
                                              W22 = reader["W22"] != null ? reader["W22"].ToString() : "",
                                              W23 = reader["W23"] != null ? reader["W23"].ToString() : "",
                                              W24 = reader["W24"] != null ? reader["W24"].ToString() : "",
                                              W25 = reader["W25"] != null ? reader["W25"].ToString() : "",
                                              W26 = reader["W26"] != null ? reader["W26"].ToString() : "",
                                              W27 = reader["W27"] != null ? reader["W27"].ToString() : "",
                                              W28 = reader["W28"] != null ? reader["W28"].ToString() : "",
                                              W29 = reader["W29"] != null ? reader["W29"].ToString() : "",
                                              W30 = reader["W30"] != null ? reader["W30"].ToString() : "",
                                              W31 = reader["W31"] != null ? reader["W31"].ToString() : ""
                                          }).ToList();

                        DemandPlant = (from DataRow reader in dataset.Tables[3].Rows
                                       select new DemandFields()
                                       {
                                           itemNumber = reader["ItemNumber"] != null ? reader["ItemNumber"].ToString() : "",
                                           VendorNumber = reader["VendorNumber"] != null ? reader["VendorNumber"].ToString() : "",
                                           plantName = reader["PlantName"] != null ? reader["PlantName"].ToString() : "",
                                           LMD = reader["LMD"] != null ? reader["LMD"].ToString() : "",
                                           pace = reader["Pace"] != null ? reader["Pace"].ToString() : "",
                                           PaceDescription = reader["PaceDescription"] != null ? reader["PaceDescription"].ToString() : "",
                                           AWU = reader["AWU"] != null ? reader["AWU"].ToString() : "",
                                           Min = reader["Min"] != null ? reader["Min"].ToString() : "",
                                           Max = reader["Max"] != null ? reader["Max"].ToString() : "",

                                           Onhand = reader["Onhand"] != null ? reader["Onhand"].ToString() : "",
                                           itemType = reader["ItemType"] != null ? reader["ItemType"].ToString() : "",
                                           Measure = reader["Measure"] != null ? reader["Measure"].ToString() : "",
                                           W1 = reader["W1"] != null ? reader["W1"].ToString() : "",
                                           W2 = reader["W2"] != null ? reader["W2"].ToString() : "",
                                           W3 = reader["W3"] != null ? reader["W3"].ToString() : "",
                                           W4 = reader["W4"] != null ? reader["W4"].ToString() : "",
                                           W5 = reader["W5"] != null ? reader["W5"].ToString() : "",
                                           W6 = reader["W6"] != null ? reader["W6"].ToString() : "",
                                           W7 = reader["W7"] != null ? reader["W7"].ToString() : "",
                                           W8 = reader["W8"] != null ? reader["W8"].ToString() : "",
                                           W9 = reader["W9"] != null ? reader["W9"].ToString() : "",
                                           W10 = reader["W10"] != null ? reader["W10"].ToString() : "",
                                           W11 = reader["W11"] != null ? reader["W11"].ToString() : "",
                                           W12 = reader["W12"] != null ? reader["W12"].ToString() : "",
                                           W13 = reader["W13"] != null ? reader["W13"].ToString() : "",
                                           W14 = reader["W14"] != null ? reader["W14"].ToString() : "",
                                           W15 = reader["W15"] != null ? reader["W15"].ToString() : "",
                                           W16 = reader["W16"] != null ? reader["W16"].ToString() : "",
                                           W17 = reader["W17"] != null ? reader["W17"].ToString() : "",
                                           W18 = reader["W18"] != null ? reader["W18"].ToString() : "",
                                           W19 = reader["W19"] != null ? reader["W19"].ToString() : "",
                                           W20 = reader["W20"] != null ? reader["W20"].ToString() : "",
                                           W21 = reader["W21"] != null ? reader["W21"].ToString() : "",
                                           W22 = reader["W22"] != null ? reader["W22"].ToString() : "",
                                           W23 = reader["W23"] != null ? reader["W23"].ToString() : "",
                                           W24 = reader["W24"] != null ? reader["W24"].ToString() : "",
                                           W25 = reader["W25"] != null ? reader["W25"].ToString() : "",
                                           W26 = reader["W26"] != null ? reader["W26"].ToString() : "",
                                           W27 = reader["W27"] != null ? reader["W27"].ToString() : "",
                                           W28 = reader["W28"] != null ? reader["W28"].ToString() : "",
                                           W29 = reader["W29"] != null ? reader["W29"].ToString() : "",
                                           W30 = reader["W30"] != null ? reader["W30"].ToString() : "",
                                           W31 = reader["W31"] != null ? reader["W31"].ToString() : ""

                                       }).ToList();
                    }
                }
                demanddet.DemandItems = DemandItems;
                demanddet.DemandCompany = DemandCompany;
                demanddet.DemandPlant = DemandPlant;
                demanddet.DemandSupplier = DemandSupplier;

                return demanddet;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }


    }
}
