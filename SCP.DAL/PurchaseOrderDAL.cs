﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;
using UserDataAccess;

namespace SCP.DAL
{
    public class PurchaseOrderDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;

        #endregion

        #region constructor

        private static readonly PurchaseOrderDAL instance = new PurchaseOrderDAL();   //singleton
        DataTable dt;
        static PurchaseOrderDAL()
        {
        }
        private PurchaseOrderDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static PurchaseOrderDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get PurchaseOrder Search 
        public List<PurchaseOrderData> GetPurchaseOrderData(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;                                
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if(dt.Rows.Count>0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetPurchaseOrderData", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
               // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<PurchaseOrderData> packages = new List<PurchaseOrderData>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        packages = (from DataRow reader in dataset.Tables[0].Rows
                                    select new PurchaseOrderData()
                                    {

                                        poNumber = Convert.ToInt64(reader["poNumber"]),
                                        lineNumer = reader["lineNumer"] != null ? reader["lineNumer"].ToString() : "",
                                        shipmentNumber = reader["shipmentNumber"] != null ? reader["shipmentNumber"].ToString() : "",
                                        xpcPoNumber = reader["xpcPoNumber"] != null ? reader["xpcPoNumber"].ToString() : "",
                                        itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                        itemDescription = reader["itemDescription"] != null ? reader["itemDescription"].ToString() : "",
                                        itemCategory = reader["itemCategory"] != null ? reader["itemCategory"].ToString() : "",
                                        statDelDate = reader["statDelDate"] != null ? reader["statDelDate"].ToString() : "",
                                        deliveryDate = reader["deliveryDate"] != null ? reader["deliveryDate"].ToString() : "",
                                        buyerName = reader["buyerName"] != null ? reader["buyerName"].ToString() : "",
                                        purchasingGroupKey = reader["purchasingGroupKey"] != null ? reader["purchasingGroupKey"].ToString() : "",
                                        startDate = reader["startDate"] != null ? reader["startDate"].ToString() : "",
                                        status = reader["status"] != null ? reader["status"].ToString() : "",
                                        erpPoStatusCode = reader["erpPoStatusCode"] != null ? reader["erpPoStatusCode"].ToString() : "",
                                        reasonForOrderingDesc = reader["reasonForOrderingDesc"] != null ? reader["reasonForOrderingDesc"].ToString() : "",
                                        vendorNumber = reader["vendorNumber"] != null ? reader["vendorNumber"].ToString() : "",
                                        purchasingOrgNum = reader["purchasingOrgNum"] != null ? reader["purchasingOrgNum"].ToString() : "",
                                        poLineLocationId = reader["poLineLocationId"] != null ? reader["poLineLocationId"].ToString() : "",
                                        accountAssignment = reader["accountAssignment"] != null ? reader["accountAssignment"].ToString() : "",
                                        quantity = reader["quantity"] != null ? reader["quantity"].ToString() : "",
                                        openQuantity = reader["openQuantity"] != null ? reader["openQuantity"].ToString() : "",
                                        quantityReceived = reader["quantityReceived"] != null ? reader["quantityReceived"].ToString() : "",
                                        bomReviewed = reader["bomReviewed"] != null ? reader["bomReviewed"].ToString() : "",
                                        bomChanged = reader["bomChanged"] != null ? reader["bomChanged"].ToString() : "",
                                        mrpAction = reader["mrpAction"] != null ? reader["mrpAction"].ToString() : "",
                                        vendorName = reader["vendorName"] != null ? reader["vendorName"].ToString() : "",
                                        materialGroup = reader["materialGroup"] != null ? reader["materialGroup"].ToString() : "",
                                        faiBlock = reader["faiBlock"] != null ? reader["faiBlock"].ToString() : "",
                                        lineItemId = reader["lineItemId"] != null ? reader["lineItemId"].ToString() : "",
                                        promiseddatetime = reader["promiseddatetime"] != null ? reader["promiseddatetime"].ToString() : "",
                                        itemId = reader["itemId"] != null ? reader["itemId"].ToString() : "",
                                        shipToLocationId = reader["shipToLocationId"] != null ? reader["shipToLocationId"].ToString() : "",
                                        billToLocationId = reader["billToLocationId"] != null ? reader["billToLocationId"].ToString() : "",
                                        poType = reader["poType"] != null ? reader["poType"].ToString() : "",
                                        releaseNumber = reader["releaseNumber"] != null ? reader["releaseNumber"].ToString() : "",
                                        erpCode = reader["erpCode"] != null ? reader["erpCode"].ToString() : "",
                                        printable = reader["printable"] != null ? reader["printable"].ToString() : "",
                                        key_1 = reader["key_1"] != null ? reader["key_1"].ToString() : "",
                                        shipToId = reader["shipToId"] != null ? reader["shipToId"].ToString() : "",
                                        poHeaderId = reader["poHeaderId"] != null ? reader["poHeaderId"].ToString() : "",
                                        kanbanCard = reader["kanbanCard"] != null ? reader["kanbanCard"].ToString() : "",
                                        needBydatetime = reader["needBydatetime"] != null ? reader["needBydatetime"].ToString() : "",
                                        unitOfMeasure = reader["unitOfMeasure"] != null ? reader["unitOfMeasure"].ToString() : "",
                                        price = reader["price"] != null ? reader["price"].ToString() : "",
                                        extendedPrice = reader["extendedPrice"] != null ? reader["extendedPrice"].ToString() : "",
                                        quantityOpen = reader["quantityOpen"] != null ? reader["quantityOpen"].ToString() : "",
                                        freightTerms = reader["freightTerms"] != null ? reader["freightTerms"].ToString() : "",
                                        fob = reader["fob"] != null ? reader["fob"].ToString() : "",
                                        shipVia = reader["shipVia"] != null ? reader["shipVia"].ToString() : "",
                                        currency = reader["currency"] != null ? reader["currency"].ToString() : "",
                                        paymentTermsName = reader["paymentTermsName"] != null ? reader["paymentTermsName"].ToString() : "",
                                        lineDescription = reader["lineDescription"] != null ? reader["lineDescription"].ToString() : "",
                                        lineNumber = reader["lineNumber"] != null ? reader["lineNumber"].ToString() : "",
                                        buyerTelephone = reader["buyerTelephone"] != null ? reader["buyerTelephone"].ToString() : "",
                                        buyerEmail = reader["buyerEmail"] != null ? reader["buyerEmail"].ToString() : "",
                                        shipToName = reader["shipToName"] != null ? reader["shipToName"].ToString() : "",
                                        itemUnitWeight = reader["itemUnitWeight"] != null ? reader["itemUnitWeight"].ToString() : "",
                                        itemWeightUOMCode = reader["itemWeightUOMCode"] != null ? reader["itemWeightUOMCode"].ToString() : "",
                                        itemLotControl = reader["itemLotControl"] != null ? reader["itemLotControl"].ToString() : "",
                                        itemSerialControl = reader["itemSerialControl"] != null ? reader["itemSerialControl"].ToString() : "",
                                        cancelReason = reader["cancelReason"] != null ? reader["cancelReason"].ToString() : "",
                                        poRevision = reader["poRevision"] != null ? reader["poRevision"].ToString() : "",
                                        vendorCustomerNumber = reader["vendorCustomerNumber"] != null ? reader["vendorCustomerNumber"].ToString() : "",
                                        startdatetime = reader["startdatetime"] != null ? reader["startdatetime"].ToString() : "",
                                        enddatetime = reader["enddatetime"] != null ? reader["enddatetime"].ToString() : "",
                                        blanketTotalAmount = reader["blanketTotalAmount"] != null ? reader["blanketTotalAmount"].ToString() : "",
                                        hostId = reader["hostId"] != null ? reader["hostId"].ToString() : "",
                                        poLineId = reader["poLineId"] != null ? reader["poLineId"].ToString() : "",
                                        shipmentNum = reader["shipmentNum"] != null ? reader["shipmentNum"].ToString() : "",
                                        deliverydatetime = reader["deliverydatetime"] != null ? reader["deliverydatetime"].ToString() : "",
                                        poLineDistributionId = reader["poLineDistributionId"] != null ? reader["poLineDistributionId"].ToString() : "",
                                        colorField = reader["colorField"] != null ? reader["colorField"].ToString() : "",
                                        itemCategoryDesc = reader["itemCategoryDesc"] != null ? reader["itemCategoryDesc"].ToString() : "",
                                        reasonForOrderingCode = reader["reasonForOrderingCode"] != null ? reader["reasonForOrderingCode"].ToString() : "",
                                        purchasingOrgDesc = reader["purchasingOrgDesc"] != null ? reader["purchasingOrgDesc"].ToString() : "",
                                        shippingInstrKey = reader["shippingInstrKey"] != null ? reader["shippingInstrKey"].ToString() : "",
                                        shippingInstrDesc = reader["shippingInstrDesc"] != null ? reader["shippingInstrDesc"].ToString() : "",
                                        qaControlKey = reader["qaControlKey"] != null ? reader["qaControlKey"].ToString() : "",
                                        blockFunctionDesc = reader["blockFunctionDesc"] != null ? reader["blockFunctionDesc"].ToString() : "",
                                        plannedDeliveryTime = reader["plannedDeliveryTime"] != null ? reader["plannedDeliveryTime"].ToString() : "",
                                        contractNumber = reader["contractNumber"] != null ? reader["contractNumber"].ToString() : "",
                                        contractLineNumber = reader["contractLineNumber"] != null ? reader["contractLineNumber"].ToString() : "",
                                        priceUnit = reader["priceUnit"] != null ? reader["priceUnit"].ToString() : "",
                                        orderPriceUnit = reader["orderPriceUnit"] != null ? reader["orderPriceUnit"].ToString() : "",
                                        poItemText = reader["poItemText"] != null ? reader["poItemText"].ToString() : "",
                                        deliveryToAddress = reader["deliveryToAddress"] != null ? reader["deliveryToAddress"].ToString() : "",
                                        rmaStatus = reader["rmaStatus"] != null ? reader["rmaStatus"].ToString() : "",
                                        eccn = reader["eccn"] != null ? reader["eccn"].ToString() : "",
                                        htsNumber = reader["htsNumber"] != null ? reader["htsNumber"].ToString() : "",
                                        mrpSuggesteddatetime = reader["mrpSuggesteddatetime"] != null ? reader["mrpSuggesteddatetime"].ToString() : "",
                                        newMrpSug = reader["newMrpSug"] != null ? reader["newMrpSug"].ToString() : "",
                                        acknowledgeRequired = reader["acknowledgeRequired"] != null ? reader["acknowledgeRequired"].ToString() : "",
                                        storageLocation = reader["storageLocation"] != null ? reader["storageLocation"].ToString() : "",
                                        accountAssignmentDesc = reader["accountAssignmentDesc"] != null ? reader["accountAssignmentDesc"].ToString() : "",
                                        bomReviewdatetime = reader["bomReviewdatetime"] != null ? reader["bomReviewdatetime"].ToString() : "",
                                        bomLastChangeddatetime = reader["bomLastChangeddatetime"] != null ? reader["bomLastChangeddatetime"].ToString() : "",
                                        serialNumber = reader["serialNumber"] != null ? reader["serialNumber"].ToString() : "",
                                        statDeldatetime = reader["statDeldatetime"] != null ? reader["statDeldatetime"].ToString() : "",
                                        bomRevision = reader["bomRevision"] != null ? reader["bomRevision"].ToString() : "",
                                        toolNumber = reader["toolNumber"] != null ? reader["toolNumber"].ToString() : "",
                                        needBydatetimeMca = reader["needBydatetimeMca"] != null ? reader["needBydatetimeMca"].ToString() : "",
                                        priority1 = reader["priority1"] != null ? reader["priority1"].ToString() : "",
                                        priority2 = reader["priority2"] != null ? reader["priority2"].ToString() : "",
                                        linedown = reader["linedown"] != null ? reader["linedown"].ToString() : ""
                                    }).ToList();
                    }


                }
                return packages;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion
    }
}
