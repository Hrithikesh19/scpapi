﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;

namespace SCP.DAL
{
    public class FoldersDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

       SqlConnection con = null;

        #endregion

        #region constructor

        private static readonly FoldersDAL instance = new FoldersDAL();   //singleton

        static FoldersDAL()
        {
        }
        private FoldersDAL()
        {
            objSQLHelper = new SqlHelper();
            
        }


        public static FoldersDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Folders 
        public List<Folders> GetFolders(string pageName)
        {
            try
            {
                string spname = string.Empty;
                if(pageName == "FIND_ORDERS")
                {
                    spname = "GetFolders";
                }
                else if (pageName == "VIEW_PTN" || pageName == "FIND_SHIPMENTS")
                {
                    spname = "GetPackageFolders";
                }
                else if (pageName == "FIND_SCHEDULES" || pageName == "FIND_SCHEDULE_SUMMARY" || pageName == "FIND_SCHEDULES_EDIT_DATE")
                {
                    spname = "GetSCHFolders";
                }
                else if (pageName == "FIND_DEMAND" || pageName == "FIND_PURCHASING_WORKBENCH" || pageName == "FIND_ITEMS")
                {
                    spname = "GetDMDFolders";
                }
                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand(spname, con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@pageName", pageName, SqlDbType.VarChar));

                List<Folders> folders = new List<Folders>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        folders = (from DataRow reader in dataset.Tables[0].Rows
                                      select new Folders()
                                      {
                                          folder_id = Convert.ToInt32(reader["folder_id"].ToString()),
                                          folder_name = reader["folder_name"] != null ? reader["folder_name"].ToString() : "",
                                          page_id = Convert.ToInt32(reader["page_id"].ToString())
                                      }).ToList();
                    }

                   
                }
                return folders;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }
      
        #endregion
    }
}
