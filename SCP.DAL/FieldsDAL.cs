﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;

namespace SCP.DAL
{
    public class FieldsDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;

        #endregion


        #region constructor

        private static readonly FieldsDAL instance = new FieldsDAL();   //singleton

        static FieldsDAL()
        {
        }
        private FieldsDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static FieldsDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Fields 
        public List<Fields> GetFields(int folderid)
        {
            try
            {

                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetFields", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@folder_id", folderid, SqlDbType.Int));

                List<Fields> fields = new List<Fields>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        fields = (from DataRow reader in dataset.Tables[0].Rows
                                  select new Fields()
                                  {
                                      folder_id = Convert.ToInt32(reader["field_id"].ToString()),
                                      field_namespace_id = Convert.ToInt32(reader["field_namespace_id"].ToString()),
                                      field_name = reader["field_name"] != null ? reader["field_name"].ToString() : "",
                                      description = reader["description"] != null ? reader["description"].ToString() : "",
                                      controltype = reader["control_type"] != null ? reader["control_type"].ToString() : "",
                                      field_type = reader["field_type"] != null ? reader["field_type"].ToString() : ""

                                  }).ToList();
                    }


                }
                return fields;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion


        #region Get Folders Fields
        public FolderDetails GetFolderFields(string PageName, int folderid)
        {
            try
            {

                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetFolderDetails", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@PageName", PageName, SqlDbType.NVarChar));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@folder_id", folderid, SqlDbType.Int));

                FolderDetails folderDetails = new FolderDetails();
                List<FolderFields> AvailableFields = new List<FolderFields>();
                List<FolderFields> SelectedFields = new List<FolderFields>();
                List<FolderFields> OrderByFields = new List<FolderFields>();

                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        
                        AvailableFields = (from DataRow reader in dataset.Tables[1].Rows
                                           select new FolderFields()
                                           {
                                               field_id = Convert.ToInt32(reader["field_id"].ToString()),
                                               field_namespace_id = Convert.ToInt32(reader["field_namespace_id"].ToString()),
                                               displayText = reader["field_name"] != null ? reader["field_name"].ToString() : ""
                                           }).ToList();

                        SelectedFields = (from DataRow reader in dataset.Tables[0].Rows
                                          select new FolderFields()
                                          {
                                              field_id = Convert.ToInt32(reader["field_id"].ToString()),
                                              field_namespace_id = Convert.ToInt32(reader["field_namespace_id"].ToString()),
                                              displayText = reader["field_name"] != null ? reader["field_name"].ToString() : ""
                                          }).ToList();

                        OrderByFields = (from DataRow reader in dataset.Tables[2].Rows
                                          select new FolderFields()
                                          {
                                              field_id = Convert.ToInt32(reader["field_id"].ToString()),
                                              field_namespace_id = Convert.ToInt32(reader["field_namespace_id"].ToString()),
                                              displayText = reader["field_name"] != null ? reader["field_name"].ToString() : ""
                                          }).ToList();
                    }


                }
                folderDetails.AvailableFields = AvailableFields;
                folderDetails.SelectedFields = SelectedFields;
                folderDetails.OrderByFields = OrderByFields;

                return folderDetails;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion

    }
}
