﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;
using UserDataAccess;

namespace SCP.DAL
{
    public class ScheduleDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;

        #endregion

        #region constructor

        private static readonly ScheduleDAL instance = new ScheduleDAL();   //singleton
        DataTable dt;
        static ScheduleDAL()
        {
        }
        private ScheduleDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static ScheduleDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Schedule Search 
        public List<ScheduleData> GetScheduleData(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;                                
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if(dt.Rows.Count>0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetScheduleData", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
               // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<ScheduleData> schedules = new List<ScheduleData>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        schedules = (from DataRow reader in dataset.Tables[0].Rows
                                    select new ScheduleData()
                                    {

                                        scheduleNumber = Convert.ToInt64(reader["scheduleNumber"]),
                                        lineNumber = reader["lineNumber"] != null ? reader["lineNumber"].ToString() : "",
                                        shipmentNumber = reader["shipmentNumber"] != null ? reader["shipmentNumber"].ToString() : "",
                                        dueDate = reader["dueDate"] != null ? reader["dueDate"].ToString() : "",
                                        itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                        scheduleQuantity = reader["scheduleQuantity"] != null ? reader["scheduleQuantity"].ToString() : "",
                                        price = reader["price"] != null ? reader["price"].ToString() : "",
                                        releaseNumber = reader["releaseNumber"] != null ? reader["releaseNumber"].ToString() : "",
                                        releaseType = reader["releaseType"] != null ? reader["releaseType"].ToString() : "",
                                        releaseDate = reader["releaseDate"] != null ? reader["releaseDate"].ToString() : "",
                                        openQty = reader["openQty"] != null ? reader["openQty"].ToString() : "",
                                        plant = reader["plant"] != null ? reader["plant"].ToString() : "",
                                        vendorName = reader["vendorName"] != null ? reader["vendorName"].ToString() : "",
                                        vendorNumber = reader["vendorNumber"] != null ? reader["vendorNumber"].ToString() : ""                                        
                                    }).ToList();
                    }


                }
                return schedules;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion

        #region Get Schedule Summary 
        public List<SchedulesSummary> GetScheduleSummary(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetScheduleSummary", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<SchedulesSummary> schedules = new List<SchedulesSummary>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        schedules = (from DataRow reader in dataset.Tables[0].Rows
                                     select new SchedulesSummary()
                                     {

                                         scheduleNumber = Convert.ToInt64(reader["scheduleNumber"]),
                                         lineNumber = reader["lineNumber"] != null ? reader["lineNumber"].ToString() : "",
                                         quantity = reader["quantity"] != null ? reader["quantity"].ToString() : "",
                                         received = reader["received"] != null ? reader["received"].ToString() : "",
                                         itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                         printed = reader["printed"] != null ? reader["printed"].ToString() : "",
                                         shipped = reader["shipped"] != null ? reader["shipped"].ToString() : "",
                                         releaseNumber = reader["releaseNumber"] != null ? reader["releaseNumber"].ToString() : "",
                                         remaining = reader["remaining"] != null ? reader["remaining"].ToString() : "",
                                         releaseDate = reader["releaseDate"] != null ? reader["releaseDate"].ToString() : "",
                                         vendorNumber = reader["vendorNumber"] != null ? reader["vendorNumber"].ToString() : ""
                                     }).ToList();
                    }


                }
                return schedules;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion


        #region Get Schedule DeliveryDates 
        public List<ScheduleDeliverDates> GetScheduleDeliveryDates(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetScheduleDeliveryDate", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<ScheduleDeliverDates> schedules = new List<ScheduleDeliverDates>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        schedules = (from DataRow reader in dataset.Tables[0].Rows
                                     select new ScheduleDeliverDates()
                                     {

                                         scheduleNumber = Convert.ToInt64(reader["scheduleNumber"]),
                                         lineNumber = reader["lineNumber"] != null ? reader["lineNumber"].ToString() : "",
                                         shipmentNumber = reader["shipmentNumber"] != null ? reader["shipmentNumber"].ToString() : "",
                                         material = reader["material"] != null ? reader["material"].ToString() : "",
                                        // itemNumber = reader["itemNumber"] != null ? reader["itemNumber"].ToString() : "",
                                         quantity = reader["quantity"] != null ? reader["quantity"].ToString() : "",
                                         receivedQuantity = reader["receivedQuantity"] != null ? reader["receivedQuantity"].ToString() : "",
                                         releaseNumber = reader["releaseNumber"] != null ? reader["releaseNumber"].ToString() : "",
                                         releaseType = reader["releaseType"] != null ? reader["releaseType"].ToString() : "",
                                         deliveryDate = reader["deliveryDate"] != null ? reader["deliveryDate"].ToString() : "",
                                         statisticalDeliveryDate = reader["statisticalDeliveryDate"] != null ? reader["statisticalDeliveryDate"].ToString() : "",
                                         plant = reader["plant"] != null ? reader["plant"].ToString() : "",
                                         LD = reader["LD"] != null ? reader["LD"].ToString() : "",
                                         priority1 = reader["priority1"] != null ? reader["priority1"].ToString() : "",
                                         priority2 = reader["priority2"] != null ? reader["priority2"].ToString() : "",
                                         openqty = reader["openqty"] != null ? reader["openqty"].ToString() : "",
                                         erpCode = reader["erpCode"] != null ? reader["erpCode"].ToString() : "",
                                         //key = reader["key"] != null ? reader["key"].ToString() : "",
                                         vendorNumber = reader["vendorNumber"] != null ? reader["vendorNumber"].ToString() : ""
                                     }).ToList();
                    }


                }
                return schedules;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion


    }
}
