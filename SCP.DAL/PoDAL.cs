﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;
using SCP.Common;

namespace SCP.DAL
{
    public class PoDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;

        #endregion

        #region constructor

        private static readonly PoDAL instance = new PoDAL();   //singleton

        static PoDAL()
        {
        }
        private PoDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static PoDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Purchase Order Details 
        public PurchaseOrderDetails GetPurchaseOrderDetails(Int64 poNumber)
        {
            try
            {
                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetPoOrderDetails", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", poNumber, SqlDbType.BigInt));

                PurchaseOrderDetails podetails = new PurchaseOrderDetails();

                List<PoOrderFields> OrderDetails = new List<PoOrderFields>();
                List<PoOrderFields> ItemDetails = new List<PoOrderFields>();
                List<PoOrderFields> AdditionalDetails = new List<PoOrderFields>();
                List<PoOrderFields> Collabration = new List<PoOrderFields>();

                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        OrderDetails = (from DataRow reader in dataset.Tables[0].Rows
                                        select new PoOrderFields()
                                        {
                                            poNumber = Convert.ToInt64(reader["poNumber"].ToString()),
                                            poType = reader["poType"] != null ? reader["poType"].ToString() : "",
                                            poStatus = reader["poStatus"] != null ? reader["poStatus"].ToString() : "",
                                            Line = reader["Line"] != null ? reader["Line"].ToString() : "",
                                            orderDate = reader["orderDate"] != null ? reader["orderDate"].ToString() : "",
                                            teaNumber = reader["teaNumber"] != null ? reader["teaNumber"].ToString() : "",
                                            release = reader["release"] != null ? reader["release"].ToString() : "",
                                            shipmentNumer = reader["shipmentNumer"] != null ? reader["shipmentNumer"].ToString() : ""
                                        }).ToList();

                        ItemDetails = (from DataRow reader in dataset.Tables[1].Rows
                                       select new PoOrderFields()
                                       {
                                           itemNumer = reader["itemNumer"] != null ? reader["itemNumer"].ToString() : "",
                                           itemText = reader["itemText"] != null ? reader["itemText"].ToString() : "",
                                           itemRevision = reader["itemRevision"] != null ? reader["itemRevision"].ToString() : "",
                                           unitPrice = reader["unitPrice"] != null ? reader["unitPrice"].ToString() : "",
                                           extendedprice = reader["extendedprice"] != null ? reader["extendedprice"].ToString() : "",
                                           currency = reader["currency"] != null ? reader["currency"].ToString() : "",
                                           quantity = reader["quantity"] != null ? reader["quantity"].ToString() : "",
                                           uom = reader["uom"] != null ? reader["uom"].ToString() : "",
                                           faiblock = reader["faiblock"] != null ? reader["faiblock"].ToString() : "",
                                           qtyopen = reader["qtyopen"] != null ? reader["qtyopen"].ToString() : "",
                                           qtyReceived = reader["qtyReceived"] != null ? reader["qtyReceived"].ToString() : "",
                                           releaseUntilldate = reader["releaseUntilldate"] != null ? reader["releaseUntilldate"].ToString() : "",
                                           plantNumber = reader["plantNumber"] != null ? reader["plantNumber"].ToString() : ""
                                       }).ToList();

                        AdditionalDetails = (from DataRow reader in dataset.Tables[2].Rows
                                             select new PoOrderFields()
                                             {
                                                 poNumber = Convert.ToInt64(reader["poNumber"].ToString()),
                                                 lineNumber = reader["lineNumber"] != null ? reader["lineNumber"].ToString() : "",
                                                 poShipmentNumber = reader["poShipmentNumber"] != null ? reader["poShipmentNumber"].ToString() : "",
                                                 poType = reader["poType"] != null ? reader["poType"].ToString() : "",
                                                 MaterialNumber = reader["MaterialNumber"] != null ? reader["MaterialNumber"].ToString() : "",
                                                 Status = reader["Status"] != null ? reader["Status"].ToString() : "",
                                                 DeliveryDate = reader["DeliveryDate"] != null ? reader["DeliveryDate"].ToString() : "",
                                                 Needbydate = reader["Needbydate"] != null ? reader["Needbydate"].ToString() : "",
                                                 Statdeldate = reader["Statdeldate"] != null ? reader["Statdeldate"].ToString() : "",
                                                 trackmypart = reader["trackmypart"] != null ? reader["trackmypart"].ToString() : ""
                                             }).ToList();

                        Collabration = (from DataRow reader in dataset.Tables[3].Rows
                                        select new PoOrderFields()
                                        {
                                            poNumber = Convert.ToInt64(reader["poNumber"].ToString()),
                                            mrpRecommondation = reader["mrpRecommondation"] != null ? reader["mrpRecommondation"].ToString() : "",
                                            mrpSuggestedDate = reader["mrpSuggestedDate"] != null ? reader["mrpSuggestedDate"].ToString() : "",
                                            mrpQty = reader["mrpQty"] != null ? reader["mrpQty"].ToString() : "",
                                            needByDate = reader["needByDate"] != null ? reader["needByDate"].ToString() : "",
                                            collabarationEarly = reader["collabarationEarly"] != null ? reader["collabarationEarly"].ToString() : "",
                                            collabarationLate = reader["collabarationLate"] != null ? reader["collabarationLate"].ToString() : "",
                                            resheduleInEarly = reader["resheduleInEarly"] != null ? reader["resheduleInEarly"].ToString() : "",
                                            resheduleInLate = reader["resheduleInLate"] != null ? reader["resheduleInLate"].ToString() : "",
                                            resheduleOutEarly = reader["resheduleOutEarly"] != null ? reader["resheduleOutEarly"].ToString() : "",

                                            resheduleOutLate = reader["resheduleOutLate"] != null ? reader["resheduleOutLate"].ToString() : "",
                                            printable = reader["printable"] != null ? reader["printable"].ToString() : "",
                                            currentDeliverDate = reader["currentDeliverDate"] != null ? reader["currentDeliverDate"].ToString() : "",
                                            currentUnitPrice = reader["currentUnitPrice"] != null ? reader["currentUnitPrice"].ToString() : "",
                                            requestDeliveryDate = reader["requestDeliveryDate"] != null ? reader["requestDeliveryDate"].ToString() : "",
                                            requestUnitPrice = reader["requestUnitPrice"] != null ? reader["requestUnitPrice"].ToString() : "",
                                            NewDeliveryDate = reader["NewDeliveryDate"] != null ? reader["NewDeliveryDate"].ToString() : "",
                                            NewUnitPrice = reader["NewUnitPrice"] != null ? reader["NewUnitPrice"].ToString() : "",
                                            openQty = reader["openQty"] != null ? reader["openQty"].ToString() : "",

                                            RejectReason = reader["RejectReason"] != null ? reader["RejectReason"].ToString() : "",
                                            RMAStatus = reader["RMAStatus"] != null ? reader["RMAStatus"].ToString() : "",
                                            MRPReason = reader["MRPReason"] != null ? reader["MRPReason"].ToString() : "",
                                            Comments = reader["Comments"] != null ? reader["Comments"].ToString() : "",
                                            PreviousCpmments = reader["PreviousCpmments"] != null ? reader["PreviousCpmments"].ToString() : "",
                                            SystemComments = reader["SystemComments"] != null ? reader["SystemComments"].ToString() : ""

                                        }).ToList();
                    }
                }
                podetails.OrderDetails = OrderDetails;
                podetails.ItemDetails = ItemDetails;
                podetails.AdditionalDetails = AdditionalDetails;
                podetails.Collabration = Collabration;

                return podetails;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }

        #endregion
    }
}
