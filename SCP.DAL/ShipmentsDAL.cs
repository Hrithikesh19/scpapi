﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using SCP.Common;
using System.Configuration;
using System.Data.SqlClient;
using SCP.BO;

namespace SCP.DAL
{
    public class ShipmentsDAL
    {
        #region "Global variables"
        SqlHelper objSQLHelper;

        string ConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();

        SqlConnection con = null;

        DataTable dt;
        #endregion

        #region constructor

        private static readonly ShipmentsDAL instance = new ShipmentsDAL();   //singleton

        static ShipmentsDAL()
        {
        }
        private ShipmentsDAL()
        {
            objSQLHelper = new SqlHelper();

        }


        public static ShipmentsDAL Instance
        {
            get
            {
                return instance;
            }
        }

        #endregion

        #region Get Shipments 
        public List<ShipmentsData> GetShipmentByShipmentNumber(Int64 shipmentNumber)
        {
            try
            {

                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetSearchShipmentsData", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@shipmentNumber", shipmentNumber, SqlDbType.BigInt));

                List<ShipmentsData> shipments = new List<ShipmentsData>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        shipments = (from DataRow reader in dataset.Tables[0].Rows
                                     select new ShipmentsData()
                                     {

                                         SHIPMENT_ID = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",                                         
                                         SHIPMENT_NUMBER = reader["SHIPMENT_NUMBER"] != null ? reader["SHIPMENT_NUMBER"].ToString() : "",
                                         STATUS = reader["STATUS"] != null ? reader["STATUS"].ToString() : "",
                                         CARRIER_NAME = reader["CARRIER_NAME"] != null ? reader["CARRIER_NAME"].ToString() : "",
                                         CARRIER_ID = reader["CARRIER_ID"] != null ? reader["CARRIER_ID"].ToString() : "",
                                         CARRIER_TRACKING_NUM = reader["CARRIER_TRACKING_NUM"] != null ? reader["CARRIER_TRACKING_NUM"].ToString() : "",
                                         WAYBILL = reader["WAYBILL"] != null ? reader["WAYBILL"].ToString() : "",
                                         REFERENCE = reader["REFERENCE"] != null ? reader["REFERENCE"].ToString() : "",
                                         RELEASE_CODE = reader["RELEASE_CODE"] != null ? reader["RELEASE_CODE"].ToString() : "",
                                         PACKING_SLIP = reader["PACKING_SLIP"] != null ? reader["PACKING_SLIP"].ToString() : "",
                                         FLIGHT_NUMBER = reader["FLIGHT_NUMBER"] != null ? reader["FLIGHT_NUMBER"].ToString() : "",
                                         SHIPPING_CHARGES = reader["SHIPPING_CHARGES"] != null ? reader["SHIPPING_CHARGES"].ToString() : "",
                                         FREIGHT_CHARGES = reader["FREIGHT_CHARGES"] != null ? reader["FREIGHT_CHARGES"].ToString() : "",
                                         NET_WEIGHT = reader["NET_WEIGHT"] != null ? reader["NET_WEIGHT"].ToString() : "",
                                         GROSS_WEIGHT = reader["GROSS_WEIGHT"] != null ? reader["GROSS_WEIGHT"].ToString() : "",
                                         CERTIFICATION = reader["CERTIFICATION"] != null ? reader["CERTIFICATION"].ToString() : "",
                                         COMPLETED_BY = reader["COMPLETED_BY"] != null ? reader["COMPLETED_BY"].ToString() : "",
                                         COMPLETED_DATE = reader["COMPLETED_DATE"] != null ? reader["COMPLETED_DATE"].ToString() : "",
                                         NUM_OF_PACKAGES = reader["NUM_OF_PACKAGES"] != null ? reader["NUM_OF_PACKAGES"].ToString() : "",
                                         ERP_CODE = reader["ERP_CODE"] != null ? reader["ERP_CODE"].ToString() : "",
                                         ERP_SHIP_TO_KEY = reader["ERP_SHIP_TO_KEY"] != null ? reader["ERP_SHIP_TO_KEY"].ToString() : "",
                                         ERP_SHIP_FROM_KEY = reader["ERP_SHIP_FROM_KEY"] != null ? reader["ERP_SHIP_FROM_KEY"].ToString() : "",
                                         ERP_FROM_ADDRESS1 = reader["ERP_FROM_ADDRESS1"] != null ? reader["ERP_FROM_ADDRESS1"].ToString() : "",
                                         ERP_FROM_ADDRESS2 = reader["ERP_FROM_ADDRESS2"] != null ? reader["ERP_FROM_ADDRESS2"].ToString() : "",
                                         ERP_FROM_ADDRESS3 = reader["ERP_FROM_ADDRESS3"] != null ? reader["ERP_FROM_ADDRESS3"].ToString() : "",
                                         ERP_FROM_CITY = reader["ERP_FROM_CITY"] != null ? reader["ERP_FROM_CITY"].ToString() : "",
                                         ERP_FROM_COUNTY = reader["ERP_FROM_COUNTY"] != null ? reader["ERP_FROM_COUNTY"].ToString() : "",
                                         ERP_FROM_STATE = reader["ERP_FROM_STATE"] != null ? reader["ERP_FROM_STATE"].ToString() : "",
                                         ERP_FROM_PROVINCE = reader["ERP_FROM_PROVINCE"] != null ? reader["ERP_FROM_PROVINCE"].ToString() : "",
                                         ERP_FROM_POSTAL_CODE = reader["ERP_FROM_POSTAL_CODE"] != null ? reader["ERP_FROM_POSTAL_CODE"].ToString() : "",
                                         ERP_FROM_COUNTRY = reader["ERP_FROM_COUNTRY"] != null ? reader["ERP_FROM_COUNTRY"].ToString() : "",
                                         ERP_TO_ADDRESS1 = reader["ERP_TO_ADDRESS1"] != null ? reader["ERP_TO_ADDRESS1"].ToString() : "",
                                         ERP_TO_ADDRESS2 = reader["ERP_TO_ADDRESS2"] != null ? reader["ERP_TO_ADDRESS2"].ToString() : "",
                                         ERP_TO_ADDRESS3 = reader["ERP_TO_ADDRESS3"] != null ? reader["ERP_TO_ADDRESS3"].ToString() : "",
                                         ERP_TO_CITY = reader["ERP_TO_CITY"] != null ? reader["ERP_TO_CITY"].ToString() : "",
                                         ERP_TO_COUNTY = reader["ERP_TO_COUNTY"] != null ? reader["ERP_TO_COUNTY"].ToString() : "",
                                         ERP_TO_STATE = reader["ERP_TO_STATE"] != null ? reader["ERP_TO_STATE"].ToString() : "",
                                         ERP_TO_PROVINCE = reader["ERP_TO_PROVINCE"] != null ? reader["ERP_TO_PROVINCE"].ToString() : "",
                                         ERP_TO_POSTAL_CODE = reader["ERP_TO_POSTAL_CODE"] != null ? reader["ERP_TO_POSTAL_CODE"].ToString() : "",
                                         ERP_TO_COUNTRY = reader["ERP_TO_COUNTRY"] != null ? reader["ERP_TO_COUNTRY"].ToString() : "",
                                         ERP_SHIP_TO_NAME = reader["ERP_SHIP_TO_NAME"] != null ? reader["ERP_SHIP_TO_NAME"].ToString() : "",
                                         ERP_SHIP_TO_LOCATION = reader["ERP_SHIP_TO_LOCATION"] != null ? reader["ERP_SHIP_TO_LOCATION"].ToString() : "",
                                         ERP_SHIP_TO_ID = reader["ERP_SHIP_TO_ID"] != null ? reader["ERP_SHIP_TO_ID"].ToString() : "",
                                         ERP_RECEIPT_NUMBER = reader["ERP_RECEIPT_NUMBER"] != null ? reader["ERP_RECEIPT_NUMBER"].ToString() : "",
                                         INTERFACE_LOG = reader["INTERFACE_LOG"] != null ? reader["INTERFACE_LOG"].ToString() : "",
                                         SO_PO_TRANSACTION_STATUS = reader["SO_PO_TRANSACTION_STATUS"] != null ? reader["SO_PO_TRANSACTION_STATUS"].ToString() : "",
                                         FROM_LINK_ID = reader["FROM_LINK_ID"] != null ? reader["FROM_LINK_ID"].ToString() : "",
                                         APPLICATION_KEY_ID = reader["APPLICATION_KEY_ID"] != null ? reader["APPLICATION_KEY_ID"].ToString() : "",
                                         //ATTRIBUTE1 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE2 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE3 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE4 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE5 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE6 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE7 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE8 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE9 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE10 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE11 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE12 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE13 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE14 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE15 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE16 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE17 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE18 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE19 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         //ATTRIBUTE20 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                         CREATED_BY = reader["CREATED_BY"] != null ? reader["CREATED_BY"].ToString() : "",
                                         CREATION_DATE = reader["CREATION_DATE"] != null ? reader["CREATION_DATE"].ToString() : "",
                                         LAST_UPDATE_DATE = reader["LAST_UPDATE_DATE"] != null ? reader["LAST_UPDATE_DATE"].ToString() : "",
                                         LAST_UPDATE_BY = reader["LAST_UPDATE_BY"] != null ? reader["LAST_UPDATE_BY"].ToString() : "",
                                         MARK_FOR_ADDRESS = reader["MARK_FOR_ADDRESS"] != null ? reader["MARK_FOR_ADDRESS"].ToString() : "",
                                         FREIGHT_BILL_NUM = reader["FREIGHT_BILL_NUM"] != null ? reader["FREIGHT_BILL_NUM"].ToString() : "",
                                         ERP_SHIP_TO_LOCATION_ID = reader["ERP_SHIP_TO_LOCATION_ID"] != null ? reader["ERP_SHIP_TO_LOCATION_ID"].ToString() : "",
                                         ERP_FROM_NAME = reader["ERP_FROM_NAME"] != null ? reader["ERP_FROM_NAME"].ToString() : "",
                                         ERP_TO_NAME = reader["ERP_TO_NAME"] != null ? reader["ERP_TO_NAME"].ToString() : "",
                                         NOTES = reader["NOTES"] != null ? reader["NOTES"].ToString() : "",
                                         TO_LINK_ID = reader["TO_LINK_ID"] != null ? reader["TO_LINK_ID"].ToString() : "",
                                         APPLICATION_CODE = reader["APPLICATION_CODE"] != null ? reader["APPLICATION_CODE"].ToString() : "",
                                         INTERFACE_ERROR = reader["INTERFACE_ERROR"] != null ? reader["INTERFACE_ERROR"].ToString() : "",
                                         INVOICE_NUMBER = reader["INVOICE_NUMBER"] != null ? reader["INVOICE_NUMBER"].ToString() : "",
                                         INVOICE_DESCRIPTION = reader["INVOICE_DESCRIPTION"] != null ? reader["INVOICE_DESCRIPTION"].ToString() : "",
                                         INVOICE_DATE = reader["INVOICE_DATE"] != null ? reader["INVOICE_DATE"].ToString() : "",
                                         TAX_CHARGES = reader["TAX_CHARGES"] != null ? reader["TAX_CHARGES"].ToString() : "",
                                         TAX_BILL_NUM = reader["TAX_BILL_NUM"] != null ? reader["TAX_BILL_NUM"].ToString() : "",
                                         TAX_NAME = reader["TAX_NAME"] != null ? reader["TAX_NAME"].ToString() : "",
                                         CURRENCY_KEY = reader["CURRENCY_KEY"] != null ? reader["CURRENCY_KEY"].ToString() : "",
                                         READY_TO_INVOICE = reader["READY_TO_INVOICE"] != null ? reader["READY_TO_INVOICE"].ToString() : "",
                                         PAY_SITE = reader["PAY_SITE"] != null ? reader["PAY_SITE"].ToString() : "",
                                         FREIGHT_TERMS = reader["FREIGHT_TERMS"] != null ? reader["FREIGHT_TERMS"].ToString() : "",
                                         INVOICE_ENABLED = reader["INVOICE_ENABLED"] != null ? reader["INVOICE_ENABLED"].ToString() : "",
                                         INVOICE_CREATED = reader["INVOICE_CREATED"] != null ? reader["INVOICE_CREATED"].ToString() : "",
                                         HOST_KEY = reader["HOST_KEY"] != null ? reader["HOST_KEY"].ToString() : "",
                                         PICKUP_DATE = reader["PICKUP_DATE"] != null ? reader["PICKUP_DATE"].ToString() : "",
                                         TRIP_ID = reader["TRIP_ID"] != null ? reader["TRIP_ID"].ToString() : "",
                                         EXPECTED_RECEIPT_DATE = reader["EXPECTED_RECEIPT_DATE"] != null ? reader["EXPECTED_RECEIPT_DATE"].ToString() : "",
                                         PROCESSING_STATUS = reader["PROCESSING_STATUS"] != null ? reader["PROCESSING_STATUS"].ToString() : "",
                                         PROCESSING_CODE = reader["PROCESSING_CODE"] != null ? reader["PROCESSING_CODE"].ToString() : "",
                                         DELIVERY_ID = reader["DELIVERY_ID"] != null ? reader["DELIVERY_ID"].ToString() : "",
                                         UNIT_OF_WEIGHT = reader["UNIT_OF_WEIGHT"] != null ? reader["UNIT_OF_WEIGHT"].ToString() : "",
                                         FSL_USED = reader["FSL_USED"] != null ? reader["FSL_USED"].ToString() : "",
                                         ERP_VENDOR_NAME = reader["ERP_VENDOR_NAME"] != null ? reader["ERP_VENDOR_NAME"].ToString() : ""


                                     }).ToList();
                    }


                }
                return shipments;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }
        public List<ShipmentsData> GetShipments(PoOrderRequest request)
        {
            try
            {
                string namespacename = string.Empty;
                string strQuery = string.Empty;
                if (request != null && request.SearchParams != null && request.SearchParams.Count > 0)
                {
                    foreach (var item in request.SearchParams)
                    {
                        dt = SQLConnections.MyTable("select namespace_name from con_field_namespaces where field_namespace_id='" + item.field_namespace_id + "'");
                        if (dt.Rows.Count > 0)
                        {
                            namespacename = dt.Rows[0]["namespace_name"].ToString();
                        }

                        if (item.Condition.ToLower() == "contains")
                            strQuery += namespacename + "." + item.field_name + " LIKE '%" + item.Values + "%' AND ";
                        else if (item.Condition.ToLower() == "equal")
                            strQuery += namespacename + "." + item.field_name + "='" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "greaterthan")
                            strQuery += namespacename + "." + item.field_name + ">'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "lessthan")
                            strQuery += namespacename + "." + item.field_name + "<'" + item.Values + "' AND ";
                        else if (item.Condition.ToLower() == "in")
                            strQuery += namespacename + "." + item.field_name + " IN (" + item.Values + ") AND ";
                    }
                }

                if (!string.IsNullOrEmpty(strQuery))
                    strQuery = strQuery.Remove(strQuery.Length - 4);


                con = new SqlConnection(ConnString);
                SqlCommand objSqlCommand = new SqlCommand("GetShipmentData", con);
                objSqlCommand.CommandType = CommandType.StoredProcedure;
                // objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@poNumber", request.poNumber, SqlDbType.BigInt));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@page_id", request.Page_Id, SqlDbType.Int));
                objSqlCommand.Parameters.Add(objSQLHelper.SetParametersIn("@SearchCondition", strQuery, SqlDbType.NVarChar));

                List<ShipmentsData> packages = new List<ShipmentsData>();
                using (con)
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }

                    using (SqlDataAdapter da = new SqlDataAdapter(objSqlCommand))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        DataSet dataset = new DataSet();
                        da.Fill(dataset);

                        packages = (from DataRow reader in dataset.Tables[0].Rows
                                    select new ShipmentsData()
                                    {

                                        SHIPMENT_ID = reader["SHIPMENT_ID"] != null ? reader["SHIPMENT_ID"].ToString() : "",                                        
                                        SHIPMENT_NUMBER = reader["SHIPMENT_NUMBER"] != null ? reader["SHIPMENT_NUMBER"].ToString() : "",
                                        STATUS = reader["STATUS"] != null ? reader["STATUS"].ToString() : "",
                                        CARRIER_NAME = reader["CARRIER_NAME"] != null ? reader["CARRIER_NAME"].ToString() : "",
                                        CARRIER_ID = reader["CARRIER_ID"] != null ? reader["CARRIER_ID"].ToString() : "",
                                        CARRIER_TRACKING_NUM = reader["CARRIER_TRACKING_NUM"] != null ? reader["CARRIER_TRACKING_NUM"].ToString() : "",
                                        WAYBILL = reader["WAYBILL"] != null ? reader["WAYBILL"].ToString() : "",
                                        REFERENCE = reader["REFERENCE"] != null ? reader["REFERENCE"].ToString() : "",
                                        RELEASE_CODE = reader["RELEASE_CODE"] != null ? reader["RELEASE_CODE"].ToString() : "",
                                        PACKING_SLIP = reader["PACKING_SLIP"] != null ? reader["PACKING_SLIP"].ToString() : "",
                                        FLIGHT_NUMBER = reader["FLIGHT_NUMBER"] != null ? reader["FLIGHT_NUMBER"].ToString() : "",
                                        SHIPPING_CHARGES = reader["SHIPPING_CHARGES"] != null ? reader["SHIPPING_CHARGES"].ToString() : "",
                                        FREIGHT_CHARGES = reader["FREIGHT_CHARGES"] != null ? reader["FREIGHT_CHARGES"].ToString() : "",
                                        NET_WEIGHT = reader["NET_WEIGHT"] != null ? reader["NET_WEIGHT"].ToString() : "",
                                        GROSS_WEIGHT = reader["GROSS_WEIGHT"] != null ? reader["GROSS_WEIGHT"].ToString() : "",
                                        CERTIFICATION = reader["CERTIFICATION"] != null ? reader["CERTIFICATION"].ToString() : "",
                                        COMPLETED_BY = reader["COMPLETED_BY"] != null ? reader["COMPLETED_BY"].ToString() : "",
                                        COMPLETED_DATE = reader["COMPLETED_DATE"] != null ? reader["COMPLETED_DATE"].ToString() : "",
                                        NUM_OF_PACKAGES = reader["NUM_OF_PACKAGES"] != null ? reader["NUM_OF_PACKAGES"].ToString() : "",
                                        ERP_CODE = reader["ERP_CODE"] != null ? reader["ERP_CODE"].ToString() : "",
                                        ERP_SHIP_TO_KEY = reader["ERP_SHIP_TO_KEY"] != null ? reader["ERP_SHIP_TO_KEY"].ToString() : "",
                                        ERP_SHIP_FROM_KEY = reader["ERP_SHIP_FROM_KEY"] != null ? reader["ERP_SHIP_FROM_KEY"].ToString() : "",
                                        ERP_FROM_ADDRESS1 = reader["ERP_FROM_ADDRESS1"] != null ? reader["ERP_FROM_ADDRESS1"].ToString() : "",
                                        ERP_FROM_ADDRESS2 = reader["ERP_FROM_ADDRESS2"] != null ? reader["ERP_FROM_ADDRESS2"].ToString() : "",
                                        ERP_FROM_ADDRESS3 = reader["ERP_FROM_ADDRESS3"] != null ? reader["ERP_FROM_ADDRESS3"].ToString() : "",
                                        ERP_FROM_CITY = reader["ERP_FROM_CITY"] != null ? reader["ERP_FROM_CITY"].ToString() : "",
                                        ERP_FROM_COUNTY = reader["ERP_FROM_COUNTY"] != null ? reader["ERP_FROM_COUNTY"].ToString() : "",
                                        ERP_FROM_STATE = reader["ERP_FROM_STATE"] != null ? reader["ERP_FROM_STATE"].ToString() : "",
                                        ERP_FROM_PROVINCE = reader["ERP_FROM_PROVINCE"] != null ? reader["ERP_FROM_PROVINCE"].ToString() : "",
                                        ERP_FROM_POSTAL_CODE = reader["ERP_FROM_POSTAL_CODE"] != null ? reader["ERP_FROM_POSTAL_CODE"].ToString() : "",
                                        ERP_FROM_COUNTRY = reader["ERP_FROM_COUNTRY"] != null ? reader["ERP_FROM_COUNTRY"].ToString() : "",
                                        ERP_TO_ADDRESS1 = reader["ERP_TO_ADDRESS1"] != null ? reader["ERP_TO_ADDRESS1"].ToString() : "",
                                        ERP_TO_ADDRESS2 = reader["ERP_TO_ADDRESS2"] != null ? reader["ERP_TO_ADDRESS2"].ToString() : "",
                                        ERP_TO_ADDRESS3 = reader["ERP_TO_ADDRESS3"] != null ? reader["ERP_TO_ADDRESS3"].ToString() : "",
                                        ERP_TO_CITY = reader["ERP_TO_CITY"] != null ? reader["ERP_TO_CITY"].ToString() : "",
                                        ERP_TO_COUNTY = reader["ERP_TO_COUNTY"] != null ? reader["ERP_TO_COUNTY"].ToString() : "",
                                        ERP_TO_STATE = reader["ERP_TO_STATE"] != null ? reader["ERP_TO_STATE"].ToString() : "",
                                        ERP_TO_PROVINCE = reader["ERP_TO_PROVINCE"] != null ? reader["ERP_TO_PROVINCE"].ToString() : "",
                                        ERP_TO_POSTAL_CODE = reader["ERP_TO_POSTAL_CODE"] != null ? reader["ERP_TO_POSTAL_CODE"].ToString() : "",
                                        ERP_TO_COUNTRY = reader["ERP_TO_COUNTRY"] != null ? reader["ERP_TO_COUNTRY"].ToString() : "",
                                        ERP_SHIP_TO_NAME = reader["ERP_SHIP_TO_NAME"] != null ? reader["ERP_SHIP_TO_NAME"].ToString() : "",
                                        ERP_SHIP_TO_LOCATION = reader["ERP_SHIP_TO_LOCATION"] != null ? reader["ERP_SHIP_TO_LOCATION"].ToString() : "",
                                        ERP_SHIP_TO_ID = reader["ERP_SHIP_TO_ID"] != null ? reader["ERP_SHIP_TO_ID"].ToString() : "",
                                        ERP_RECEIPT_NUMBER = reader["ERP_RECEIPT_NUMBER"] != null ? reader["ERP_RECEIPT_NUMBER"].ToString() : "",
                                        INTERFACE_LOG = reader["INTERFACE_LOG"] != null ? reader["INTERFACE_LOG"].ToString() : "",
                                        SO_PO_TRANSACTION_STATUS = reader["SO_PO_TRANSACTION_STATUS"] != null ? reader["SO_PO_TRANSACTION_STATUS"].ToString() : "",
                                        FROM_LINK_ID = reader["FROM_LINK_ID"] != null ? reader["FROM_LINK_ID"].ToString() : "",
                                        APPLICATION_KEY_ID = reader["APPLICATION_KEY_ID"] != null ? reader["APPLICATION_KEY_ID"].ToString() : "",
                                        //ATTRIBUTE1 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE2 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE3 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE4 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE5 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE6 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE7 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE8 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE9 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE10 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE11 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE12 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE13 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE14 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE15 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE16 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE17 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE18 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE19 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        //ATTRIBUTE20 = reader["PACKAGE_ID"] != null ? reader["PACKAGE_ID"].ToString() : "",
                                        CREATED_BY = reader["CREATED_BY"] != null ? reader["CREATED_BY"].ToString() : "",
                                        CREATION_DATE = reader["CREATION_DATE"] != null ? reader["CREATION_DATE"].ToString() : "",
                                        LAST_UPDATE_DATE = reader["LAST_UPDATE_DATE"] != null ? reader["LAST_UPDATE_DATE"].ToString() : "",
                                        LAST_UPDATE_BY = reader["LAST_UPDATE_BY"] != null ? reader["LAST_UPDATE_BY"].ToString() : "",
                                        MARK_FOR_ADDRESS = reader["MARK_FOR_ADDRESS"] != null ? reader["MARK_FOR_ADDRESS"].ToString() : "",
                                        FREIGHT_BILL_NUM = reader["FREIGHT_BILL_NUM"] != null ? reader["FREIGHT_BILL_NUM"].ToString() : "",
                                        ERP_SHIP_TO_LOCATION_ID = reader["ERP_SHIP_TO_LOCATION_ID"] != null ? reader["ERP_SHIP_TO_LOCATION_ID"].ToString() : "",
                                        ERP_FROM_NAME = reader["ERP_FROM_NAME"] != null ? reader["ERP_FROM_NAME"].ToString() : "",
                                        ERP_TO_NAME = reader["ERP_TO_NAME"] != null ? reader["ERP_TO_NAME"].ToString() : "",
                                        NOTES = reader["NOTES"] != null ? reader["NOTES"].ToString() : "",
                                        TO_LINK_ID = reader["TO_LINK_ID"] != null ? reader["TO_LINK_ID"].ToString() : "",
                                        APPLICATION_CODE = reader["APPLICATION_CODE"] != null ? reader["APPLICATION_CODE"].ToString() : "",
                                        INTERFACE_ERROR = reader["INTERFACE_ERROR"] != null ? reader["INTERFACE_ERROR"].ToString() : "",
                                        INVOICE_NUMBER = reader["INVOICE_NUMBER"] != null ? reader["INVOICE_NUMBER"].ToString() : "",
                                        INVOICE_DESCRIPTION = reader["INVOICE_DESCRIPTION"] != null ? reader["INVOICE_DESCRIPTION"].ToString() : "",
                                        INVOICE_DATE = reader["INVOICE_DATE"] != null ? reader["INVOICE_DATE"].ToString() : "",
                                        TAX_CHARGES = reader["TAX_CHARGES"] != null ? reader["TAX_CHARGES"].ToString() : "",
                                        TAX_BILL_NUM = reader["TAX_BILL_NUM"] != null ? reader["TAX_BILL_NUM"].ToString() : "",
                                        TAX_NAME = reader["TAX_NAME"] != null ? reader["TAX_NAME"].ToString() : "",
                                        CURRENCY_KEY = reader["CURRENCY_KEY"] != null ? reader["CURRENCY_KEY"].ToString() : "",
                                        READY_TO_INVOICE = reader["READY_TO_INVOICE"] != null ? reader["READY_TO_INVOICE"].ToString() : "",
                                        PAY_SITE = reader["PAY_SITE"] != null ? reader["PAY_SITE"].ToString() : "",
                                        FREIGHT_TERMS = reader["FREIGHT_TERMS"] != null ? reader["FREIGHT_TERMS"].ToString() : "",
                                        INVOICE_ENABLED = reader["INVOICE_ENABLED"] != null ? reader["INVOICE_ENABLED"].ToString() : "",
                                        INVOICE_CREATED = reader["INVOICE_CREATED"] != null ? reader["INVOICE_CREATED"].ToString() : "",
                                        HOST_KEY = reader["HOST_KEY"] != null ? reader["HOST_KEY"].ToString() : "",
                                        PICKUP_DATE = reader["PICKUP_DATE"] != null ? reader["PICKUP_DATE"].ToString() : "",
                                        TRIP_ID = reader["TRIP_ID"] != null ? reader["TRIP_ID"].ToString() : "",
                                        EXPECTED_RECEIPT_DATE = reader["EXPECTED_RECEIPT_DATE"] != null ? reader["EXPECTED_RECEIPT_DATE"].ToString() : "",
                                        PROCESSING_STATUS = reader["PROCESSING_STATUS"] != null ? reader["PROCESSING_STATUS"].ToString() : "",
                                        PROCESSING_CODE = reader["PROCESSING_CODE"] != null ? reader["PROCESSING_CODE"].ToString() : "",
                                        DELIVERY_ID = reader["DELIVERY_ID"] != null ? reader["DELIVERY_ID"].ToString() : "",
                                        UNIT_OF_WEIGHT = reader["UNIT_OF_WEIGHT"] != null ? reader["UNIT_OF_WEIGHT"].ToString() : "",
                                        FSL_USED = reader["FSL_USED"] != null ? reader["FSL_USED"].ToString() : "",
                                        ERP_VENDOR_NAME = reader["ERP_VENDOR_NAME"] != null ? reader["ERP_VENDOR_NAME"].ToString() : ""
                                    }).ToList();
                    }


                }
                return packages;
            }
            catch (Exception ex)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                return null;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }

        }
        #endregion
    }
}
