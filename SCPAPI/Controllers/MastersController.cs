﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SCPAPI.Controllers
{
    public class MastersController : ApiController
    {
        DataTable dt;
        [HttpPost]
        public HttpResponseMessage GetMasters(string category)
        {
            try
            {
                if (category == "VendorNumber")
                    dt = SQLConnections.MyTable("select vendorNumber,vendorName from VendorMaster order by vendorNumber");
                if (category == "Host")
                    dt = SQLConnections.MyTable("Select distinct HostID from con_Hosts");
                if (category == "CarrierName")
                    dt = SQLConnections.MyTable("Select CarrierID,CarrierName from con_carrier");
                if (category == "CompanyType")
                    dt = SQLConnections.MyTable("Select company_type_id,company_type from con_company_types");
                if (category == "Country")
                    dt = SQLConnections.MyTable("Select countryId,countryCode,name from Country");
                if (category == "FieldLoveClass")
                    dt = SQLConnections.MyTable("Select distinct class_name_key from con_field_lov_types");
                if (category == "Applications")
                    dt = SQLConnections.MyTable("Select application_code,application_name from con_applications");
                if (category == "BuyerCompany")
                    dt = SQLConnections.MyTable("Select company_id,company_name from con_companies where company_type_id=2");

                if (category == "SupplierCompany")
                    dt = SQLConnections.MyTable("Select company_id,company_name from con_companies where company_type_id=1");
                if (category == "LookupCategory1")
                    dt = SQLConnections.MyTable("SELECT distinct[category1]  FROM [SCPDB].[dbo].[Con_lookups_master]");
                if (category == "LookupCategory2")
                    dt = SQLConnections.MyTable("SELECT distinct[category2]  FROM [SCPDB].[dbo].[Con_lookups_master]");
                if (category == "LookupCategory3")
                    dt = SQLConnections.MyTable("SELECT distinct[category3]  FROM [SCPDB].[dbo].[Con_lookups_master]");
                if (category == "LookupCategory4")
                    dt = SQLConnections.MyTable("SELECT distinct[category4]  FROM [SCPDB].[dbo].[Con_lookups_master]");
                if (category == "LookupCategory5")
                    dt = SQLConnections.MyTable("SELECT distinct[category5]  FROM [SCPDB].[dbo].[Con_lookups_master]");
                if (category == "FieldType")
                    dt = SQLConnections.MyTable("SELECT *  FROM [SCPDB].[dbo].[con_fieldtype] order by ID");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
