﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect.SetUp
{
    public class LookupsController : ApiController
    {
        public IEnumerable<con_lookups> GetLookups()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_lookups.ToList();
                return entity;
            }
        }
        public IEnumerable<con_lookups> GetLookupsByCategory(string category1)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_lookups.Where(p => p.category1 == category1).ToList();
                return entity;
            }
        }
     
        [HttpPost]
        public HttpResponseMessage AddLookups([FromBody] con_lookups bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {

                   
                    bts.creation_date = DateTime.Now;
                    bts.last_update_date = DateTime.Now;

                    entities.con_lookups.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.category1.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateLookups(con_lookups bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_lookups.FirstOrDefault(e => e.lookup_id == bld.lookup_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Category " + bld.category1.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.category1 = bld.category1;
                        entity.category2 = bld.category2;
                        entity.category3 = bld.category3;
                        entity.category4 = bld.category4;
                        entity.category5 = bld.category5;
                        entity.value = bld.value;
                        entity.description = bld.description;
                        entity.disabled_flag = bld.disabled_flag;
                        entity.last_update_date = DateTime.Now;
                        entity.last_update_by = bld.last_update_by;

                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteLookUp(Int64 lookupid)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_lookups.FirstOrDefault(e => e.lookup_id == lookupid);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Lookup Id = " + lookupid.ToString() + " not found to delete");
                    }
                    else
                    {
                        entities.con_lookups.Remove(entity);
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpPut]
        public HttpResponseMessage DisableLookup([FromBody] con_lookups bts)
        {
            try
            {               
                DataTable dt = SQLConnections.MyTable("Select * from con_lookups where lookup_id= '" + bts.lookup_id + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("update con_lookups set disabled_flag = '" + bts.disabled_flag + "' where lookup_id ='" + bts.lookup_id + "'");
                }
                
                var message = Request.CreateResponse(HttpStatusCode.Created, "Update Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.lookup_id.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
