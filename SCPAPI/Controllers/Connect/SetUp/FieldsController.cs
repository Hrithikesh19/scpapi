﻿using SCP.BAL;
using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect.SetUp
{
    public class FieldsController : ApiController
    {
        #region Declarations
        FieldsBAL fieldsBAL = null;

        #endregion
        public IEnumerable<con_fields> GetFields()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_fields.ToList();
                return entity;
            }
        }
        public List<Fields> GetFieldsByFolderID(int folderId)
        {
            fieldsBAL = FieldsBAL.Instance;
            return fieldsBAL.GetFields(folderId);
        }

        public FolderDetails GetFolderDetailsByPageName(string PageName, int folderid)
        {
            fieldsBAL = FieldsBAL.Instance;
            return fieldsBAL.GetFolderFields(PageName, folderid);
        }


        public IEnumerable<con_field_lov_types> GetFieldLoveClassByName(string classnamekey)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_field_lov_types.Where(p => p.class_name_key == classnamekey).ToList();
                return entity;
            }
        }

        public IEnumerable<con_fields> GetFieldById(int fieldId)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_fields.Where(p => p.field_id == fieldId).ToList();
                return entity;
            }
        }
        [HttpPost]
        public HttpResponseMessage AddFields([FromBody] con_fields bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    
                    bts.creation_date = DateTime.Now;                    
                    bts.last_update_date = DateTime.Now;

                    entities.con_fields.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.field_id.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateFields(con_fields bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_fields.FirstOrDefault(e => e.field_id == bld.field_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Field Id " + bld.field_id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.field_namespace_id = bld.field_namespace_id;
                        entity.field_name = bld.field_name;
                        entity.field_type = bld.field_type;
                        entity.description = bld.description;
                        entity.control_type = bld.control_type;
                        entity.wrap = bld.wrap;
                        entity.mode_id = bld.mode_id;
                        entity.field_lov_id = bld.field_lov_id;
                        entity.last_update_by = bld.last_update_by;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteFieldId(Int64 fieldid)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_fields.FirstOrDefault(e => e.field_id == fieldid);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Field Id = " + fieldid.ToString() + " not found to delete");
                    }
                    else
                    {
                        entities.con_fields.Remove(entity);
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddFieldResponsible([FromBody] FieldsResponsiblities bts)
        {
            try
            {
                string[] respIds = bts.Resp_Id.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_field_responsible where field_id= '" + bts.FieldId + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_field_responsible where field_id= '" + bts.FieldId + "'");
                }

                foreach (var item in respIds)
                {
                    SQLConnections.Execute("Insert into con_field_responsible values('" + bts.FieldId + "','" + item + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.lastupdatedBy + "', '" + DateTime.Now + "')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.FieldId.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        public HttpResponseMessage GetFieldsResponsible(Int64 fieldId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select cr.resp_name,cfr.field_id,cfr.field_resp_id from con_field_responsible cfr inner join con_responsibilities cr on cfr.field_resp_id = cr.resp_id where cfr.field_id='" + fieldId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    fieldId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
