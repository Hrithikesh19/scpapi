﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect.SetUp
{
    public class NamespacesController : ApiController
    {
        public IEnumerable<con_field_namespaces> GetNamespaces()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_field_namespaces.ToList();
                return entity;
            }
        }
        public IEnumerable<con_field_namespaces> GetFieldNamespaceByID(Int64 namespaceId)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_field_namespaces.Where(p => p.field_namespace_id == namespaceId).ToList();
                return entity;
            }
        }


        [HttpPost]
        public HttpResponseMessage AddNameSpaces([FromBody] con_field_namespaces bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {

                    bts.creation_date = DateTime.Now;
                    bts.last_update_date = DateTime.Now;

                    entities.con_field_namespaces.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.namespace_name.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateNameSpaces(con_field_namespaces bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_field_namespaces.FirstOrDefault(e => e.field_namespace_id == bld.field_namespace_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Name Space Id " + bld.field_namespace_id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.namespace_name = bld.namespace_name;
                        entity.class_name = bld.class_name;
                        entity.description = bld.description;
                        entity.last_update_by = bld.last_update_by;
                        entity.last_update_date = DateTime.Now;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteNameSpace(Int64 namespaceid)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_field_namespaces.FirstOrDefault(e => e.field_namespace_id == namespaceid);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Name Space Id = " + namespaceid.ToString() + " not found to delete");
                    }
                    else
                    {
                        entities.con_field_namespaces.Remove(entity);
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
