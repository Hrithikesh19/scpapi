﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect.SetUp
{
    public class PagesController : ApiController
    {
        public IEnumerable<con_pages> GetPages()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_pages.ToList();
                return entity;
            }
        }
        public IEnumerable<con_pages> GetPageById(Int64 PageId)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_pages.Where(p => p.page_id == PageId).ToList();
                return entity;
            }
        }

        public IEnumerable<con_pages> GetPageByAppicationCode(string Appcode)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_pages.Where(p => p.application_code == Appcode).ToList();
                return entity;
            }
        }


        [HttpPost]
        public HttpResponseMessage AddPages([FromBody] con_pages bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                   
                    bts.creation_date = DateTime.Now;                    
                    bts.last_update_date = DateTime.Now;

                    entities.con_pages.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.page_id.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]        
        public HttpResponseMessage UpdatePages(con_pages bld)
        {
            try
            {               
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_pages.FirstOrDefault(e => e.page_id == bld.page_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Page Id " + bld.page_id.ToString() + " not found to update");
                    }
                    else
                    {
                        //entity.name = bld.name;
                        entity.description = bld.description;
                        entity.application_code = bld.application_code;
                        entity.last_update_date = DateTime.Now;
                        entity.last_update_by = bld.last_update_by;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeletePageId(Int64 PageId)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_pages.FirstOrDefault(e => e.page_id == PageId);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Page Id = " + PageId.ToString() + " not found to delete");
                    }
                    else
                    {
                        entities.con_pages.Remove(entity);
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddPageNamespaces([FromBody] PageNamespaces bts)
        {
            try
            {
                string[] nameSpaceIds = bts.field_namespace_id.Split(',');                                    
                DataTable dt = SQLConnections.MyTable("Select * from con_page_field_namespaces where page_id= '" + bts.PageId + "'");
                if(dt.Rows.Count>1)
                {
                    SQLConnections.Execute("Delete from con_page_field_namespaces where page_id= '" + bts.PageId + "'");                    
                }

                foreach (var item in nameSpaceIds)
                {
                    SQLConnections.Execute("Insert into  con_page_field_namespaces values('" + item + "','" + bts.PageId + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.lastupdatedBy + "','" + DateTime.Now + "')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.PageId.ToString());

                    return message;
               // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        
        public HttpResponseMessage GetPageNamespaces(Int64 PageId)
        {
            try
            {
                               
                DataTable dt = SQLConnections.MyTable("Select cfn.namespace_name,cpfn.page_id,cpfn.field_namespace_id from con_page_field_namespaces cpfn inner join con_field_namespaces cfn on cpfn.field_namespace_id = cfn.field_namespace_id where cpfn.page_id= '" + PageId + "'");
                
                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    PageId.ToString());

                return message;                
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddPageResponsible([FromBody] PageResponsiblities bts)
        {
            try
            {
                string[] respIds = bts.Resp_Id.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_page_row_renderers where page_id= '" + bts.PageId + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_page_row_renderers where page_id= '" + bts.PageId + "'");
                }

                foreach (var item in respIds)
                {
                    SQLConnections.Execute("Insert into con_page_row_renderers values('" + bts.PageId + "','" + item + "','1')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.PageId.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage GetPageResponsible(Int64 PageId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select cr.resp_name,cprr.page_id,cprr.row_renderer_id from con_page_row_renderers cprr inner join con_responsibilities cr on cprr.row_renderer_id = cr.resp_id where cprr.page_id='" + PageId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    PageId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
