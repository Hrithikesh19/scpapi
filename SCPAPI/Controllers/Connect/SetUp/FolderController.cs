﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SCP.BAL;
using SCP.BO;
using UserDataAccess;
using System.Data;
using System.Data.SqlClient;

namespace SCPAPI.Controllers.Connect.SetUp
{
    public class FolderController : ApiController
    {
        #region Declarations
        FoldersBAL foldersBAL = null;
        string[] groupids;
        Int64 folderid;
        #endregion
        public IEnumerable<con_folders> GetFolders()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_folders.ToList();
                return entity;
            }
        }
        public List<Folders> GetFolderByPageName(string pageName)
        {
            foldersBAL = FoldersBAL.Instance;
            return foldersBAL.GetFolders(pageName);
        }

        public HttpResponseMessage GetPagesByApplication(string AppCode)
        {
            try
            {
                DataTable dt = SQLConnections.MyTable("select page_id,name from con_pages where application_code='" + AppCode + "'");

                var message = Request.CreateResponse(HttpStatusCode.OK, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    AppCode.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage GetNamespacesByPageID(Int64 PageId)
        {
            try
            {
                DataTable dt = SQLConnections.MyTable("select cfn.namespace_name, cpfn.field_namespace_id from con_page_field_namespaces cpfn inner join con_field_namespaces cfn on cfn.field_namespace_id= cpfn.field_namespace_id where cpfn.page_id= '" + PageId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    PageId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage GetAvailableFiledsByNamespaceId(Int64 namespaceid)
        {
            try
            {
                DataTable dt = SQLConnections.MyTable("select cf.field_id,cf.field_name,cf.field_namespace_id,cf.field_type,cf.description,CONCAT(cfn.namespace_name, '.', cf.field_name) DisplayText   from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id= cfn.field_namespace_id where cf.field_namespace_id='" + namespaceid + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    namespaceid.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddFolder([FromBody] InsertFolder bts)
        {
            try
            {
                string[] fieldids = bts.fieldid.Split(',');
                int i = 0;
                string insertSql = "Insert into  con_folders (folder_name,page_id,application_code,user_created,field_namespace_id,created_by,creation_date,last_update_by,last_update_date) values (@foldername,@pageid,@appcode,@usercreated,@field_namespace_id,@createdBy,@creation_date,@lastupdatedBy,@last_update_date);SELECT @@IDENTITY";
                using (SqlConnection con = new SqlConnection(Config.SQLcon))
                {
                    using (SqlCommand cmd = new SqlCommand(insertSql, con))
                    {
                        con.Open();

                        cmd.Parameters.AddWithValue("@foldername", bts.foldername);
                        cmd.Parameters.AddWithValue("@pageid", bts.pageid);
                        cmd.Parameters.AddWithValue("@appcode", bts.appcode);
                        cmd.Parameters.AddWithValue("@usercreated", bts.usercreated);
                        cmd.Parameters.AddWithValue("@field_namespace_id", bts.field_namespace_id);
                        cmd.Parameters.AddWithValue("@createdBy", bts.createdBy);
                        cmd.Parameters.AddWithValue("@creation_date", DateTime.Now);
                        cmd.Parameters.AddWithValue("@lastupdatedBy", bts.lastupdatedBy);
                        cmd.Parameters.AddWithValue("@last_update_date", DateTime.Now);
                        

                        //folderid = cmd.ExecuteNonQuery();
                        folderid = Convert.ToInt64(cmd.ExecuteScalar());

                        if (con.State == System.Data.ConnectionState.Open) con.Close();
                    }
                }
                foreach (var fieldid in fieldids)
                {
                    i = i + 1;                    
                    SQLConnections.Execute("Insert into  con_folder_details (folder_id,field_id,sequence,created_by,creation_date,last_update_by,last_update_date)values('" + folderid + "','" + fieldid + "','" + i + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.createdBy + "','" + DateTime.Now + "')");
                }

                if (bts.groupid != string.Empty)
                {
                    groupids = bts.groupid.Split(',');
                    foreach (var gropuod in groupids)
                    {
                        //SQLConnections.Execute("Insert into  con_folder_details (folder_id,field_id,order_by,sequence,created_by,creation_date,last_update_by,last_update_date)values('" + folderid + "','" + fieldid + "','" + gropuod + "','" + i + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.lastupdatedBy + "','" + DateTime.Now + "')");
                        SQLConnections.Execute("update con_folder_details set order_by = '" + gropuod + "' where field_id='" + gropuod + "'");
                    }
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, "Successfully Created Folder ID " + folderid);
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.foldername.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateFolder([FromBody] InsertFolderDetails bts)
        {
            try
            {
                string[] fieldids = bts.fieldid.Split(',');
                int i = 0;
                string msg = string.Empty;
                DataTable dt = SQLConnections.MyTable("Select * from con_folders where folder_id= '" + bts.folderid + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("update con_folders set  folder_name= '" + bts.foldername + "' where folder_id= '" + bts.folderid + "'");
                    SQLConnections.Execute("Delete from con_folder_details where folder_id= '" + bts.folderid + "'");

                    foreach (var fieldid in fieldids)
                    {
                        i = i + 1;
                        //if (bts.groupid != string.Empty)
                        //{                           
                        //    groupids = bts.groupid.Split(',');
                        //    foreach (var gropuod in groupids)
                        //    {
                        //        SQLConnections.Execute("Insert into  con_folder_details (folder_id,field_id,order_by,sequence,created_by,creation_date,last_update_by,last_update_date) values ('" + bts.folderid + "','" + fieldid + "','" + gropuod + "','" + i + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.lastupdatedBy + "','" + DateTime.Now + "')");
                        //    }
                        //}
                        SQLConnections.Execute("Insert into  con_folder_details (folder_id,field_id,sequence,created_by,creation_date,last_update_by,last_update_date) values ('" + bts.folderid + "','" + fieldid + "','" + i + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.lastupdatedBy + "','" + DateTime.Now + "')");
                    }

                    if (bts.groupid != string.Empty)
                    {
                        groupids = bts.groupid.Split(',');
                        foreach (var gropuod in groupids)
                        {
                            //SQLConnections.Execute("Insert into  con_folder_details (folder_id,field_id,order_by,sequence,created_by,creation_date,last_update_by,last_update_date)values('" + folderid + "','" + fieldid + "','" + gropuod + "','" + i + "','" + bts.createdBy + "','" + DateTime.Now + "','" + bts.lastupdatedBy + "','" + DateTime.Now + "')");
                            SQLConnections.Execute("update con_folder_details set order_by = '" + gropuod + "' where field_id='" + gropuod + "'");
                        }
                    }

                    msg = "Update Successfull FolderID " + bts.folderid;
                }
                else
                {
                    msg = "Folder Id " + bts.folderid.ToString() + " not found to update";
                }
                                
                var message = Request.CreateResponse(HttpStatusCode.Created, msg);
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.foldername.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteFolder(Int64 folderid)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_folders.FirstOrDefault(e => e.folder_id == folderid);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Folder Id = " + folderid.ToString() + " not found to delete");
                    }
                    else
                    {
                        SQLConnections.Execute("Delete from con_folders where folder_id= '" + folderid + "'");
                        SQLConnections.Execute("Delete from con_folder_details where folder_id= '" + folderid + "'");

                    }

                    var message = Request.CreateResponse(HttpStatusCode.OK, "Deleted Folder ID " + entity.folder_id );
                    message.Headers.Location = new Uri(Request.RequestUri +
                        folderid.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
