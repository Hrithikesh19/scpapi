﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect.Company
{
    

    public class CompaniesController : ApiController
    {
        string Conditions = string.Empty;
        public IEnumerable<con_companies> GetCompanies()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_companies.ToList();
                return entity;
            }
        }
        public IEnumerable<con_companies> GetCompanyByID(Int64 companyid)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_companies.Where(p => p.company_id == companyid).ToList();
                return entity;
            }
        }
       
        
        [HttpPost]
        public HttpResponseMessage AddCompanies([FromBody] con_companies bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {                   
                    bts.creation_date = DateTime.Now;                    
                    bts.last_update_date = DateTime.Now;
                    entities.con_companies.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.company_id.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest,  ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateCompanies(con_companies bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_companies.FirstOrDefault(e => e.company_id == bld.company_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Company Id " + bld.company_id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.company_name = bld.company_name;
                        entity.COMPANY_EMAIL = bld.COMPANY_EMAIL;
                        entity.company_type_id = bld.company_type_id;
                        entity.min_order_date = bld.min_order_date;
                        entity.disabled_flag = bld.disabled_flag;
                        entity.XC_ENABLED = bld.XC_ENABLED;
                        entity.ALLOW_ADV_PACK = bld.ALLOW_ADV_PACK;
                        entity.VIEW_ECO_DOCUMENTS = bld.VIEW_ECO_DOCUMENTS;
                        entity.QUAL_CONTROL = bld.QUAL_CONTROL;
                        entity.last_update_by = bld.last_update_by;
                        entity.last_update_date = DateTime.Now;                      
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage DisableFlag(con_companies bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_companies.FirstOrDefault(e => e.company_id == bld.company_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Company Id = " + bld.company_id.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.disabled_flag = bld.disabled_flag;                       
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddCompanyVendors([FromBody] CompanyVendors bts)
        {
            try
            {
                string[] VendorIds = bts.VendorNumber.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_company_vendors where CompanyID= '" + bts.CompanyID + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_company_vendors where CompanyID= '" + bts.CompanyID + "'");
                }

                foreach (var item in VendorIds)
                {
                    SQLConnections.Execute("Insert into  con_company_vendors values('" + bts.CompanyID + "','" + item + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.CompanyID.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        public HttpResponseMessage GetCompanyVendors(Int64 CompanyId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select v.vendorNumber,cv.CompanyID,v.vendorName from con_company_vendors cv inner join VendorMaster v on cv.VendorID = v.vendorNumber WHERE CV.CompanyID = '" + CompanyId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    CompanyId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddCompanyHosts([FromBody] CompanyHosts bts)
        {
            try
            {
                string[] hostIds = bts.HostID.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_company_hosts where companyID= '" + bts.CompanyID + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_company_hosts where companyID= '" + bts.CompanyID + "'");
                }

                foreach (var item in hostIds)
                {
                    SQLConnections.Execute("Insert into con_company_hosts values('" + bts.CompanyID + "','" + item + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.HostID.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage GetCompanyHosts(Int64 CompanyId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select hostid,companyId from con_company_hosts WHERE companyId = '" + CompanyId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    CompanyId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
