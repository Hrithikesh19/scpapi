﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect
{
    public class GlobalSearchController : ApiController
    {
        string Conditions = string.Empty;
        string DisableFlag;
        DateTime minorderstart, minorderend;
        DataTable dt;
        string userids, UserID;

        [HttpPost]
        public HttpResponseMessage GetAllCompanies([FromBody] Companies cnc)
        {
            try
            {

                if (cnc.min_order_date.ToString() != string.Empty)
                {
                    minorderstart = Convert.ToDateTime(cnc.min_order_date).AddDays(30);
                }
                if (cnc.min_order_date_end.ToString() != string.Empty)
                {
                    minorderend = Convert.ToDateTime(cnc.min_order_date_end).AddDays(-30);
                }


                if (cnc.company_name != string.Empty)
                {
                    Conditions += " and company_name='" + cnc.company_name + "'";
                }

                if (cnc.company_type_id.ToString() != string.Empty)
                {
                    Conditions += " and company_type_id='" + cnc.company_type_id + "'";
                }

                if (cnc.min_order_date.ToString() != string.Empty && cnc.min_order_date_end.ToString() != string.Empty)
                {
                    Conditions += " and min_order_date between '" + cnc.min_order_date + "' and '" + cnc.min_order_date_end + "'";
                }
                else
                if (cnc.min_order_date.ToString() != string.Empty)
                {
                    Conditions += " and min_order_date >= '" + minorderstart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }
                else if (cnc.min_order_date_end.ToString() != string.Empty)
                {
                    Conditions += " and min_order_date <= '" + minorderend.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }

                //if (cnc.COMPANY_PRINT_TO_PDF != string.Empty)
                //{
                //    Conditions += " and COMPANY_PRINT_TO_PDF='" + cnc.COMPANY_PRINT_TO_PDF + "'";
                //}

                if (cnc.disabled_flag == "Y")
                {
                    DisableFlag = "('Y')";
                }
                if (cnc.disabled_flag == "N")
                {
                    DisableFlag = "('N')";
                }
                if (cnc.disabled_flag == "ALL")
                {
                    DisableFlag = "('Y','N')";
                }

                DataTable dt = SQLConnections.MyTable("Select * from con_companies where disabled_flag in " + DisableFlag + "" + Conditions);
                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.company_id.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAllUsers([FromBody] Users cnc)
        {
            try
            {
                if (cnc.contact_name != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.contact_name='" + cnc.contact_name + "'";
                    }
                    else
                    {
                        Conditions += " and cu.contact_name='" + cnc.contact_name + "'";
                    }

                }

                if (cnc.user_type == "ALL")
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.user_type  in ('1','2','3')";
                    }
                    else
                    {
                        Conditions += " and cu.user_type  in ('1','2','3')";
                    }

                }
                else
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {

                        Conditions += " and cu.user_type='" + cnc.user_type + "'";
                    }
                    else
                    {
                        Conditions += " and cu.user_type='" + cnc.user_type + "'";
                    }

                }

                if (cnc.disabled_flag == "Y")
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.disabled_flag  in ('Y')";
                    }
                    else
                    {
                        Conditions += " and cu.disabled_flag  in ('Y')";
                    }

                }
                if (cnc.disabled_flag == "N")
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.disabled_flag  in ('N')";
                    }
                    else
                    {
                        Conditions += " and cu.disabled_flag  in (' N')";
                    }
                }
                if (cnc.disabled_flag == "ALL")
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.disabled_flag  in ('Y','N')";
                    }
                    else
                    {
                        Conditions += " and cu.disabled_flag  in ('Y','N')";
                    }
                }

                if (cnc.country != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.country='" + cnc.country + "'";
                    }
                    else
                    {
                        Conditions += " and cu.country='" + cnc.country + "'";
                    }

                }


                if (cnc.lostloginstartdays.ToString() != string.Empty && cnc.lostloginenddays.ToString() != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.last_login_date between '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginstartdays)).ToString("yyyy-MM-dd HH:mm:ss") + "' and '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginenddays)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                    else
                    {
                        Conditions += " and cu.last_login_date between '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginstartdays)).ToString("yyyy-MM-dd HH:mm:ss") + "' and '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginenddays)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }

                }
                else if (cnc.lostloginstartdays.ToString() != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.last_login_date >= '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginstartdays)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                    else
                    {
                        Conditions += " and cu.last_login_date >= '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginstartdays)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                }
                else if (cnc.lostloginenddays.ToString() != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.last_login_date <= '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginenddays)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                    else
                    {
                        Conditions += " and cu.last_login_date <= '" + DateTime.Now.AddDays(Convert.ToInt32(cnc.lostloginenddays)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                }

                if (cnc.password_expiration_date.ToString() == "Y")
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        Conditions += " and cu.password_expiration_date <='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                    else
                    {
                        Conditions += " and cu.password_expiration_date <='" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                    }
                }


                if (cnc.company_id.ToString() != string.Empty && cnc.user_name.ToString() != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        DataTable userdt = SQLConnections.MyTable("select* from con_users where user_name='" + cnc.user_name + "'");

                        if (userdt.Rows.Count > 0)
                        {
                            foreach (DataRow row in userdt.Rows)
                            {
                                UserID = row["user_id"].ToString();
                            }
                        }

                        dt = SQLConnections.MyTable("SELECT cu.user_id,cu.user_name,cu.user_type,cu.password,cu.company_id,cc.company_name,cu.contact_name,cu.contact_email,cu.contact_phone, cu.locale_name, cu.disabled_flag, cu.last_login_date, cu.password_expiration_date, cu.status, cu.country from con_users cu inner join con_companies cc     on cc.company_id = cu.company_id inner join con_user_roles cur on cur.user_id = cu.user_id where cu.company_id='" + cnc.company_id + "' and cu.user_name='" + cnc.user_name + "' and cur.role_id = '" + Convert.ToInt32(cnc.roleid.ToString()) + "'" + Conditions);
                    }
                    else
                    {
                        dt = SQLConnections.MyTable("SELECT cu.user_id,cu.user_name,cu.user_type,cu.password,cu.company_id,cc.company_name,cu.contact_name,cu.contact_email,cu.contact_phone, cu.locale_name, cu.disabled_flag, cu.last_login_date, cu.password_expiration_date, cu.status, cu.country  from con_users cu   inner join con_companies cc     on cc.company_id = cu.company_id  where cu.company_id='" + cnc.company_id + "' and cu.user_name='" + cnc.user_name + "'" + Conditions);
                    }

                }
                else if (cnc.user_name.ToString() != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {
                        DataTable userdt = SQLConnections.MyTable("select* from con_users where user_name='" + cnc.user_name + "'");

                        if (userdt.Rows.Count > 0)
                        {
                            foreach (DataRow row in userdt.Rows)
                            {
                                UserID = row["user_id"].ToString();
                            }
                        }

                        dt = SQLConnections.MyTable("SELECT cu.user_id,cu.user_name,cu.user_type,cu.password,cu.company_id,cc.company_name,cu.contact_name,cu.contact_email,cu.contact_phone, cu.locale_name, cu.disabled_flag, cu.last_login_date, cu.password_expiration_date, cu.status, cu.country from con_users cu inner join con_companies cc     on cc.company_id = cu.company_id inner join con_user_roles cur on cur.user_id = cu.user_id where cu.user_name='" + cnc.user_name + "' and cur.role_id = '" + Convert.ToInt32(cnc.roleid.ToString()) + "'" + Conditions);
                    }
                    else
                    {
                        dt = SQLConnections.MyTable("SELECT cu.user_id,cu.user_name,cu.user_type,cu.password,cu.company_id,cc.company_name,cu.contact_name,cu.contact_email,cu.contact_phone, cu.locale_name, cu.disabled_flag, cu.last_login_date, cu.password_expiration_date, cu.status, cu.country  from con_users cu   inner join con_companies cc     on cc.company_id = cu.company_id  where cu.user_name='" + cnc.user_name + "'" + Conditions);
                    }

                }
                else if (cnc.company_id.ToString() != string.Empty)
                {
                    if (cnc.roleid.ToString() != string.Empty)
                    {

                        //DataTable userdt = SQLConnections.MyTable("select* from con_users where company_id='" + cnc.company_id + "'");

                        //if (userdt.Rows.Count > 0)
                        //{
                        //    foreach (DataRow row in userdt.Rows)
                        //    {
                        //        userids += row["user_id"].ToString() + ",";
                        //    }
                        //}

                        //userids = userids.Remove(userids.Length - 1, 1);

                        dt = SQLConnections.MyTable("SELECT cu.user_id,cu.user_name,cu.user_type,cu.password,cu.company_id,cc.company_name,cu.contact_name,cu.contact_email,cu.contact_phone, cu.locale_name, cu.disabled_flag, cu.last_login_date, cu.password_expiration_date, cu.status, cu.country from con_users cu inner join con_companies cc     on cc.company_id = cu.company_id inner join con_user_roles cur on cur.user_id = cu.user_id where cu.company_id='" + cnc.company_id + "' and cur.role_id = '" + Convert.ToInt32(cnc.roleid.ToString()) + "'" + Conditions);
                    }
                    else
                    {
                        dt = SQLConnections.MyTable("SELECT cu.user_id,cu.user_name,cu.user_type,cu.password,cu.company_id,cc.company_name,cu.contact_name,cu.contact_email,cu.contact_phone, cu.locale_name, cu.disabled_flag, cu.last_login_date, cu.password_expiration_date, cu.status, cu.country  from con_users cu   inner join con_companies cc     on cc.company_id = cu.company_id  where cu.company_id='" + cnc.company_id + "'" + Conditions);
                    }

                }
                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.company_id.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Responsibilities([FromBody] con_responsibilities cnc)
        {
            try
            {
                if (cnc.resp_name != string.Empty)
                {
                    Conditions += " and resp_name='" + cnc.resp_name + "'";
                }

                if (cnc.disabled_flag == "Y")
                {
                    DisableFlag = "('Y')";
                }
                if (cnc.disabled_flag == "N")
                {
                    DisableFlag = "('N')";
                }
                if (cnc.disabled_flag == "ALL")
                {
                    DisableFlag = "('Y','N')";
                }


                dt = SQLConnections.MyTable("Select * from con_responsibilities where disabled_flag in " + DisableFlag + "" + Conditions);

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.disabled_flag.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [HttpPost]
        public HttpResponseMessage Roles([FromBody] con_roles cnc)
        {
            try
            {
                if (cnc.role_name != string.Empty)
                {
                    Conditions += " and role_name='" + cnc.role_name + "'";
                }

                if (cnc.disabled_flag == "Y")
                {
                    DisableFlag = "('Y')";
                }
                if (cnc.disabled_flag == "N")
                {
                    DisableFlag = "('N')";
                }
                if (cnc.disabled_flag == "ALL")
                {
                    DisableFlag = "('Y','N')";
                }


                dt = SQLConnections.MyTable("Select * from con_roles where disabled_flag in " + DisableFlag + "" + Conditions);

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.disabled_flag.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage BusinessGroups([FromBody] con_BusinessGroups cnc)
        {
            try
            {
                if (cnc.BusinessGroupName != string.Empty)
                {
                    Conditions += " and BusinessGroupName='" + cnc.BusinessGroupName + "'";
                }

                if (cnc.DisableFlag == "Y")
                {
                    DisableFlag = "('Y')";
                }
                if (cnc.DisableFlag == "N")
                {
                    DisableFlag = "('N')";
                }
                if (cnc.DisableFlag == "ALL")
                {
                    DisableFlag = "('Y','N')";
                }


                dt = SQLConnections.MyTable("Select * from con_BusinessGroups where DisableFlag in " + DisableFlag + "" + Conditions);

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.DisableFlag.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [HttpPost]
        public HttpResponseMessage Namespaces([FromBody] con_field_namespaces cnc)
        {
            try
            {
                if (cnc.namespace_name != string.Empty && cnc.class_name != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select * from con_field_namespaces where namespace_name = '" + cnc.namespace_name + "' and class_name='" + cnc.class_name + "'");
                }
                else if (cnc.namespace_name != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select * from con_field_namespaces where namespace_name = '" + cnc.namespace_name + "'");
                }
                else if (cnc.class_name != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select * from con_field_namespaces where class_name = '" + cnc.class_name + "'");
                }
                else
                {
                    dt = SQLConnections.MyTable("Select * from con_field_namespaces");
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.namespace_name.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Pages([FromBody] con_pages cnc)
        {
            try
            {
                if (cnc.name != string.Empty && cnc.description != string.Empty && cnc.application_code != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where cp.name = '" + cnc.name + "' and cp.description='" + cnc.description + "' and cp.application_code='" + cnc.application_code + "'");
                }
                else if (cnc.name != string.Empty && cnc.description != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where cp.name = '" + cnc.name + "' and cp.description='" + cnc.description + "'");
                }
                else if (cnc.description != string.Empty && cnc.application_code != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where  cp.description='" + cnc.description + "' and cp.application_code='" + cnc.application_code + "'");
                }
                else if (cnc.name != string.Empty && cnc.application_code != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where cp.name = '" + cnc.name + "' and cp.application_code='" + cnc.application_code + "'");
                }
                else if (cnc.name != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where cp.name = '" + cnc.name + "'");
                }
                else if (cnc.description != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where cp.description = '" + cnc.description + "'");
                }
                else if (cnc.application_code != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code where cp.application_code = '" + cnc.application_code + "'");
                }
                else
                {
                    dt = SQLConnections.MyTable("Select cp.page_id,cp.name,cp.description,cp.filename,cp.application_code,cp.created_by,cp.creation_date,cp.last_update_by,cp.last_update_date ,ca.application_name from con_pages cp inner join con_applications ca on cp.application_code=ca.application_code");
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.name.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Fields([FromBody] confields cnc)
        {
            try
            {
                if (cnc.field_name != string.Empty && cnc.field_type.ToString() != string.Empty && cnc.field_namespace_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where cf.field_name = '" + cnc.field_name + "' and cf.field_type='" + cnc.field_type + "' and cf.field_namespace_id='" + cnc.field_namespace_id + "'");
                }
                else if (cnc.field_name != string.Empty && cnc.field_type.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where cf.field_name = '" + cnc.field_name + "' and cf.field_type='" + cnc.field_type + "'");
                }
                else if (cnc.field_type.ToString() != string.Empty && cnc.field_namespace_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where  cf.field_type='" + cnc.field_type + "' and cf.field_namespace_id='" + cnc.field_namespace_id + "'");
                }
                else if (cnc.field_name != string.Empty && cnc.field_namespace_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where cf.field_name = '" + cnc.field_name + "' and cf.field_namespace_id='" + cnc.field_namespace_id + "'");
                }
                else if (cnc.field_name != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where cf.field_name = '" + cnc.field_name + "'");
                }
                else if (cnc.field_type.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where cf.field_type = '" + cnc.field_type + "'");
                }
                else if (cnc.field_namespace_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type where cf.field_namespace_id = '" + cnc.field_namespace_id + "'");
                }
                else
                {
                    dt = SQLConnections.MyTable("Select cf.field_id,cf.field_namespace_id,cfn.namespace_name,cf.field_name,cf.description,cf.control_type,cf.field_type as fieldtypeId,cft.FieldType,cf.wrap,cf.mode_id,cf.field_lov_id,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_by,cf.DISABLED_FLAG from con_fields cf inner join con_field_namespaces cfn on cf.field_namespace_id=cfn.field_namespace_id inner join con_fieldtype  cft on cft.ID = cf.field_type");
                }

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.field_name.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAllFolders([FromBody] folders cnc)
        {
            try
            {
                if (cnc.folder_name != string.Empty && cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date >= '" + cnc.last_update_date + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date <= '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.application_code.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.application_code='" + cnc.application_code + "'  and cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name.ToString() != string.Empty && cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "'");
                }
                else if (cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date >= '" + cnc.last_update_date + "'");
                }
                else if (cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.page_id='" + cnc.page_id + "' and cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name='" + cnc.folder_name + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date >= '" + cnc.last_update_date + "'");
                }
                else if (cnc.folder_name.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name='" + cnc.folder_name + "' and cf.page_id='" + cnc.page_id + "' and cf.last_update_date <= '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name='" + cnc.folder_name + "' and cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.application_code.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.application_code='" + cnc.application_code + "'");
                }
                else if (cnc.application_code.ToString() != string.Empty && cnc.page_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.application_code='" + cnc.application_code + "' and cf.page_id='" + cnc.page_id + "'");
                }
                else if (cnc.page_id.ToString() != string.Empty && cnc.last_update_date.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.page_id='" + cnc.page_id + "' and cf.last_update_date >= '" + cnc.last_update_date + "'");
                }
                else if (cnc.page_id.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.page_id='" + cnc.page_id + "' and cf.last_update_date <= '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.last_update_date.ToString() != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.last_update_date between '" + cnc.last_update_date + "' and '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.page_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "' and cf.page_id='" + cnc.page_id + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.last_update_date.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name='" + cnc.folder_name + "' and cf.last_update_date >= '" + cnc.last_update_date + "'");
                }
                else if (cnc.folder_name != string.Empty && cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name='" + cnc.folder_name + "' and cf.last_update_date <= '" + cnc.last_update_date_end + "'");
                }
                else if (cnc.folder_name != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.folder_name = '" + cnc.folder_name + "'");
                }
                else if (cnc.application_code.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.application_code='" + cnc.application_code + "'");
                }
                else if (cnc.page_id.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where cf.page_id='" + cnc.page_id + "'");
                }
                else if (cnc.last_update_date.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where  cf.last_update_date >= '" + cnc.last_update_date + "'");
                }
                else if (cnc.last_update_date_end.ToString() != string.Empty)
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id  where  cf.last_update_date >= '" + cnc.last_update_date_end + "'");
                }
                else
                {
                    dt = SQLConnections.MyTable("Select cf.folder_id,cf.folder_name,cf.page_id,cp.name,cf.inactive_date,cf.application_code,ca.application_name,cf.user_id,cf.company_id,cf.user_created,cf.created_by,cf.creation_date,cf.last_update_by,cf.last_update_date,cf.field_namespace_id,cfn.namespace_name from con_folders cf inner join con_applications ca on ca.application_code=cf.application_code inner join con_field_namespaces cfn on cfn.field_namespace_id= cf.field_namespace_id inner join con_pages cp on cp.page_id=cf.page_id ");
                }

                


                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.folder_name.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage LookUps([FromBody] Lookups cnc)
        {
            try
            {

                if (cnc.creation_date.ToString() != string.Empty)
                {
                    minorderstart = Convert.ToDateTime(cnc.creation_date).AddDays(30);
                }
                if (cnc.creation_date_end.ToString() != string.Empty)
                {
                    minorderend = Convert.ToDateTime(cnc.creation_date_end).AddDays(-30);
                }


                if (cnc.category1 != string.Empty)
                {
                    Conditions += " and category1='" + cnc.category1 + "'";
                }
                if (cnc.category2 != string.Empty)
                {
                    Conditions += " and category2='" + cnc.category2 + "'";
                }
                if (cnc.category3 != string.Empty)
                {
                    Conditions += " and category3='" + cnc.category3 + "'";
                }
                if (cnc.category4 != string.Empty)
                {
                    Conditions += " and category4='" + cnc.category4 + "'";
                }
                if (cnc.category5 != string.Empty)
                {
                    Conditions += " and category5='" + cnc.category5 + "'";
                }
                if (cnc.value != string.Empty)
                {
                    Conditions += " and value='" + cnc.value + "'";
                }

                if (cnc.creation_date.ToString() != string.Empty && cnc.creation_date_end.ToString() != string.Empty)
                {
                    Conditions += " and creation_date between '" + cnc.creation_date + "' and '" + cnc.creation_date_end + "'";
                }
                else if (cnc.creation_date.ToString() != string.Empty)
                {
                    Conditions += " and creation_date >= '" + minorderstart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }
                else if (cnc.creation_date_end.ToString() != string.Empty)
                {
                    Conditions += " and creation_date <= '" + minorderend.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }



                if (cnc.disabled_flag == "Y")
                {
                    DisableFlag = "('Y')";
                }
                if (cnc.disabled_flag == "N")
                {
                    DisableFlag = "('N')";
                }
                if (cnc.disabled_flag == "ALL")
                {
                    DisableFlag = "('Y','N')";
                }

                DataTable dt = SQLConnections.MyTable("Select * from con_lookups where disabled_flag in " + DisableFlag + "" + Conditions);
                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.value.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage GetAllMaterials([FromBody] materials cnc)
        {
            try
            {

                if (cnc.LasteUpdateDate.ToString() != string.Empty)
                {
                    minorderstart = Convert.ToDateTime(cnc.LasteUpdateDate).AddDays(30);
                }
                if (cnc.LasteUpdateDateEnd.ToString() != string.Empty)
                {
                    minorderend = Convert.ToDateTime(cnc.LasteUpdateDateEnd).AddDays(-30);
                }


                if (cnc.VendorNumber != string.Empty)
                {
                    Conditions += " and VendorNumber='" + cnc.VendorNumber + "'";
                }

                if (cnc.VendorName != string.Empty)
                {
                    Conditions += " and VendorName='" + cnc.VendorName + "'";
                }

                if (cnc.AMATMaterialNumber != string.Empty)
                {
                    Conditions += " and AMATMaterialNumber='" + cnc.AMATMaterialNumber + "'";
                }
                if (cnc.VendorMaterialNumber != string.Empty)
                {
                    Conditions += " and VendorMaterialNumber='" + cnc.VendorMaterialNumber + "'";
                }

                if (cnc.CaseQuantity != string.Empty)
                {
                    Conditions += " and CaseQuantity='" + cnc.CaseQuantity + "'";
                }

                if (cnc.CountryofOrginCode != string.Empty)
                {
                    Conditions += " and CountryofOrginCode='" + cnc.CountryofOrginCode + "'";
                }

                if (cnc.CountryofOrginMark != string.Empty)
                {
                    Conditions += " and CountryofOrginMark='" + cnc.CountryofOrginMark + "'";
                }

                if (cnc.LasteUpdateDate.ToString() != string.Empty && cnc.LasteUpdateDateEnd.ToString() != string.Empty)
                {
                    Conditions += " and LasteUpdateDate between '" + cnc.LasteUpdateDate + "' and '" + cnc.LasteUpdateDateEnd + "'";
                }
                else
                if (cnc.LasteUpdateDate.ToString() != string.Empty)
                {
                    Conditions += " and LasteUpdateDate >= '" + minorderstart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }
                else if (cnc.LasteUpdateDateEnd.ToString() != string.Empty)
                {
                    Conditions += " and LasteUpdateDate <= '" + minorderend.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }

                

                if (cnc.Status == "Y")
                {
                    DisableFlag = "('Y')";
                }
                if (cnc.Status == "N")
                {
                    DisableFlag = "('N')";
                }
                if (cnc.Status == "ALL")
                {
                    DisableFlag = "('Y','N')";
                }

                DataTable dt = SQLConnections.MyTable("Select * from MaterialMaster where status in " + DisableFlag + "" + Conditions);
                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.ID.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
