﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using context = System.Web.HttpContext;
namespace SCPAPI.Controllers.Connect
{
    public class UsersController : ApiController
    {
        public IEnumerable<con_users> GetUsers()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_users.ToList();
                return entity;
            }
        }
        public IEnumerable<con_users> GetUserByID(Int64 UserId)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_users.Where(p => p.user_id == UserId).ToList();
                return entity;
            }
        }
        public IEnumerable<con_users> GetUserByName(string UserName)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_users.Where(p => p.user_name == UserName).ToList();
                return entity;
            }
        }
        [HttpPost]
        public HttpResponseMessage GetUserLogin([FromBody] con_users bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_users.Where(p => p.user_name == bts.user_name && p.password == bts.password).ToList();
                    if (entity.Count == 0)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.Unauthorized,
                            "Invalid User");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, entity.ToList());
                    }
                    //return entity;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage Login([System.Web.Http.FromBody] con_users lgn)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString());
            SqlCommand cmd = new SqlCommand();
            string sucssmsg;
            int ret = 0;
            try
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT count(*) FROM con_users WHERE user_name ='" + lgn.user_name.Trim() + "' and password='" + lgn.password.Trim() + "'";
                cmd.Connection = con;
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
                con.Open();
                ret = Convert.ToInt32(cmd.ExecuteScalar());
                con.Close();

                if (ret == 0)
                    sucssmsg = "Invalid User.";
                else
                    sucssmsg = "Success";

                var message = Request.CreateResponse(HttpStatusCode.OK, sucssmsg);
                message.Headers.Location = new Uri(Request.RequestUri +
                    lgn.user_name.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        [HttpPost]
        public HttpResponseMessage AddUser([FromBody] con_users bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    bts.creation_date = DateTime.Now;
                    bts.last_update_date = DateTime.Now;
                    bts.temporary_password = "N";
                    entities.con_users.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.company_id.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateUsers(con_users bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_users.FirstOrDefault(e => e.user_id == bld.user_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "User Id " + bld.user_id.ToString() + " not found to update");
                    }
                    else
                    {
                        //entity.user_name = bld.user_name;
                        entity.password = bld.password;
                        entity.user_type = bld.user_type;
                        entity.contact_name = bld.contact_name;
                        entity.disabled_flag = bld.disabled_flag;
                        entity.contact_phone = bld.contact_phone;
                        entity.contact_email = bld.contact_email;
                        entity.company_id = bld.company_id;
                        entity.locale_name = bld.locale_name;
                        //entity.status = bld.status;
                        entity.password_expiration_date = bld.password_expiration_date;
                        entity.last_login_date = bld.last_login_date;
                        entity.country = bld.country;
                        entity.last_update_by = bld.last_update_by;
                        entity.last_update_date = DateTime.Now;
                        //entity.external_flag = bld.external_flag;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage DisableFlag(con_users bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_users.FirstOrDefault(e => e.user_id == bld.user_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "User Id = " + bld.user_id.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.disabled_flag = bld.disabled_flag;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddUserRoles([FromBody] UserRoles bts)
        {
            try
            {
                string[] RoleIds = bts.RoleId.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_user_roles where user_id= '" + bts.UserId + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_user_roles where user_id= '" + bts.UserId + "'");
                }

                foreach (var item in RoleIds)
                {
                    DataTable dtr = SQLConnections.MyTable("select role_name from con_roles where role_id = '" + item + "'");
                    SQLConnections.Execute("Insert into con_user_roles(role_id, user_id, role_name, created_by, creation_date, last_update_date, last_update_by) values('" + item + "', '" + bts.UserId + "', '" + dtr.Rows[0]["role_name"].ToString() + "', '" + bts.UserId + "', '" + DateTime.Now + "', '" + DateTime.Now + "', '" + bts.UserId + "')");

                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.RoleId.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        public HttpResponseMessage GetUserRoles(Int64 UserId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select * from con_user_roles	WHERE user_id = '" + UserId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    UserId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddUserVendors([FromBody] UserVendors bts)
        {
            try
            {
                string[] VendorIds = bts.VendorNumber.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_User_vendors where UserID= '" + bts.UserID + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_User_vendors where UserID= '" + bts.UserID + "'");
                }

                foreach (var item in VendorIds)
                {
                    SQLConnections.Execute("Insert into  con_User_vendors values('" + bts.UserID + "','" + item + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.UserID.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        public HttpResponseMessage GetUserVendors(Int64 UserId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select v.vendorNumber,cv.UserID,v.vendorName from con_User_vendors cv inner join VendorMaster v on cv.VendorID = v.vendorNumber WHERE CV.UserID = '" + UserId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    UserId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddUserHosts([FromBody] UserHosts bts)
        {
            try
            {
                string[] hostIds = bts.HostID.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_User_hosts where UserID= '" + bts.UserID + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_User_hosts where UserID= '" + bts.UserID + "'");
                }

                foreach (var item in hostIds)
                {
                    SQLConnections.Execute("Insert into con_User_hosts values('" + bts.UserID + "','" + item + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "')");
                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.HostID.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage GetUserHosts(Int64 UserId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select hostid,UserId from con_User_hosts WHERE UserId = '" + UserId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    UserId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
