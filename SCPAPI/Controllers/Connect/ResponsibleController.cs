﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect
{
    public class ResponsibleController : ApiController
    {
        public IEnumerable<con_responsibilities> GetResponsible()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_responsibilities.ToList();
                return entity;
            }
        }
        public IEnumerable<con_responsibilities> GetResponsibleByID(Int64 respid)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_responsibilities.Where(p => p.resp_id == respid).ToList();
                return entity;
            }
        }

        [HttpPost]
        public HttpResponseMessage AddResponsible([FromBody] con_responsibilities bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    bts.creation_date = DateTime.Now;
                    bts.last_update_date = DateTime.Now;
                    entities.con_responsibilities.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.resp_id.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateResponsible(con_responsibilities bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_responsibilities.FirstOrDefault(e => e.resp_id == bld.resp_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Responsible Id " + bld.resp_id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.resp_name = bld.resp_name;
                        entity.resp_description = bld.resp_description;
                        entity.disabled_flag = bld.disabled_flag;
                        entity.last_update_by = bld.last_update_by;
                        entity.last_update_date = DateTime.Now;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage DisableFlag(con_responsibilities bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_responsibilities.FirstOrDefault(e => e.resp_id == bld.resp_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Responsible Id = " + bld.resp_id.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.disabled_flag = bld.disabled_flag;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
