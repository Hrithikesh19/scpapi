﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect
{
    public class BusinessGroupsController : ApiController
    {
        public IEnumerable<con_BusinessGroups> GetBusinessGroups()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_BusinessGroups.ToList();
                return entity;
            }
        }
        public IEnumerable<con_BusinessGroups> GetBusinessGroupsByID(Int64 BusinessGroupID)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_BusinessGroups.Where(p => p.ID == BusinessGroupID).ToList();
                return entity;
            }
        }

        [HttpPost]
        public HttpResponseMessage AddBusinessGroups([FromBody] con_BusinessGroups bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    bts.CreatedDate = DateTime.Now;
                    bts.LastUpdatedDate = DateTime.Now;
                    entities.con_BusinessGroups.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.BusinessGroupName.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateBusinessGroups(con_BusinessGroups bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_BusinessGroups.FirstOrDefault(e => e.ID == bld.ID);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Business Group Id " + bld.ID.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.BusinessGroupName = bld.BusinessGroupName;
                        entity.Description = bld.Description;
                        entity.LastUpdatedDate = DateTime.Now;
                        entity.LastUpdatedBy = bld.LastUpdatedBy;
                        entity.DisableFlag = bld.DisableFlag;                       
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage DisableFlag(con_BusinessGroups bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_BusinessGroups.FirstOrDefault(e => e.ID == bld.ID);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Business Group Id = " + bld.ID.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.DisableFlag = bld.DisableFlag;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddBusinessGroupHosts([FromBody] BusinessGroupHosts bts)
        {
            try
            {
                string[] respIds = bts.HostID.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_BusinessGroups_Hosts where Business_GroupID= '" + bts.BusinessGroupId + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_BusinessGroups_Hosts where Business_GroupID= '" + bts.BusinessGroupId + "'");
                }

                foreach (var item in respIds)
                {                   
                    SQLConnections.Execute("Insert into con_BusinessGroups_Hosts(Business_GroupID, HostID, creation_date,created_by,last_update_by, last_update_date) values('" + bts.BusinessGroupId + "', '" + item + "', '" + DateTime.Now + "', '" + bts.createdBy + "', '" + bts.lastupdatedBy + "', '" + DateTime.Now + "')");

                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.BusinessGroupId.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        public HttpResponseMessage GetBusinessGroupHosts(Int64 BusinessGroupId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select CBH.Business_GroupID,CB.BusinessGroupName ,CBH.HostID from con_BusinessGroups_Hosts CBH inner join con_BusinessGroups CB on CBH.Business_GroupID = CB.ID WHERE CBH.Business_GroupID= '" + BusinessGroupId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    BusinessGroupId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
