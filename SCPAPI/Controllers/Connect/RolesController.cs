﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.Connect
{
    public class RolesController : ApiController
    {
        public IEnumerable<con_roles> GetAllRoles()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_roles.ToList();
                return entity;
            }
        }
        public IEnumerable<con_roles> GetRolesID(Int64 RoleId)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.con_roles.Where(p => p.role_id == RoleId).ToList();
                return entity;
            }
        }

        [HttpPost]
        public HttpResponseMessage AddRoles([FromBody] con_roles bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    bts.creation_date = DateTime.Now;
                    bts.last_update_date = DateTime.Now;
                    entities.con_roles.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.role_id.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateRoles(con_roles bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_roles.FirstOrDefault(e => e.role_id == bld.role_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Role Id " + bld.role_id.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.role_name = bld.role_name;
                        entity.role_description = bld.role_description;
                        entity.last_update_date = DateTime.Now;
                        entity.last_update_by = bld.last_update_by;
                        entity.disabled_flag = bld.disabled_flag;                       
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage DisableFlag(con_roles bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.con_roles.FirstOrDefault(e => e.role_id == bld.role_id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Role Id = " + bld.role_id.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.disabled_flag = bld.disabled_flag;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddRolesandResponsible([FromBody] RolesandResponsible bts)
        {
            try
            {
                string[] respIds = bts.respid.Split(',');
                DataTable dt = SQLConnections.MyTable("Select * from con_role_responsibilities where role_id= '" + bts.roleid + "'");
                if (dt.Rows.Count > 0)
                {
                    SQLConnections.Execute("Delete from con_role_responsibilities where role_id= '" + bts.roleid + "'");
                }

                foreach (var item in respIds)
                {                   
                    SQLConnections.Execute("Insert into con_role_responsibilities(role_id, resp_id, creation_date,created_by,last_update_by, last_update_date) values('" + bts.roleid + "', '" + item + "', '" + DateTime.Now + "', '" + bts.createdBy + "', '" + bts.lastupdatedBy + "', '" + DateTime.Now + "')");

                }
                var message = Request.CreateResponse(HttpStatusCode.Created, "Insert Successfull");
                message.Headers.Location = new Uri(Request.RequestUri +
                    bts.roleid.ToString());

                return message;
                // }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }


        public HttpResponseMessage GetRolesandResponsible(Int64 RoleId)
        {
            try
            {

                DataTable dt = SQLConnections.MyTable("Select crr.role_id,cr.resp_name,crr.resp_id from con_role_responsibilities crr inner join con_responsibilities cr on cr.resp_id = crr.resp_id WHERE crr.role_id= '" + RoleId + "'");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    RoleId.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
