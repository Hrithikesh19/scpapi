﻿using SCP.BO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.endeavour.Address
{
    

    public class AddressController : ApiController
    {
        string Conditions = string.Empty;      
        string statusflag;
        DateTime lastupdatestart, lastupdateend;
        DataTable dt;       
        public IEnumerable<end_Address> GetAddresses()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.end_Address.ToList();
                return entity;
            }
        }
        public IEnumerable<end_Address> GetAddressByVendor(string Vendor)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.end_Address.Where(p => p.vendorName == Vendor).ToList();
                return entity;
            }
        }
       
        
        [HttpPost]
        public HttpResponseMessage AddAddress([FromBody] end_Address bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {                   
                    bts.createddate = DateTime.Now;                    
                    bts.lastupdatedate = DateTime.Now;
                    bts.status = "N";
                    entities.end_Address.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.addressId.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateAddress(end_Address bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.end_Address.FirstOrDefault(e => e.addressId == bld.addressId);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Address Id " + bld.addressId.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.vendorName = bld.vendorName;
                        entity.countrycode = bld.countrycode;
                        entity.addressname = bld.addressname;
                        entity.purchaseorg = bld.purchaseorg;
                        entity.countryname = bld.countryname;
                        entity.className = bld.className;
                        entity.plant = bld.plant;
                        entity.addresstype = bld.addresstype;
                        entity.addressline1 = bld.addressline1;
                        entity.addressline2 = bld.addressline2;
                        entity.addressline3 = bld.addressline3;
                        entity.addressline4 = bld.addressline4;
                        entity.addressline5 = bld.addressline5;
                        entity.lastupdatedby = bld.lastupdatedby;
                        entity.lastupdatedate = DateTime.Now;                      
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage DisableFlag(end_Address bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.end_Address.FirstOrDefault(e => e.addressId == bld.addressId);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Address Id = " + bld.addressId.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.status = bld.status;                       
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }        

        [HttpPost]
        public HttpResponseMessage GetAllSearchAddress([FromBody] endavourAddress cnc)
        {
            try
            {

                if (cnc.lastupdatedate.ToString() != string.Empty)
                {
                    lastupdatestart = Convert.ToDateTime(cnc.lastupdatedate).AddDays(30);
                }
                if (cnc.lastupdatedateend.ToString() != string.Empty)
                {
                    lastupdateend = Convert.ToDateTime(cnc.lastupdatedateend).AddDays(-30);
                }


                if (cnc.vendorName != string.Empty)
                {
                    Conditions += " and vendorName='" + cnc.vendorName + "'";
                }

                if (cnc.purchaseorg != string.Empty)
                {
                    Conditions += " and purchaseorg='" + cnc.purchaseorg + "'";
                }
                if (cnc.plant != string.Empty)
                {
                    Conditions += " and plant='" + cnc.plant + "'";
                }
                if (cnc.addressname != string.Empty)
                {
                    Conditions += " and addressname='" + cnc.addressname + "'";

                }
                if (cnc.lastupdatedate.ToString() != string.Empty && cnc.lastupdatedateend.ToString() != string.Empty)
                {
                    Conditions += " and lastupdatedate between '" + cnc.lastupdatedate + "' and '" + cnc.lastupdatedateend + "'";
                }
                else
                if (cnc.lastupdatedate.ToString() != string.Empty)
                {
                    Conditions += " and lastupdatedate >= '" + lastupdatestart.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }
                else if (cnc.lastupdatedateend.ToString() != string.Empty)
                {
                    Conditions += " and lastupdatedate <= '" + lastupdateend.ToString("yyyy-MM-dd HH:mm:ss") + "'";
                }

                //if (cnc.COMPANY_PRINT_TO_PDF != string.Empty)
                //{
                //    Conditions += " and COMPANY_PRINT_TO_PDF='" + cnc.COMPANY_PRINT_TO_PDF + "'";
                //}

                if (cnc.status == "Y")
                {
                    statusflag = "('Y')";
                }
                if (cnc.status == "N")
                {
                    statusflag = "('N')";
                }
                if (cnc.status == "ALL")
                {
                    statusflag = "('Y','N')";
                }

                DataTable dt = SQLConnections.MyTable("Select * from [end.Address] where status in " + statusflag + "" + Conditions);
                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    cnc.vendorName.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
    }
}
