﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using SCP.BAL;
using SCP.BO;
using System.Data;

namespace SCPAPI.Controllers.endeavour
{

    public class ScheduleController : ApiController
    {       
        ScheduleBAL schBAL = null;
        DataTable dt;

       
        [HttpPost]
        public List<ScheduleData> GetScheduleData(PoOrderRequest request)
        {
            schBAL = ScheduleBAL.Instance;
            return schBAL.GetScheduleData(request);                       
        }

        [HttpPost]
        public List<SchedulesSummary> GetScheduleSummary(PoOrderRequest request)
        {
            schBAL = ScheduleBAL.Instance;
            return schBAL.GetScheduleSummary(request);
        }

        [HttpPost]
        public List<ScheduleDeliverDates> GetScheduleDeliveryDates(PoOrderRequest request)
        {
            schBAL = ScheduleBAL.Instance;
            return schBAL.GetScheduleDeliveryDates(request);
        }

        public IEnumerable<ScheduleLine> GetSchedules()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.ScheduleLines.ToList();
                return entity;
            }
        }


        public IEnumerable<ScheduleLine> GetScheduleByNumber(Int64 scheduleNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.ScheduleLines.Where(p => p.scheduleNumber == scheduleNumber).ToList();
                return entity;
            }
        }

        public IEnumerable<package> GetPackagesByScheduleNumber(string scheduleNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.packages.Where(p => p.ERP_PO_NUMBER == scheduleNumber).ToList();
                return entity;
            }
        }

       

        //[HttpPost]
        public HttpResponseMessage GetScheduleMasters(string category)
        {
            try
            {
                if (category == "ScheduleNumber")
                    dt = SQLConnections.MyTable("Select distinct scheduleNumber from ScheduleLine");
                if (category == "ReleaseType")
                    dt = SQLConnections.MyTable("Select distinct releaseType from ScheduleLine");               
                if (category == "VendorNumber")
                    dt = SQLConnections.MyTable("select distinct vendorNumber,vendorName from ScheduleLine order by vendorNumber"); 
                if (category == "ItemNumber")
                    dt = SQLConnections.MyTable("select distinct itemNumber from ScheduleLine");                
                var message = Request.CreateResponse(HttpStatusCode.OK, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
