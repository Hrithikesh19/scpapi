﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using SCP.BAL;
using SCP.BO;
using System.Data;

namespace SCPAPI.Controllers.endeavour
{

    public class PurchaseOrderController : ApiController
    {
        PoBAL poBAL = null;
        PurchaseOrderBAL PurchaseBAL = null;
        DataTable dt;

        //public List<PurchaseOrderData> GetPurchaseOrderData()
        //{
        //    PurchaseBAL = PurchaseOrderBAL.Instance;
        //    return PurchaseBAL.GetPurchaseOrderData(0);
        //}

        [HttpPost]
        public List<PurchaseOrderData> GetPurchaseOrderData(PoOrderRequest request)
        {
            PurchaseBAL = PurchaseOrderBAL.Instance;
            return PurchaseBAL.GetPurchaseOrderData(request);                       
        }

        public IEnumerable<PurchaseOrderLine> GetPurchaseOrders()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.PurchaseOrderLines.ToList();
                return entity;
            }
        }


        public IEnumerable<PurchaseOrderLine> GetPurchaseOrderByPoNumber(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.PurchaseOrderLines.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }

        public PurchaseOrderDetails GetPoOrderDetails(Int64 poNumber)
        {
            poBAL = PoBAL.Instance;
            return poBAL.GetPurchaseorderDetails(poNumber);
        }

        public IEnumerable<po_OrderStatus> GetPoOrderStatus(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.po_OrderStatus.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }

        public IEnumerable<po_ShippingDetails> GetPoShippingDetails(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.po_ShippingDetails.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }
        public IEnumerable<po_BillingDetails> GetPoBillingDetails(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.po_BillingDetails.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }

        public IEnumerable<po_Notes> GetPoNotes(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.po_Notes.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }

        [HttpPost]
        public HttpResponseMessage AddPoNotes([FromBody] po_Notes bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {

                    entities.po_Notes.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.poNumber.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpPut]
        public HttpResponseMessage UpdatePoNotes(po_Notes bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.po_Notes.FirstOrDefault(e => e.poNumber == bld.poNumber);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "PO Number = " + bld.poNumber.ToString() + " not found to Update");
                    }
                    else
                    {
                        entity.noteStatus = bld.noteStatus;
                        entity.notesToSupplier = bld.notesToSupplier;
                        entity.Reason = bld.Reason;
                        entity.toUser = bld.toUser;
                        entity.contents = bld.contents;
                        entity.attachments = bld.attachments;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        public IEnumerable<po_OrderHistory> GetPoOrderHistory(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.po_OrderHistory.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }

        public IEnumerable<po_CollabarationHistory> GetCollabarationHistory(Int64 ponumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.po_CollabarationHistory.Where(p => p.poNumber == ponumber).ToList();
                return entity;
            }
        }

        //[HttpPost]
        public HttpResponseMessage GetPurchaseOrderMasters(string category)
        {
            try
            {
                if (category == "ItemCategory")
                    dt = SQLConnections.MyTable("Select distinct itemCategory from PurchaseOrderLine where itemCategory <> ''");
                if (category == "PONumber")
                    dt = SQLConnections.MyTable("Select distinct top 100 CAST(PONumber as varchar(10)) as PONumber,vendorName,buyerName from PurchaseOrderLine");
                if (category == "Status")
                    dt = SQLConnections.MyTable("Select distinct status from PurchaseOrderLine");
                if (category == "VendorNumber")
                    dt = SQLConnections.MyTable("select distinct vendorNumber,vendorName from PurchaseOrderLine order by vendorNumber");
                if (category == "AccountAssignment")
                    dt = SQLConnections.MyTable("select distinct accountAssignment,accountAssignmentDesc from PurchaseOrderLine where accountAssignment <> '' order by accountAssignment");
                if (category == "FAIBlock")
                    dt = SQLConnections.MyTable("select distinct faiBlock from PurchaseOrderLine");
                if (category == "BOMReviewed")
                    dt = SQLConnections.MyTable("select distinct bomReviewed from PurchaseOrderLine where bomReviewed <> ''");
                if (category == "POStatus")
                    dt = SQLConnections.MyTable("select distinct erpPoStatusCode from PurchaseOrderLine");
                if (category == "PurchasingOrgNumber")
                    dt = SQLConnections.MyTable("select distinct purchasingOrgNum,purchasingOrgDesc from PurchaseOrderLine");
                if (category == "ItemNumber")
                    dt = SQLConnections.MyTable("select distinct itemNumber from PurchaseOrderLine");
                if (category == "MRPRecommendation")
                    dt = SQLConnections.MyTable("select distinct mrpAction from PurchaseOrderLine where mrpAction is not null");
                if (category == "BOMChanged")
                    dt = SQLConnections.MyTable("select distinct bomChanged from PurchaseOrderLine");
                if (category == "Plant")
                    dt = SQLConnections.MyTable("select distinct poLineLocationId from PurchaseOrderLine");
                if (category == "RMAStatus")
                    dt = SQLConnections.MyTable("select distinct rmaStatus from PurchaseOrderLine where rmaStatus is not null");
                //dt = SQLConnections.MyTable("Select * from con_companies where disabled_flag in " + DisableFlag + "" + Conditions);
                var message = Request.CreateResponse(HttpStatusCode.OK, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
