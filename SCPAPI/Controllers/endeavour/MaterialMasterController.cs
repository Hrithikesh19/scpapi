﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using SCP.BO;
using SCP.BAL;
using System.Data;

namespace SCPAPI.Controllers.endeavour
{
    public class MaterialMasterController : ApiController
    {
        ShipmentsBAL shpBAL = null;
        DataTable dt;
        public IEnumerable<MaterialMaster> GetMaterials()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.MaterialMasters.ToList();
                return entity;
            }
        }
        public IEnumerable<MaterialMaster> GetMaterialByNumber(string MaterialNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.MaterialMasters.Where(p => p.VendorMaterialNumber == MaterialNumber).ToList();
                return entity;
            }
        }

       

        [HttpPost]
        public HttpResponseMessage AddMaterials([FromBody] MaterialMaster bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {


                    bts.CreatedDate = DateTime.Now;
                    bts.LasteUpdateDate = DateTime.Now;

                    entities.MaterialMasters.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.ID.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateMaterials(MaterialMaster bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.MaterialMasters.FirstOrDefault(e => e.ID == bld.ID);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Material ID " + bld.ID.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.VendorNumber = bld.VendorNumber;
                        entity.VendorName = bld.VendorName;
                        entity.AMATMaterialNumber = bld.AMATMaterialNumber;
                        entity.VendorMaterialNumber = bld.VendorMaterialNumber;
                        entity.CaseQuantity = bld.CaseQuantity;
                        entity.NetWeight = bld.NetWeight;
                        entity.CountryofOrginCode = bld.CountryofOrginCode;
                        entity.CountryName = bld.CountryName;
                        entity.CountryofOrginMark = bld.CountryofOrginMark;
                        entity.Status = "Y";                        
                        entity.LastUpdatedBy = bld.LastUpdatedBy;
                        entity.LasteUpdateDate =DateTime.Now;
                        
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpPost]
        public HttpResponseMessage MaterialMasters(string category)
        {
            try
            {
                if (category == "VendorNumber")
                    dt = SQLConnections.MyTable("Select distinct VendorNumber from MaterialMaster where VendorNumber is not null");                
                if (category == "VendorName")
                    dt = SQLConnections.MyTable("select distinct VendorName from MaterialMaster where VendorName is not null");
                if (category == "AMATMaterialNumber")
                    dt = SQLConnections.MyTable("select distinct AMATMaterialNumber from MaterialMaster where AMATMaterialNumber is not null");
                if (category == "CountryofOrginCode")
                    dt = SQLConnections.MyTable("Select distinct CountryofOrginCode from MaterialMaster where CountryofOrginCode is not null");                              

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        

        [HttpPut]
        public HttpResponseMessage UpdateMaterialStatus([FromBody] MaterialMaster bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.MaterialMasters.FirstOrDefault(e => e.ID == bld.ID);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Material Number " + bld.ID.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.Status = bld.Status;                                                                     
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }  
        

    }
}
