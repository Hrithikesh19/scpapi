﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
namespace SCPAPI.Controllers.endeavour
{
    public class mrpController : ApiController
    {       
        public IEnumerable<PurchaseOrderLine> GetMRPSuggestion()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.PurchaseOrderLines.Where(c => c.mrpAction != null).ToList();
                return entity;
            }
        }

        public IEnumerable<PurchaseOrderLine> GetMRPSuggestionBYPoNumber(Int64 poNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.PurchaseOrderLines.Where(c => c.mrpAction != null && c.poNumber == poNumber).ToList();
                return entity;
            }
        }
    }
}
