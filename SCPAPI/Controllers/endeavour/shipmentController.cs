﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using SCP.BO;
using SCP.BAL;
using System.Data;

namespace SCPAPI.Controllers.endeavour
{
    public class shipmentController : ApiController
    {
        ShipmentsBAL shpBAL = null;
        DataTable dt;
        public IEnumerable<Shipment> Getshipments()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.Shipments.ToList();
                return entity;
            }
        }
        public IEnumerable<Shipment> GetshipmentsByNumber(int shipmentid)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.Shipments.Where(p => p.SHIPMENT_ID == shipmentid).ToList();
                return entity;
            }
        }


        [HttpPost]
        public List<ShipmentsData> GetShipmentsData(PoOrderRequest request)
        {
            shpBAL = ShipmentsBAL.Instance;
            return shpBAL.GetShipments(request);
        }

        //public List<ShipmentsData> GetShipmentsData()
        //{
        //    shpBAL = ShipmentsBAL.Instance;
        //    return shpBAL.GetShipments(0);
        //}
        public List<ShipmentsData> GetShipmentsByScheduleNumber(Int64 shipmentnumber)
        {
            shpBAL = ShipmentsBAL.Instance;
            return shpBAL.GetShipmentByShipmentNumber(shipmentnumber);
        }

        [HttpPost]
        public HttpResponseMessage AddShipments([FromBody] Shipment bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {


                    bts.CREATION_DATE = DateTime.Now;
                    bts.LAST_UPDATE_DATE = DateTime.Now;

                    entities.Shipments.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.SHIPMENT_ID.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateShipments(Shipment bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.Shipments.FirstOrDefault(e => e.SHIPMENT_ID == bld.SHIPMENT_ID);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Shipment ID " + bld.SHIPMENT_ID.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.SHIPMENT_NUMBER = bld.SHIPMENT_NUMBER;
                        entity.ERP_FROM_ADDRESS1 = bld.ERP_FROM_ADDRESS1;
                        entity.STATUS = bld.STATUS;
                        entity.ERP_TO_ADDRESS3 = bld.ERP_TO_ADDRESS3;
                        entity.PACKING_SLIP = bld.PACKING_SLIP;
                        entity.REFERENCE = bld.REFERENCE;
                        entity.WAYBILL = bld.WAYBILL;
                        entity.NOTES = bld.NOTES;
                        entity.FREIGHT_TERMS = bld.FREIGHT_TERMS;


                        entity.NUM_OF_PACKAGES = bld.NUM_OF_PACKAGES;
                        entity.UNIT_OF_WEIGHT = bld.UNIT_OF_WEIGHT;
                        entity.NET_WEIGHT = bld.NET_WEIGHT;
                        entity.FLIGHT_NUMBER = bld.FLIGHT_NUMBER;
                        entity.GROSS_WEIGHT = bld.GROSS_WEIGHT;
                        entity.FREIGHT_BILL_NUM = bld.FREIGHT_BILL_NUM;
                        entity.DELIVERY_ID = bld.DELIVERY_ID;
                        entity.SHIPPING_CHARGES = bld.SHIPPING_CHARGES;
                        entity.FREIGHT_CHARGES = bld.FREIGHT_CHARGES;
                        entity.PICKUP_DATE = bld.PICKUP_DATE;
                        entity.EXPECTED_RECEIPT_DATE = bld.EXPECTED_RECEIPT_DATE;
                        entity.CERTIFICATION = bld.CERTIFICATION;
                        entity.LAST_UPDATE_BY = bld.LAST_UPDATE_BY;
                        entity.LAST_UPDATE_DATE = bld.LAST_UPDATE_DATE;

                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        [HttpPost]
        public HttpResponseMessage ShipmentsMasters(string category)
        {
            try
            {
                if (category == "PackageNumber")
                    dt = SQLConnections.MyTable("Select distinct PACKAGE_NUMBER from packages");
                if (category == "PONumber")
                    dt = SQLConnections.MyTable("Select distinct top 100 CAST(ERP_PO_NUMBER as varchar(10)) as PONumber,ERP_VENDOR_NAME,ERP_BUYER_NAME from packages");
                if (category == "CarrierName")
                    dt = SQLConnections.MyTable("select distinct CARRIER_NAME,CARRIER_NAME from Shipments where CARRIER_NAME is not null");
                //if (category == "PTNStatus")
                //    dt = SQLConnections.MyTable("select distinct CARRIER_NAME,CARRIER_NAME from Shipments where CARRIER_NAME is not null");
                if (category == "ShipmentStatus")
                    dt = SQLConnections.MyTable("Select distinct status from Shipments");               
                if (category == "MaterialNumber")
                    dt = SQLConnections.MyTable("select distinct ERP_ITEM_NUMBER  from packages where ERP_ITEM_NUMBER<>''");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddShipmentNotes([FromBody] Shipment_Notes bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {

                    
                    
                    entities.Shipment_Notes.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.shipmentNumber.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdateShipmentNotes([FromBody] Shipment_Notes bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.Shipment_Notes.FirstOrDefault(e => e.shipmentNumber == bld.shipmentNumber);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Shipment Number " + bld.shipmentNumber.ToString() + " not found to update");
                    }
                    else
                    {
                        entity.noteStatus = bld.noteStatus;
                        entity.Reason = bld.Reason;
                        entity.toUser = bld.toUser;
                        entity.Email = bld.Email;
                        entity.contents = bld.contents;
                        entity.attachments = bld.attachments;                                                
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }
        public IEnumerable<Shipment_Notes> GetShipmentNotes(Int64 shipmentnumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.Shipment_Notes.Where(p => p.shipmentNumber == shipmentnumber).ToList();
                return entity;
            }
        }
    }
}
