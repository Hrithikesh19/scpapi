﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using System.Data;
using SCP.BAL;
using SCP.BO;

namespace SCPAPI.Controllers.endeavour
{

    public class PackageController : ApiController
    {
        PackagesBAL pkgBAL = null;
        DataTable dt;
        public IEnumerable<package> GetPackages()
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.packages.ToList();
                return entity;
            }
        }

        //public List<PackagesData> GetPackagesData()
        //{
        //    pkgBAL = PackagesBAL.Instance;
        //    return pkgBAL.GetPackages("0");
        //}
        //public List<PackagesData> GetPackagesData(string pkgNumber)
        //{
        //    pkgBAL = PackagesBAL.Instance;
        //    return pkgBAL.GetPackages(pkgNumber);
        //}

        [HttpPost]
        public List<PackagesData> GetPackagesData(PoOrderRequest request)
        {
            pkgBAL = PackagesBAL.Instance;
            return pkgBAL.GetPackages(request);
        }

        public IEnumerable<package> GetPackagesByPkgNumber(string pkgNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.packages.Where(p => p.PACKAGE_NUMBER == pkgNumber).ToList();
                return entity;
            }
        }
        public IEnumerable<PackageHistory> GetPackageHistory(Int64 pkgNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.PackageHistories.Where(p => p.pkgNumber == pkgNumber).ToList();
                return entity;
            }
        }


        [HttpPost]
        public HttpResponseMessage PackageMasters(string category)
        {
            try
            {
                if (category == "PackageNumber")
                    dt = SQLConnections.MyTable("Select distinct PACKAGE_NUMBER from packages");
                if (category == "PONumber")
                    dt = SQLConnections.MyTable("Select distinct top 100 CAST(ERP_PO_NUMBER as varchar(10)) as PONumber,ERP_VENDOR_NAME,ERP_BUYER_NAME from packages");
                if (category == "PTNStatus")
                    dt = SQLConnections.MyTable("Select distinct status from packages");
                if (category == "CarrierName")
                    dt = SQLConnections.MyTable("select distinct CARRIER_NAME,CARRIER_NAME from Shipments where CARRIER_NAME is not null");
                if (category == "ShipmentStatus")
                    dt = SQLConnections.MyTable("Select distinct status from Shipments");
                if (category == "Supplier")
                    dt = SQLConnections.MyTable("select distinct vendorName,vendorNumber from PurchaseOrderLine");
                if (category == "ScheduleNumber")
                    dt = SQLConnections.MyTable("select distinct ERP_SCHEDULE_NUMBER from packages where ERP_SCHEDULE_NUMBER <>''");
                if (category == "MaterialNumber")
                    dt = SQLConnections.MyTable("select distinct ERP_ITEM_NUMBER  from packages where ERP_ITEM_NUMBER<>''");
                if (category == "KanbanCard")
                    dt = SQLConnections.MyTable("select distinct ERP_KANBAN_CARD  from packages where ERP_KANBAN_CARD<>''");
                if (category == "PromiseNumber")
                    dt = SQLConnections.MyTable("select distinct SCH_PROMISE_NUMBER  from packages where SCH_PROMISE_NUMBER<>''");

                var message = Request.CreateResponse(HttpStatusCode.Created, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPost]
        public HttpResponseMessage AddPackageVariants([FromBody] packagevariation bts)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {

                    bts.Date = DateTime.Now;
                    entities.packagevariations.Add(bts);
                    entities.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, bts);
                    message.Headers.Location = new Uri(Request.RequestUri +
                        bts.pkgNumber.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        [HttpPut]
        public HttpResponseMessage UpdatePackageVariants(packagevariation bld)
        {
            try
            {
                using (SCPDBEntities entities = new SCPDBEntities())
                {
                    var entity = entities.packagevariations.FirstOrDefault(e => e.pkgNumber == bld.pkgNumber);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound,
                            "Package Number = " + bld.pkgNumber.ToString() + " not found to Disable");
                    }
                    else
                    {
                        entity.varianceReason = bld.varianceReason;
                        entity.status = bld.status;
                        entity.description = bld.description;
                        entity.resoultion = bld.resoultion;
                        entity.closedby = bld.closedby;
                        entities.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public IEnumerable<packagevariation> GetPackageVariation(Int64 pkgNumber)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.packagevariations.Where(p => p.pkgNumber == pkgNumber).ToList();
                return entity;
            }
        }
    }
}
