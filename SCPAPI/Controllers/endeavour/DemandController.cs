﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserDataAccess;
using SCP.BAL;
using SCP.BO;
using System.Data;

namespace SCPAPI.Controllers.endeavour
{

    public class DemandController : ApiController
    {       
        DemandBAL schBAL = null;
        DataTable dt;

       
        [HttpPost]
        public List<DemandPurchaseWorkBenchData> GetDemandPurchaseWorkBenchData(PoOrderRequest request)
        {
            schBAL = DemandBAL.Instance;
            return schBAL.GetDemandPurchaseWorkBenchData(request);                       
        }

        [HttpPost]
        public List<DemandItemAttributes> GetDemandItemAttributes(PoOrderRequest request)
        {
            schBAL = DemandBAL.Instance;
            return schBAL.GetDemandItemAttributes(request);
        }

        public IEnumerable<DemandDailyHistory> GetDemandDailyHistory(Int64 Plant)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.DemandDailyHistories.Where(p => p.Plant == Plant).ToList();
                return entity;
            }
        }

        public IEnumerable<DemandWeeklyHistory> GetDemandWeeklyHistory(Int64 Plant)
        {
            using (SCPDBEntities entities = new SCPDBEntities())
            {
                var entity = entities.DemandWeeklyHistories.Where(p => p.Plant == Plant).ToList();
                return entity;
            }
        }
        [HttpPost]
        public DemandDetails GetDemandSearchForecast(PoOrderRequest request)
        {
            schBAL = DemandBAL.Instance;
            return schBAL.GetDemandSearchForecast(request);
        }
        public DemandDetails GetDemandDetails(string itemNumber)
        {
            schBAL = DemandBAL.Instance;
            return schBAL.GetDemandDetails(itemNumber);
        }
        //[HttpPost]
        public HttpResponseMessage GetDemandMasters(string category)
        {
            try
            {
                if (category == "SupplierName")
                    dt = SQLConnections.MyTable("Select distinct supplierName,exloc8 from DemandPurchasingWorkbench");
                if (category == "ItemNumber")
                    dt = SQLConnections.MyTable("Select distinct itemNumber,itemDescription from DemandItemAttributes");
                if (category == "VendorNumber")
                    dt = SQLConnections.MyTable("select distinct VendorNumber from demandsupplier order by vendorNumber");
                if (category == "PlantName")
                    dt = SQLConnections.MyTable("select distinct PlantName from demandplant");
                if (category == "CompanyName")
                    dt = SQLConnections.MyTable("select distinct CompanyName from demandcompany");
                if (category == "MrpController")
                    dt = SQLConnections.MyTable("select distinct mrpController from DemandItem");
                if (category == "ItemType")
                    dt = SQLConnections.MyTable("select distinct itemType from DemandItem");
                if (category == "SupplierNumber")
                    dt = SQLConnections.MyTable("select distinct supplierNumber from DemandItem");
                if (category == "PurchasingGroup")
                    dt = SQLConnections.MyTable("select distinct purchasingGroup from DemandItem");
                var message = Request.CreateResponse(HttpStatusCode.OK, dt);
                message.Headers.Location = new Uri(Request.RequestUri +
                    category.ToString());

                return message;
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

    }
}
