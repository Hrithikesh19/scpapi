﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Data;
#region " Classes"

public class SQLConnections
{
    public static void Execute(String Query)
    {
        int result = 0;

        using (SqlConnection mySqlCon = new SqlConnection(Config.SQLcon))
        {

            SqlCommand myCmd = new SqlCommand(Query, mySqlCon);
            myCmd.Connection.Open();
            result = myCmd.ExecuteNonQuery();
            myCmd.Connection.Close();
            Query = "";
        }

    }
    public static void Execute(String Query, String Conn)
    {
        int result = 0;

        using (SqlConnection mySqlCon = new SqlConnection(Conn))
        {

            SqlCommand myCmd = new SqlCommand(Query, mySqlCon);
            myCmd.Connection.Open();
            result = myCmd.ExecuteNonQuery();
            myCmd.Connection.Close();
            Query = "";
        }

    }
    public static DataTable MyTable(String Query)
    {
        DataTable MyDT = new DataTable();
        using (SqlConnection myConnection = new SqlConnection(Config.SQLcon))
        {
            SqlDataAdapter myDA = new SqlDataAdapter(Query, myConnection);
            myDA.Fill(MyDT);
        }

        return MyDT;

    }
    public static DataTable MyTable(String Query, String Conn)
    {
        DataTable MyDT = new DataTable();
        using (SqlConnection myConnection = new SqlConnection(Conn))
        {
            SqlDataAdapter myDA = new SqlDataAdapter(Query, myConnection);
            myDA.Fill(MyDT);
        }

        return MyDT;

    }
    public static DataTable TableData(String Query, String Conn)
    {
        DataTable MyDT = new DataTable();
        using (SqlConnection myConnection = new SqlConnection(Conn))
        {
            SqlCommand cmd = new SqlCommand(Query, myConnection);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            cmd.CommandTimeout = 0;
            da.Fill(MyDT);
        }             
        return MyDT;

    }
}

#endregion

