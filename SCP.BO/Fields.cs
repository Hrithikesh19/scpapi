﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCP.BO
{
    public class Fields
    {

        public int folder_id { get; set; }
        public int field_namespace_id { get; set; }
        public string field_name { get; set; }
        public string description { get; set; }
        public string controltype { get; set; }
        public string field_type { get; set; }

    }

    public class FolderFields
    {

        public int field_id { get; set; }
        public int field_namespace_id { get; set; }
        public string displayText { get; set; }        

    }

    public class FolderDetails
    {

        public List<FolderFields> AvailableFields { get; set; }
        public List<FolderFields> SelectedFields { get; set; }

        public List<FolderFields> OrderByFields { get; set; }


    }

}
