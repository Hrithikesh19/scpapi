﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCP.BO
{
    public class Companies
    {

        public long company_id { get; set; }
        public string company_name { get; set; }
        public string company_code { get; set; }
        public Nullable<int> company_type_id { get; set; }
        public Nullable<int> approved { get; set; }
        public string addr1 { get; set; }
        public string addr2 { get; set; }
        public string addr3 { get; set; }
        public string addr4 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string attribute1 { get; set; }
        public string attribute2 { get; set; }
        public string attribute3 { get; set; }
        public string attribute4 { get; set; }
        public string attribute5 { get; set; }
        public string attribute6 { get; set; }
        public string attribute7 { get; set; }
        public string attribute8 { get; set; }
        public string attribute9 { get; set; }
        public string attribute10 { get; set; }
        public string disabled_flag { get; set; }
        public Nullable<System.DateTime> enable_date { get; set; }
        public Nullable<System.DateTime> disable_date { get; set; }
        public string all_vendors { get; set; }
        public string min_order_date { get; set; }
        public string min_order_date_end { get; set; }
        public int created_by { get; set; }
        public System.DateTime creation_date { get; set; }
        public int last_update_by { get; set; }
        public System.DateTime last_update_date { get; set; }
        public string ALL_VENDOR_LOCATIONS { get; set; }
        public string QUAL_CONTROL { get; set; }
        public string XPC_COMPANY { get; set; }
        public string SNG_COMPANY { get; set; }
        public string XC_ENABLED { get; set; }
        public string COMPANY_PRINT_TO_PDF { get; set; }
        public string VIEW_ECO_DOCUMENTS { get; set; }
        public string COMPANY_EMAIL { get; set; }
        public string ALLOW_ADV_PACK { get; set; }

    }

    public class Users
    {
        public long user_id { get; set; }
        public string user_name { get; set; }
        public string Company_Name { get; set; }
        public string roleid { get; set; }
        public string user_type { get; set; }
        public string password { get; set; }
        public string external_flag { get; set; }
        public Nullable<int> company_id { get; set; }
        public string contact_name { get; set; }
        public string contact_email { get; set; }
        public string contact_phone { get; set; }
        public string attribute1 { get; set; }
        public string attribute2 { get; set; }
        public string attribute3 { get; set; }
        public string attribute4 { get; set; }
        public string attribute5 { get; set; }
        public string attribute6 { get; set; }
        public string attribute7 { get; set; }
        public string attribute8 { get; set; }
        public string attribute9 { get; set; }
        public string attribute10 { get; set; }
        public string locale_name { get; set; }
        public string disabled_flag { get; set; }
        public Nullable<System.DateTime> enable_date { get; set; }
        public Nullable<System.DateTime> disable_date { get; set; }
        public string lostloginstartdays { get; set; }
        public string lostloginenddays { get; set; }
        public DateTime last_login_date { get; set; }
        public string password_expiration_date { get; set; }
        public string status { get; set; }
        public Nullable<int> login_attempts { get; set; }
        public string temporary_password { get; set; }
        public string country { get; set; }
        public int created_by { get; set; }
        public System.DateTime creation_date { get; set; }
        public int last_update_by { get; set; }
        public System.DateTime last_update_date { get; set; }
    }

    public class folders
    {
        public long folder_id { get; set; }
        public string folder_name { get; set; }
        public string page_id { get; set; }
        public string inactive_date { get; set; }
        public string application_code { get; set; }
        public string user_id { get; set; }
        public string company_id { get; set; }
        public string user_created { get; set; }
        public string created_by { get; set; }
        public DateTime creation_date { get; set; }
        public string last_update_by { get; set; }
        public string last_update_date { get; set; }
        public string last_update_date_end { get; set; }
    }

    public class Lookups
    {
        public long lookup_id { get; set; }
        public string category1 { get; set; }
        public string category2 { get; set; }
        public string category3 { get; set; }
        public string category4 { get; set; }
        public string category5 { get; set; }
        public string value { get; set; }
        public string disabled_flag { get; set; }
        public Nullable<System.DateTime> enable_date { get; set; }
        public Nullable<System.DateTime> disable_date { get; set; }
        public string description { get; set; }
        public string creation_date { get; set; }
        public string creation_date_end { get; set; }
        public int created_by { get; set; }
        public int last_update_by { get; set; }
        public System.DateTime last_update_date { get; set; }
    }

    public class PoOrderFields
    {
        public string itemNumer { get; set; }
        public string itemText { get; set; }
        public string itemRevision { get; set; }
        public string unitPrice { get; set; }
        public string extendedprice { get; set; }
        public string currency { get; set; }
        public string quantity { get; set; }
        public string uom { get; set; }
        public string faiblock { get; set; }
        public string qtyopen { get; set; }
        public string qtyReceived { get; set; }
        public string releaseUntilldate { get; set; }
        public string plantNumber { get; set; }




        public long poNumber { get; set; }
        public string mrpRecommondation { get; set; }
        public string mrpSuggestedDate { get; set; }
        public string mrpQty { get; set; }
        public string needByDate { get; set; }
        public string collabarationEarly { get; set; }
        public string collabarationLate { get; set; }
        public string resheduleInEarly { get; set; }
        public string resheduleInLate { get; set; }
        public string resheduleOutEarly { get; set; }
        public string resheduleOutLate { get; set; }
        public string printable { get; set; }
        public string currentDeliverDate { get; set; }
        public string currentUnitPrice { get; set; }
        public string requestDeliveryDate { get; set; }
        public string requestUnitPrice { get; set; }
        public string NewDeliveryDate { get; set; }
        public string NewUnitPrice { get; set; }
        public string openQty { get; set; }
        public string RejectReason { get; set; }
        public string RMAStatus { get; set; }
        public string MRPReason { get; set; }
        public string Comments { get; set; }
        public string PreviousCpmments { get; set; }
        public string SystemComments { get; set; }


        //public long poNumber { get; set; }
        public string poType { get; set; }
        public string poStatus { get; set; }
        public string Line { get; set; }
        public string orderDate { get; set; }
        public string teaNumber { get; set; }
        public string release { get; set; }
        public string shipmentNumer { get; set; }


        //public long poNumber { get; set; }
        public string lineNumber { get; set; }
        public string poShipmentNumber { get; set; }
        //public string poType { get; set; }
        public string MaterialNumber { get; set; }
        public string Status { get; set; }
        public string DeliveryDate { get; set; }
        public string Needbydate { get; set; }
        public string Statdeldate { get; set; }
        public string trackmypart { get; set; }

    }
    public class PurchaseOrderDetails
    {

        public List<PoOrderFields> OrderDetails { get; set; }
        public List<PoOrderFields> ItemDetails { get; set; }
        public List<PoOrderFields> AdditionalDetails { get; set; }
        public List<PoOrderFields> Collabration { get; set; }

    }
    public class DemandDetails
    {

        public List<DemandFields> DemandItems { get; set; }
        public List<DemandFields> DemandCompany { get; set; }
        public List<DemandFields> DemandSupplier { get; set; }
        public List<DemandFields> DemandPlant { get; set; }

    }

    public class DemandFields
    {
        public string itemNumber { get; set; }
        public string itemType { get; set; }
        public string mrpController { get; set; }
        public string pace { get; set; }
        public string parentSupplierCompany { get; set; }
        public string plantName { get; set; }
        public string purchasingGroup { get; set; }
        public string supplierNumber { get; set; }
       

        public string CompanyName { get; set; }
        public string LMD { get; set; }
        public string TotalRequirements { get; set; }
        public string PastDueRequirements { get; set; }
        public string Measure { get; set; }
        public string W1 { get; set; }
        public string W2 { get; set; }
        public string W3 { get; set; }
        public string W4 { get; set; }
        public string W5 { get; set; }
        public string W6 { get; set; }
        public string W7 { get; set; }
        public string W8 { get; set; }
        public string W9 { get; set; }
        public string W10 { get; set; }
        public string W11 { get; set; }
        public string W12 { get; set; }
        public string W13 { get; set; }
        public string W14 { get; set; }
        public string W15 { get; set; }
        public string W16 { get; set; }
        public string W17 { get; set; }
        public string W18 { get; set; }
        public string W19 { get; set; }
        public string W20 { get; set; }
        
        public string W21 { get; set; }
        public string W22 { get; set; }
        public string W23 { get; set; }
        public string W24 { get; set; }
        public string W25 { get; set; }
        public string W26 { get; set; }
        public string W27 { get; set; }
        public string W28 { get; set; }
        public string W29 { get; set; }
        //public string poType { get; set; }
        public string W30 { get; set; }
        public string W31 { get; set; }
        public string VendorNumber { get; set; }
       // public string PlantName { get; set; }
        public string PaceDescription { get; set; }
        public string AWU { get; set; }

        public string Min { get; set; }
        public string Max { get; set; }
        public string Onhand { get; set; }        

    }
    public class PackagesData
    {
        public string PACKAGE_ID { get; set; }
        public string PACKAGE_NUMBER { get; set; }
        public string SHIPMENT_ID { get; set; }
        public string STATUS { get; set; }
        public string STAGING_LANE { get; set; }
        public string QUANTITY { get; set; }
        public string SECONDARY_QUANTITY { get; set; }
        public string QUANTITY_RECEIVED { get; set; }
        public string REFERENCE { get; set; }
        public string REVISION { get; set; }
        public string INVOICE_PRICE { get; set; }
        public string HOST_KEY { get; set; }
        public string MIM_TRACKING_NUMBER { get; set; }
        public string MIM_FREIGHT_CHARGE { get; set; }
        public string MIM_CURRENCY { get; set; }
        public string MIM_WEIGHT { get; set; }
        public string ERP_CODE { get; set; }
        public string ERP_KEY_ID { get; set; }
        public string ERP_KEY_FIELD { get; set; }
        public string ERP_SHIP_TO_KEY { get; set; }
        public string ERP_SHIP_FROM_KEY { get; set; }
        public string ERP_ITEM_NUMBER { get; set; }
        public string ERP_ITEM_DESCRIPTION { get; set; }
        public string ERP_INVENTORY_ITEM_ID { get; set; }
        public string ERP_OPM_ITEM_ID { get; set; }
        public string ERP_ITEM_REV { get; set; }
        public string ERP_ITEM_LOT_CONTROLLED { get; set; }
        public string ERP_ITEM_SUBLOT_CONTROLLED { get; set; }
        public string ERP_ITEM_SERIAL_CONTROLLED { get; set; }
        public string ERP_ITEM_REV_CONTROLLED { get; set; }
        public string ERP_ITEM_UOM { get; set; }
        public string ERP_ITEM_SECONDARY_UOM { get; set; }
        public string ERP_ITEM_DUALUOM_CONTROL { get; set; }
        public string ERP_ITEM_WEIGHT { get; set; }
        public string ERP_ITEM_WEIGHT_UOM { get; set; }
        public string ERP_PO_NUMBER { get; set; }
        public string ERP_PO_NOTE { get; set; }
        public string ERP_PO_TYPE { get; set; }
        public string ERP_LINE_NUMBER { get; set; }
        public string ERP_VENDOR_NAME { get; set; }
        public string ERP_BUYER_NAME { get; set; }
        public string ERP_KANBAN_CARD { get; set; }
        public string ERP_SHIP_TO_NAME { get; set; }
        public string ERP_SHIP_TO_NAME_ID { get; set; }
        public string ERP_SHIP_TO_LOCATION { get; set; }
        public string ERP_SHIP_TO_LOCATION_ID { get; set; }
        public string ERP_SCHEDULE_NUMBER { get; set; }
        public string PO_LINE_LOCATION_ID { get; set; }
        public string INTERFACE_LOG { get; set; }
        public string INTERFACE_ERROR { get; set; }
        public string SO_PO_TRANSACTION_STATUS { get; set; }
        public string SCH_PROMISE_NUMBER { get; set; }
        public string VMI_AGREEMENT_NUMBER { get; set; }
        public string APPLICATION_CODE { get; set; }
        public string APPLICATION_KEY_ID { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public string ATTRIBUTE6 { get; set; }
        public string ATTRIBUTE7 { get; set; }
        public string ATTRIBUTE8 { get; set; }
        public string ATTRIBUTE9 { get; set; }
        public string ATTRIBUTE10 { get; set; }
        public string ATTRIBUTE11 { get; set; }
        public string ATTRIBUTE12 { get; set; }
        public string ATTRIBUTE13 { get; set; }
        public string ATTRIBUTE14 { get; set; }
        public string ATTRIBUTE15 { get; set; }
        public string ATTRIBUTE16 { get; set; }
        public string ATTRIBUTE17 { get; set; }
        public string ATTRIBUTE18 { get; set; }
        public string ATTRIBUTE19 { get; set; }
        public string ATTRIBUTE20 { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATION_DATE { get; set; }
        public string LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string VENDOR_MATERIAL { get; set; }
        public string COUNTRY_OF_ORIGIN_CODE { get; set; }
        public string COUNTRY_OF_ORIGIN_MARK { get; set; }
        public string REJECT_REASON { get; set; }
        public string FSL_USED { get; set; }
        public string BOX_PER_PACKAGE { get; set; }
        public string LAST_UPDATE_BY_NAME { get; set; }
        public string FAI_BLOCK { get; set; }
        public string RELEASE_UNTIL_DATE { get; set; }
        public string TYPE_CODE { get; set; }
        public string SPECIALHANDLING { get; set; }
        public string MP_CALL_ID { get; set; }
    }

    public class ShipmentsData
    {
        public string SHIPMENT_ID { get; set; }
        public string SHIPMENT_NUMBER { get; set; }
        public string STATUS { get; set; }
        public string CARRIER_NAME { get; set; }
        public string CARRIER_ID { get; set; }
        public string CARRIER_TRACKING_NUM { get; set; }
        public string WAYBILL { get; set; }
        public string REFERENCE { get; set; }
        public string RELEASE_CODE { get; set; }
        public string PACKING_SLIP { get; set; }
        public string FLIGHT_NUMBER { get; set; }
        public string SHIPPING_CHARGES { get; set; }
        public string FREIGHT_CHARGES { get; set; }
        public string NET_WEIGHT { get; set; }
        public string GROSS_WEIGHT { get; set; }
        public string CERTIFICATION { get; set; }
        public string COMPLETED_BY { get; set; }
        public string COMPLETED_DATE { get; set; }
        public string NUM_OF_PACKAGES { get; set; }
        public string ERP_CODE { get; set; }
        public string ERP_SHIP_TO_KEY { get; set; }
        public string ERP_SHIP_FROM_KEY { get; set; }
        public string ERP_FROM_ADDRESS1 { get; set; }
        public string ERP_FROM_ADDRESS2 { get; set; }
        public string ERP_FROM_ADDRESS3 { get; set; }
        public string ERP_FROM_CITY { get; set; }
        public string ERP_FROM_COUNTY { get; set; }
        public string ERP_FROM_STATE { get; set; }
        public string ERP_FROM_PROVINCE { get; set; }
        public string ERP_FROM_POSTAL_CODE { get; set; }
        public string ERP_FROM_COUNTRY { get; set; }
        public string ERP_TO_ADDRESS1 { get; set; }
        public string ERP_TO_ADDRESS2 { get; set; }
        public string ERP_TO_ADDRESS3 { get; set; }
        public string ERP_TO_CITY { get; set; }
        public string ERP_TO_COUNTY { get; set; }
        public string ERP_TO_STATE { get; set; }
        public string ERP_TO_PROVINCE { get; set; }
        public string ERP_TO_POSTAL_CODE { get; set; }
        public string ERP_TO_COUNTRY { get; set; }
        public string ERP_SHIP_TO_NAME { get; set; }
        public string ERP_SHIP_TO_LOCATION { get; set; }
        public string ERP_SHIP_TO_ID { get; set; }
        public string ERP_RECEIPT_NUMBER { get; set; }
        public string INTERFACE_LOG { get; set; }
        public string SO_PO_TRANSACTION_STATUS { get; set; }
        public string FROM_LINK_ID { get; set; }
        public string APPLICATION_KEY_ID { get; set; }
        public string ATTRIBUTE1 { get; set; }
        public string ATTRIBUTE2 { get; set; }
        public string ATTRIBUTE3 { get; set; }
        public string ATTRIBUTE4 { get; set; }
        public string ATTRIBUTE5 { get; set; }
        public string ATTRIBUTE6 { get; set; }
        public string ATTRIBUTE7 { get; set; }
        public string ATTRIBUTE8 { get; set; }
        public string ATTRIBUTE9 { get; set; }
        public string ATTRIBUTE10 { get; set; }
        public string ATTRIBUTE11 { get; set; }
        public string ATTRIBUTE12 { get; set; }
        public string ATTRIBUTE13 { get; set; }
        public string ATTRIBUTE14 { get; set; }
        public string ATTRIBUTE15 { get; set; }
        public string ATTRIBUTE16 { get; set; }
        public string ATTRIBUTE17 { get; set; }
        public string ATTRIBUTE18 { get; set; }
        public string ATTRIBUTE19 { get; set; }
        public string ATTRIBUTE20 { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATION_DATE { get; set; }
        public string LAST_UPDATE_DATE { get; set; }
        public string LAST_UPDATE_BY { get; set; }
        public string MARK_FOR_ADDRESS { get; set; }
        public string FREIGHT_BILL_NUM { get; set; }
        public string ERP_SHIP_TO_LOCATION_ID { get; set; }
        public string ERP_FROM_NAME { get; set; }
        public string ERP_TO_NAME { get; set; }
        public string NOTES { get; set; }
        public string TO_LINK_ID { get; set; }
        public string APPLICATION_CODE { get; set; }
        public string INTERFACE_ERROR { get; set; }
        public string INVOICE_NUMBER { get; set; }
        public string INVOICE_DESCRIPTION { get; set; }
        public string INVOICE_DATE { get; set; }
        public string TAX_CHARGES { get; set; }
        public string TAX_BILL_NUM { get; set; }
        public string TAX_NAME { get; set; }
        public string CURRENCY_KEY { get; set; }
        public string READY_TO_INVOICE { get; set; }
        public string PAY_SITE { get; set; }
        public string FREIGHT_TERMS { get; set; }
        public string INVOICE_ENABLED { get; set; }
        public string INVOICE_CREATED { get; set; }
        public string HOST_KEY { get; set; }
        public string PICKUP_DATE { get; set; }
        public string TRIP_ID { get; set; }
        public string EXPECTED_RECEIPT_DATE { get; set; }
        public string PROCESSING_STATUS { get; set; }
        public string PROCESSING_CODE { get; set; }
        public string DELIVERY_ID { get; set; }
        public string UNIT_OF_WEIGHT { get; set; }
        public string FSL_USED { get; set; }
        public string ERP_VENDOR_NAME { get; set; }
    }

    public class endavourAddress
    {
        public string addressId { get; set; }
        public string vendorName { get; set; }
        public string countrycode { get; set; }
        public string addressname { get; set; }
        public string purchaseorg { get; set; }
        public string countryname { get; set; }
        public string className { get; set; }
        public string plant { get; set; }
        public string addresstype { get; set; }
        public string addressline1 { get; set; }
        public string addressline2 { get; set; }
        public string addressline3 { get; set; }
        public string addressline4 { get; set; }
        public string addressline5 { get; set; }
        public string createdby { get; set; }
        public string createddate { get; set; }
        public string lastupdatedby { get; set; }
        public string lastupdatedate { get; set; }
        public string lastupdatedateend { get; set; }
        public string status { get; set; }
    }
    public class materials
    {
        public long ID { get; set; }
        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public string AMATMaterialNumber { get; set; }
        public string VendorMaterialNumber { get; set; }
        public string CaseQuantity { get; set; }
        public string NetWeight { get; set; }
        public string CountryofOrginCode { get; set; }
        public string CountryName { get; set; }
        public string CountryofOrginMark { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string LasteUpdateDate { get; set; }
        public string LasteUpdateDateEnd { get; set; }
        public string LastUpdatedBy { get; set; }
    }

    public partial class PurchaseOrderData
    {
        public long poNumber { get; set; }
        public string lineNumer { get; set; }
        public string shipmentNumber { get; set; }
        public string xpcPoNumber { get; set; }
        public string itemNumber { get; set; }
        public string itemDescription { get; set; }
        public string itemCategory { get; set; }
        public string statDelDate { get; set; }
        public string deliveryDate { get; set; }
        public string buyerName { get; set; }
        public string purchasingGroupKey { get; set; }
        public string startDate { get; set; }
        public string status { get; set; }
        public string erpPoStatusCode { get; set; }
        public string reasonForOrderingDesc { get; set; }
        public string vendorNumber { get; set; }
        public string purchasingOrgNum { get; set; }
        public string poLineLocationId { get; set; }
        public string accountAssignment { get; set; }
        public string quantity { get; set; }
        public string openQuantity { get; set; }
        public string quantityReceived { get; set; }
        public string bomReviewed { get; set; }
        public string bomChanged { get; set; }
        public string mrpAction { get; set; }
        public string vendorName { get; set; }
        public string materialGroup { get; set; }
        public string faiBlock { get; set; }
        public string lineItemId { get; set; }
        public string promiseddatetime { get; set; }
        public string itemId { get; set; }
        public string shipToLocationId { get; set; }
        public string billToLocationId { get; set; }
        public string poType { get; set; }
        public string releaseNumber { get; set; }
        public string erpCode { get; set; }
        public string printable { get; set; }
        public string key_1 { get; set; }
        public string shipToId { get; set; }
        public string poHeaderId { get; set; }
        public string kanbanCard { get; set; }
        public string needBydatetime { get; set; }
        public string unitOfMeasure { get; set; }
        public string price { get; set; }
        public string extendedPrice { get; set; }
        public string quantityOpen { get; set; }
        public string freightTerms { get; set; }
        public string fob { get; set; }
        public string shipVia { get; set; }
        public string currency { get; set; }
        public string paymentTermsName { get; set; }
        public string lineDescription { get; set; }
        public string lineNumber { get; set; }
        public string buyerTelephone { get; set; }
        public string buyerEmail { get; set; }
        public string shipToName { get; set; }
        public string itemUnitWeight { get; set; }
        public string itemWeightUOMCode { get; set; }
        public string itemLotControl { get; set; }
        public string itemSerialControl { get; set; }
        public string cancelReason { get; set; }
        public string poRevision { get; set; }
        public string vendorCustomerNumber { get; set; }
        public string startdatetime { get; set; }
        public string enddatetime { get; set; }
        public string blanketTotalAmount { get; set; }
        public string hostId { get; set; }
        public string poLineId { get; set; }
        public string shipmentNum { get; set; }
        public string deliverydatetime { get; set; }
        public string poLineDistributionId { get; set; }
        public string colorField { get; set; }
        public string itemCategoryDesc { get; set; }
        public string reasonForOrderingCode { get; set; }
        public string purchasingOrgDesc { get; set; }
        public string shippingInstrKey { get; set; }
        public string shippingInstrDesc { get; set; }
        public string qaControlKey { get; set; }
        public string blockFunctionDesc { get; set; }
        public string plannedDeliveryTime { get; set; }
        public string contractNumber { get; set; }
        public string contractLineNumber { get; set; }
        public string priceUnit { get; set; }
        public string orderPriceUnit { get; set; }
        public string poItemText { get; set; }
        public string deliveryToAddress { get; set; }
        public string rmaStatus { get; set; }
        public string eccn { get; set; }
        public string htsNumber { get; set; }
        public string mrpSuggesteddatetime { get; set; }
        public string newMrpSug { get; set; }
        public string acknowledgeRequired { get; set; }
        public string storageLocation { get; set; }
        public string accountAssignmentDesc { get; set; }
        public string bomReviewdatetime { get; set; }
        public string bomLastChangeddatetime { get; set; }
        public string serialNumber { get; set; }
        public string statDeldatetime { get; set; }
        public string bomRevision { get; set; }
        public string toolNumber { get; set; }
        public string needBydatetimeMca { get; set; }
        public string priority1 { get; set; }
        public string priority2 { get; set; }
        public string linedown { get; set; }
    }

    public partial class ScheduleData
    {
        public long scheduleNumber { get; set; }
        public string lineNumber { get; set; }
        public string shipmentNumber { get; set; }
        public string dueDate { get; set; }
        public string scheduleQuantity { get; set; }
        public string price { get; set; }
        public string itemNumber { get; set; }
        public string releaseNumber { get; set; }
        public string releaseType { get; set; }
        public string releaseDate { get; set; }
        public string openQty { get; set; }
        public string plant { get; set; }
        public string vendorName { get; set; }
        public string vendorNumber { get; set; }
    }

    public partial class DemandPurchaseWorkBenchData
    {
        public string supplierName { get; set; }
        public string exloc8 { get; set; }
        public string org { get; set; }
        public string pc { get; set; }
        public string partNumber { get; set; }
        public string partDescription { get; set; }
        public string status { get; set; }
        public string numberOfDaysAging { get; set; }
        public string supplierShortToOpen { get; set; }
        public string stockoutShortTerm { get; set; }
        public string supplierShortToMrpMidTerm { get; set; }
        public string wipQtyShortLongTerm { get; set; }
        public string demandSpike { get; set; }
        public string belowMinWithNoOpenPo { get; set; }
        public string paceLtIssue { get; set; }
        public string qtyOverMax { get; set; }
        public string amat { get; set; }
        public string propsData { get; set; }
        public string openPoReleases { get; set; }
        public string exloc1 { get; set; }
        public string sci { get; set; }
        public string min { get; set; }
        public string max { get; set; }
        public string awu { get; set; }
        public string supplierFgi { get; set; }
        public string wipNext4Weeks { get; set; }
        public string wipWeeks5To8 { get; set; }
        public string exloc7 { get; set; }
        public string WK1 { get; set; }
        public string WK2 { get; set; }
        public string WK3 { get; set; }
        public string WK4 { get; set; }
        public string WK5 { get; set; }
        public string WK6 { get; set; }
        public string WK7 { get; set; }
        public string WK8 { get; set; }
        public string WK9 { get; set; }
        public string WK10 { get; set; }
        public string WK11 { get; set; }
        public string WK12 { get; set; }
        public string WK13 { get; set; }
        public string WK14 { get; set; }
        public string WK15 { get; set; }
        public string WK16 { get; set; }
        public string WK17 { get; set; }
        public string WK18 { get; set; }
        public string WK19 { get; set; }
        public string WK20 { get; set; }
        public string WK21 { get; set; }
        public string WK22 { get; set; }
        public string WK23 { get; set; }
        public string WK24 { get; set; }
        public string WK25 { get; set; }
        public string WK26 { get; set; }
        public string maxBu { get; set; }
        public string safetyStockLevel { get; set; }
        public string leadTime { get; set; }
        public string userItemType { get; set; }
        public string MOQ { get; set; }
        public string FOQ { get; set; }



    }

    public partial class DemandItemAttributes
    {
        public string plantName { get; set; }
        public string supplierName { get; set; }
        public string itemNumber { get; set; }
        public string itemDescription { get; set; }
        public string Revision { get; set; }
        public string amatOnHand { get; set; }
        public string consignedOnHand { get; set; }
        public string purchasingGroup { get; set; }
        public string safetyStockReq { get; set; }
        public string serializedPart { get; set; }
        public string specProcKey { get; set; }
        public string unitOfMeasure { get; set; }       
    }
    public partial class SchedulesSummary
    {
        public long scheduleNumber { get; set; }
        public string lineNumber { get; set; }
        public string quantity { get; set; }
        public string received { get; set; }
        public string printed { get; set; }
        public string shipped { get; set; }
        public string itemNumber { get; set; }
        public string releaseNumber { get; set; }
        public string remaining { get; set; }

        public string releaseDate { get; set; }
        public string vendorNumber { get; set; }
    }


    public partial class ScheduleDeliverDates
    {
        public long scheduleNumber { get; set; }
        public string lineNumber { get; set; }
        public string shipmentNumber { get; set; }
        public string material { get; set; }
        public string quantity { get; set; }
        public string priority1 { get; set; }
        public string priority2 { get; set; }
        public string openqty { get; set; }
        public string erpCode { get; set; }
        public string key { get; set; }
        public string releaseNumber { get; set; }
        public string itemNumber { get; set; }
        public string releaseType { get; set; }
        public string LD { get; set; }
        public string statisticalDeliveryDate { get; set; }
        public string deliveryDate { get; set; }
        public string plant { get; set; }
        public string receivedQuantity { get; set; }
        public string vendorNumber { get; set; }
    }
    public class PoOrderRequest
    {
        public int Page_Id { get; set; }
        //public int poNumber { get; set; }
        public List<SearchParam> SearchParams { get; set; }
    }

    public class SearchParam
    {
        public string Condition { get; set; }
        public string Values { get; set; }
        public int field_namespace_id { get; set; }
        public string field_name { get; set; }
    }
}
