﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCP.BO
{
    public class CompanyVendors
    {

        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public string CompanyID { get; set; }

    }

    public class UserVendors
    {

        public string VendorNumber { get; set; }
        public string VendorName { get; set; }
        public string UserID { get; set; }

    }

    public class UserRoles
    {

        public string UserId { get; set; }
        public string RoleId { get; set; }
        public string RoleName { get; set; }

    }
    public class RolesandResponsible
    {

        public string roleid { get; set; }
        public string respid { get; set; }
        public string respname { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }

    }

    public class BusinessGroupHosts
    {

        public string BusinessGroupId { get; set; }
        public string HostID { get; set; }
        public string BusinessGroupName { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }

    }
    public class CompanyHosts
    {

        public string HostID { get; set; }
        public string CompanyID { get; set; }

    }
    public class UserHosts
    {

        public string HostID { get; set; }
        public string UserID { get; set; }

    }

    public class PageNamespaces
    {

        public int PageId { get; set; }
        public string field_namespace_id { get; set; }
        public string field_namespace_name { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }
    }
    public class PageResponsiblities
    {

        public int PageId { get; set; }
        public string Resp_Id { get; set; }
        public string Resp_name { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }
    }
    public class FieldsResponsiblities
    {

        public int FieldId { get; set; }
        public string Resp_Id { get; set; }
        public string Resp_name { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }
    }
    public class InsertFolderDetails
    {
        public int newid { get; set; }
        public string fieldid { get; set; }
        public string foldername { get; set; }
        public string pageid { get; set; }
        public string appcode { get; set; }
        public string usercreated { get; set; }
        public string groupid { get; set; }
        public string folderid { get; set; }
        public string squence { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }
    }

    public class InsertFolder
    {
        public int newid { get; set; }
        public string folderid { get; set; }
        public string fieldid { get; set; }
        public string foldername { get; set; }
        public string pageid { get; set; }
        public string appcode { get; set; }
        public string usercreated { get; set; }
        public string field_namespace_id { get; set; }
        public string groupid { get; set; }
        public string squence { get; set; }
        public string createdBy { get; set; }
        public string lastupdatedBy { get; set; }
    }
    public partial class confields
    {
        public long field_id { get; set; }
        public string field_namespace_id { get; set; }
        public string field_name { get; set; }
        public string description { get; set; }
        public string field_type { get; set; }
        public string wrap { get; set; }
        public string mode_id { get; set; }
        public string field_lov_id { get; set; }
        public string created_by { get; set; }
        public string creation_date { get; set; }
        public string last_update_by { get; set; }
        public string last_update_date { get; set; }
        public string DISABLED_FLAG { get; set; }
    }
}
