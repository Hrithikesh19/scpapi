﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCP.BO
{
    public class Folders
    {
        public int folder_id { get; set; }
        public string folder_name { get; set; }
        public int page_id { get; set; }

    }
}
