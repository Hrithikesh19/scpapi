//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UserDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class end_Address
    {
        public long addressId { get; set; }
        public string vendorName { get; set; }
        public string countrycode { get; set; }
        public string addressname { get; set; }
        public string purchaseorg { get; set; }
        public string countryname { get; set; }
        public string className { get; set; }
        public string plant { get; set; }
        public string addresstype { get; set; }
        public string addressline1 { get; set; }
        public string addressline2 { get; set; }
        public string addressline3 { get; set; }
        public string addressline4 { get; set; }
        public string addressline5 { get; set; }
        public Nullable<long> createdby { get; set; }
        public Nullable<System.DateTime> createddate { get; set; }
        public Nullable<long> lastupdatedby { get; set; }
        public Nullable<System.DateTime> lastupdatedate { get; set; }
        public string status { get; set; }
    }
}
