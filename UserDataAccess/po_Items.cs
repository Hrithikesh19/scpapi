//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UserDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class po_Items
    {
        public string itemNumer { get; set; }
        public string itemText { get; set; }
        public string itemRevision { get; set; }
        public Nullable<double> unitPrice { get; set; }
        public Nullable<double> extendedprice { get; set; }
        public string currency { get; set; }
        public Nullable<double> quantity { get; set; }
        public string uom { get; set; }
        public string faiblock { get; set; }
        public Nullable<double> qtyopen { get; set; }
        public Nullable<double> qtyReceived { get; set; }
        public Nullable<System.DateTime> releaseUntilldate { get; set; }
        public Nullable<int> plantNumber { get; set; }
    }
}
