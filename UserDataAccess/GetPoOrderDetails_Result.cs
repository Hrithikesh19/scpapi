//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UserDataAccess
{
    using System;
    
    public partial class GetPoOrderDetails_Result
    {
        public long poNumber { get; set; }
        public string poType { get; set; }
        public string poStatus { get; set; }
        public string Line { get; set; }
        public Nullable<System.DateTime> orderDate { get; set; }
        public string teaNumber { get; set; }
        public Nullable<double> release { get; set; }
        public Nullable<int> shipmentNumer { get; set; }
    }
}
