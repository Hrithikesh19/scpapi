//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UserDataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class con_folders
    {
        public long folder_id { get; set; }
        public string folder_name { get; set; }
        public int page_id { get; set; }
        public Nullable<System.DateTime> inactive_date { get; set; }
        public string application_code { get; set; }
        public Nullable<int> user_id { get; set; }
        public Nullable<int> company_id { get; set; }
        public string user_created { get; set; }
        public int created_by { get; set; }
        public System.DateTime creation_date { get; set; }
        public int last_update_by { get; set; }
        public System.DateTime last_update_date { get; set; }
        public Nullable<long> field_namespace_id { get; set; }
    }
}
